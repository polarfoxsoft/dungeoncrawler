﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using OpenTK.Input;
using OpenTK.Graphics.OpenGL;
using Engine.Graphics;
using Engine.UI;
using Engine.UI.Controls;
using Engine.Text;
using Engine.Core.Collisions;

namespace DungeonCrawler.Settings
{
	public class ResolutionSettings
	{
		#region Fields

		private static readonly List<int> Widths;
		private static readonly List<int> Heights;

		private int _widthIndex;
		private int _heightIndex;
		private TextStyle _style;
		private CoordinateManager _manager;
		private TextLine _widthView;
		private TextLine _heightView;
		private TextLine _widthTitle;
		private TextLine _heightTitle;
		private TextLine _volumeView;
		private TextLine _volumeTitle;
		private TextLine _title;
		private Sprite _background;
		private CheckBox _fullscreen;
		private CheckBox _mute;
		private ControlContainer _buttons;
		private AxisAlignedBoundingBox _bounds;
		private Point _margin;
		int _prevVolume;
		int _volume;
		bool _prevMute;
		Action _onApply;
		#endregion // Fields

		#region Properties

		public bool Active
		{
			get
			{
				return _buttons.Active;
			}
			set
			{
				_buttons.Active = value;
			}
		}

		public int Volume
		{
			get
			{
				return _volume;
			}
			set
			{
				if (value <=10 && value >= 0)
				{
					_volume = value;
				}				
			}
		}

		public int PrevVolume
		{
			get
			{
				return _prevVolume;
			}
			set
			{
				if (value <= 10 && value >= 0)
				{
					_prevVolume = value;
				}
			}
		}

		public int ScreenWidth
		{
			get
			{
				return Widths[_widthIndex];
			}
		}

		public int ScreenHeight
		{
			get
			{
				return Heights[_heightIndex];
			}
		}

		public bool Fullscreen
		{
			get
			{
				return _fullscreen.Checked;
			}
			set
			{
				_fullscreen.Checked = value;
			}
		}

		public bool Mute
		{
			get
			{
				return _mute.Checked;
			}
			set
			{
				_mute.Checked = value;
			}
		}

		public bool PrevMute
		{
			get
			{
				return _prevMute;
			}
			set
			{
				_prevMute = value;
			}
		}

		public int X
		{
			get
			{
				return _bounds.X;
			}

			set
			{
				_bounds.X = value;
				_buttons.X = value + _margin.X;
			}
		}

		public int Y
		{
			get
			{
				return _bounds.Y;
			}

			set
			{
				_bounds.Y = value;
				_buttons.Y = value + _margin.Y;
			}
		}

		public AxisAlignedBoundingBox Bounds
		{
			get
			{
				return _bounds;
			}
		}

		public int Width
		{
			get
			{
				return _bounds.Width;
			}
		}

		public int Height
		{
			get
			{
				return _bounds.Height;
			}
		}

		#endregion // Properties

		static ResolutionSettings()
		{
			Widths = new List<int> { 800, 1280, 1600, 1920 };
			Heights = new List<int> { 600, 720, 900, 1024, 1080 };
		}

		public ResolutionSettings(CoordinateManager manager, Action onApply, Action onVolumeApply, Action onCancel)
		{
			if (manager == null)
			{
				throw new ArgumentNullException("manager");
			}

			_manager = manager;
			//var font = new Engine.Text.Font("Fonts/Consolas.txt", _manager);
			var font = new Engine.Text.Font("Fonts/Courier.txt", _manager);
			var lightColor = Color.FromArgb(0, 255, 196);
			var darkColor = Color.FromArgb(6, 122, 95);
			//_style = new TextStyle(font, darkColor, 0.5f);
			_style = new TextStyle(font, darkColor, 2.0f);

			var rect = new Rectangle(0, 0, 450, 550);
			_bounds = new AxisAlignedBoundingBox(rect);
			_background = new Sprite(Texture.FromColor(Color.FromArgb(160, Color.WhiteSmoke)), rect, _manager);
			_widthTitle = new TextLine("Width", _style, _manager);
			_heightTitle = new TextLine("Height", _style, _manager);
			_volumeTitle = new TextLine("Select volume", _style, _manager);
			_title = new TextLine("Select screen size", _style, _manager);
			_margin = new Point(30, 130);

			var texture = Texture.FromImage("textures/editor_buttons.png");
			var buttonSize = 32;
			var inc = new Sprite(texture, new Rectangle(224, 0, buttonSize, buttonSize), _manager);
			var dec = new Sprite(texture, new Rectangle(224, buttonSize, buttonSize, buttonSize), _manager);
			var right = Width - 2 * _margin.X - buttonSize;
			var down = 240;
			var middle = 90;
			var apply = new TextLine("Apply", _style, _manager);
			var cancel = new TextLine("Cancel", _style, _manager);
			var fullscreenOn = new TextLine("Fullscreen: On", _style, _manager);
			var fullscreenOff = new TextLine("Fullscreen: Off", _style, _manager);
			var muteOn = new TextLine("Mute: On", _style, _manager);
			var muteOff = new TextLine("Mute: Off", _style, _manager);
			var fLevel = middle+ fullscreenOff.Height + 20;
			var mLevel = down + muteOff.Height + 40;
			var bottom = mLevel + 60;
			_onApply = onVolumeApply;

			_fullscreen = new CheckBox(fullscreenOff, new TextLine(fullscreenOff, lightColor), fullscreenOn, new TextLine(fullscreenOn, lightColor), _manager, (rect.Width - fullscreenOff.Width) / 2 - _margin.X, fLevel);
			_buttons = new ControlContainer();
			_mute = new CheckBox(muteOff, new TextLine(muteOff, lightColor), muteOn, new TextLine(muteOn, lightColor), _manager, (rect.Width - muteOff.Width) / 2 - _margin.X, mLevel);
			_buttons.Add(new Button(new DelegateCommand(() => DecWidthIndex()), 0, 0, dec, dec, _manager));
			_buttons.Add(new Button(new DelegateCommand(() => IncWidthIndex()), right, 0, inc, inc, _manager));
			_buttons.Add(new Button(new DelegateCommand(() => DecHeightIndex()), 0, middle, dec, dec, _manager));
			_buttons.Add(new Button(new DelegateCommand(() => IncHeightIndex()), right, middle, inc, inc, _manager));
			_buttons.Add(_fullscreen);
			_buttons.Add(new Button(new DelegateCommand(() => DecVolume()), 0, down + 25, dec, dec, _manager));
			_buttons.Add(new Button(new DelegateCommand(() => IncVolume()), right, down + 25, inc, inc, _manager));
			_buttons.Add(_mute);
			_buttons.Add(new Button(new DelegateCommand(() => { Active = false; onApply(); }), 25, bottom, apply, new TextLine(apply, lightColor), _manager));
			_buttons.Add(new Button(new DelegateCommand(() => { Active = false; onCancel(); }), Width - 2 * _margin.X - cancel.Width - 20, bottom, cancel, new TextLine(cancel, lightColor), _manager));

			_buttons.X = _margin.X;
			_buttons.Y = _margin.Y;
			UpdateVolumeView();
			SetResolution(_manager.Width, _manager.Height);
		}

		public void OnClick(object sender, MouseButtonEventArgs e)
		{
			if (Active)
			{
				_buttons.OnClick(sender, e);
				// The string of evil
				_onApply();
			}
		}

		public void OnHover(object sender, MouseMoveEventArgs e)
		{
			if (Active)
			{
				_buttons.OnHover(sender, e);
			}
		}

		public void Render()
		{
			UpdateVolumeView();
			GL.PushMatrix();

			GL.LoadIdentity();
			GL.Translate(-1, 1, 0);

			_manager.Translate(X, -Y - Height);
			_background.Render();

			GL.PushMatrix();
			_manager.Translate((Width - _title.Width) / 2, Height - _title.Height - 20);
			_title.Render();
			GL.PopMatrix();

			GL.PushMatrix();
			_manager.Translate((Width - _widthView.Width) / 2, Height - _margin.Y - _widthView.Height);
			_widthView.Render();
			GL.PopMatrix();

			GL.PushMatrix();
			_manager.Translate((Width - _widthTitle.Width) / 2, Height - _margin.Y - _widthView.Height + _widthView.Height + 10);
			_widthTitle.Render();
			GL.PopMatrix();

			GL.PushMatrix();
			_manager.Translate((Width - _heightView.Width) / 2, Height - _margin.Y - _heightView.Height - 90);
			_heightView.Render();
			GL.PopMatrix();

			GL.PushMatrix();
			_manager.Translate((Width - _heightTitle.Width) / 2, Height - _margin.Y - _heightView.Height - 90 + _heightView.Height + 10);
			_heightTitle.Render();
			GL.PopMatrix();

			GL.PushMatrix();
			_manager.Translate((Width - _volumeTitle.Width) / 2, Height - _volumeTitle.Height - 340);
			_volumeTitle.Render();
			GL.PopMatrix();
			GL.PushMatrix();
			_manager.Translate((Width - _volumeView.Width) / 2, Height - _margin.Y - _volumeView.Height - 265);
			_volumeView.Render();
			GL.PopMatrix();

			_buttons.Render();

			GL.PopMatrix();
		}

		public void SetResolution(int width, int height)
		{
			_widthIndex = Widths.FindIndex(x => x >= width);
			if (_widthIndex == -1)
			{
				_widthIndex = Widths.Count - 1;
			}
			_heightIndex = Heights.FindIndex(x => x >= height);
			if (_heightIndex == -1)
			{
				_heightIndex = Heights.Count - 1;
			}
			UpdateWidthView();
			UpdateHeightView();
		}

		public void SaveSettings()
		{
			using (var file = new StreamWriter("settings.txt"))
			{
				file.WriteLine(ScreenWidth);
				file.WriteLine(ScreenHeight);
				file.WriteLine(Fullscreen);
				file.WriteLine(Volume);
				file.WriteLine(Mute);
			}
		}

		public bool LoadSettings()
		{
			if (File.Exists("settings.txt"))
			{
				using (var file = new StreamReader("settings.txt"))
				{
					SetResolution(int.Parse(file.ReadLine()), int.Parse(file.ReadLine()));
					Fullscreen = bool.Parse(file.ReadLine());
					Volume = (int.Parse(file.ReadLine()));
					Mute = bool.Parse(file.ReadLine());
				}
				return true;
			}
			return false;
		}

		private void IncWidthIndex()
		{
			_widthIndex++;
			if (_widthIndex >= Widths.Count)
			{
				_widthIndex = 0;
			}
			UpdateWidthView();
		}

		private void DecWidthIndex()
		{
			_widthIndex--;
			if (_widthIndex < 0)
			{
				_widthIndex = Widths.Count - 1;
			}
			UpdateWidthView();
		}

		private void IncVolume()
		{
			Volume++;
			UpdateVolumeView();
		}

		private void DecVolume()
		{
			Volume--;
			UpdateVolumeView();
		}

		private void UpdateVolumeView()
		{

			_volumeView = new TextLine(string.Format("{0} %", Volume*10), _style, _manager);
		}
		private void UpdateWidthView()
		{
			_widthView = new TextLine(ScreenWidth.ToString(), _style, _manager);
		}

		private void IncHeightIndex()
		{
			_heightIndex++;
			if (_heightIndex >= Heights.Count)
			{
				_heightIndex = 0;
			}
			UpdateHeightView();
		}

		private void DecHeightIndex()
		{
			_heightIndex--;
			if (_heightIndex < 0)
			{
				_heightIndex = Heights.Count - 1;
			}
			UpdateHeightView();
		}

		private void UpdateHeightView()
		{
			_heightView = new TextLine(ScreenHeight.ToString(), _style, _manager);
		}
	}
}
