﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NAudio.Wave;

namespace DungeonCrawler.Audio
{
	public class AudioTrack
	{
		private WaveOut _waveOut;
		private LoopStream _stream;
		private float _volume;
		private float _volumeFactor;
		private bool _mute;

		public bool Mute
		{
			get
			{
				return _mute;
			}
			set
			{
				if (_mute && !value)
				{
					ApplyVolume();
				}
				else if (!_mute && value)
				{
					_waveOut.Volume = 0;
				}
				_mute = value;
			}
		}

		public float VolumeFactor
		{
			get { return _volumeFactor; }
			set
			{
				_volumeFactor = Normalize(value);
				ApplyVolume();
			}
		}

		public float Volume
		{
			get { return _volume; }
			set
			{
				_volume = Normalize(value);
				ApplyVolume();
			}
		}

		public float CurrentVolume
		{
			get { return _waveOut.Volume; }
		}

		public PlaybackState PlaybackState
		{
			get { return _waveOut.PlaybackState; }
		}

		public LoopStream LoopStream
		{
			get { return _stream; }
		}

		public AudioTrack(LoopStream stream, float volumeFactor = 1, float volume = 1)
			: this(stream, new WaveOut(), volumeFactor, volume)
		{
		}

		public AudioTrack(LoopStream stream, WaveOut waveOut, float volumeFactor = 1, float volume = 1)
		{
			_stream = stream;
			_waveOut = waveOut;
			_waveOut.Init(stream);
			VolumeFactor = volume;
			Volume = volume;
		}

		public void Play()
		{
			if (_waveOut.PlaybackState == PlaybackState.Stopped)
			{
				_stream.Position = 0;
			}
			_waveOut.Play();
		}

		public void Stop()
		{
			_waveOut.Stop();
		}

		public void Pause()
		{
			_waveOut.Pause();
		}

		public void Resume()
		{
			_waveOut.Resume();
		}

		private void ApplyVolume()
		{
			_waveOut.Volume = _volume * _volumeFactor;
		}

		private static float Normalize(float value)
		{
			return (value > 1) ? 1 : (value < 0) ? 0 : value;
		}
	}
}
