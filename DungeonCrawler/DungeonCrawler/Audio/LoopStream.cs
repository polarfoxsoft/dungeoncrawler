﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NAudio;
using NAudio.Wave;

namespace DungeonCrawler.Audio
{
	public class LoopStream : WaveStream
	{
		readonly WaveStream _stream;
		private TimeSpan _startTime;
		private TimeSpan _endTime;
		private long _startPosition;
		private long _endPosition;
		private bool _loopEvent;
		private bool _stopEvent;

		public LoopStream(WaveStream sourceStream)
		{
			_stream = sourceStream;
			EnableLooping = true;
			ResetCrop();
		}

		public event Action LoopFinished;
		public event Action LoopStopped;

		public bool EnableLooping { get; set; }

		public override WaveFormat WaveFormat
		{
			get { return _stream.WaveFormat; }
		}

		public override long Length
		{
			get { return _stream.Length; }
		}

		public override long Position
		{
			get { return _stream.Position; }
			set { _stream.Position = value; }
		}

		public TimeSpan StartTime
		{
			get { return _startTime; }
			set
			{
				if (value.CompareTo(_stream.TotalTime) > 0)
				{
					value = _stream.TotalTime;
				}
				_startTime = value;
				_startPosition = TimeToPosition(_startTime);
			}
		}

		public TimeSpan EndTime
		{
			get { return _endTime; }
			set
			{
				if (value.CompareTo(_stream.TotalTime) > 0)
				{
					value = _stream.TotalTime;
				}
				_endTime = value;
				_endPosition = TimeToPosition(_endTime);
			}
		}

		private long TimeToPosition(TimeSpan time)
		{
			if (time == _stream.TotalTime)
			{
				return _stream.Length;
			}
			var pos = _stream.Position;
			_stream.CurrentTime = time;
			var res = _stream.Position;
			_stream.Position = pos;
			return res;
		}

		public void ResetCrop()
		{
			StartTime = new TimeSpan();
			EndTime = _stream.TotalTime;
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			if (_stream.Position < _startPosition)
			{
				_stream.Position = _startPosition;
				_loopEvent = false;
				_stopEvent = false;
			}
			if (_loopEvent && !_stopEvent)
			{
				if (EnableLooping)
				{
					LoopFinished?.Invoke();
				}
				else
				{
					_stopEvent = true;
					LoopStopped?.Invoke();
				}
				_loopEvent = false;
			}
			if (_stopEvent)
			{
				return 0;
			}

			int bytesRead = _stream.Read(buffer, offset, count);
			if (_stream.Position >= _endPosition)
			{
				bytesRead -= (int)(_stream.Position - _endPosition);
				bytesRead = bytesRead < 0 ? 0 : bytesRead;
				_stream.Position = EnableLooping ? _startPosition : _endPosition;
				_loopEvent = true;
			}
			return bytesRead;
		}
	}
}
