﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using OpenTK.Input;
using OpenTK.Graphics;
using Engine;
using Engine.Core.Collisions;
using Engine.Graphics;
using Engine.Text;
using Engine.UI.Controls;

namespace DungeonCrawler
{
	public class HelpMessage
	{
		#region Fields
		
		private TextStyle _style;
		private CoordinateManager _manager;
		private TextLine[] _hints;
		private TextLine _title;
		private Sprite _background;
		private Button _close;
		private AxisAlignedBoundingBox _bounds;
		private Point _margin;
		private Point _buttonTranslation;
		private int _lineSpan;
		private int _groupSpan;
		bool _active;

		#endregion // Fields

		#region Properties

		public bool Active
		{
			get
			{
				return _active;
			}
			set
			{
				_active = value;
			}
		}

		public int X
		{
			get
			{
				return _bounds.X;
			}

			set
			{
				_bounds.X = value;
				_close.X = value + _buttonTranslation.X;
			}
		}

		public int Y
		{
			get
			{
				return _bounds.Y;
			}

			set
			{
				_bounds.Y = value;
				_close.Y = value + _buttonTranslation.Y;
			}
		}

		public AxisAlignedBoundingBox Bounds
		{
			get
			{
				return _bounds;
			}
		}

		public int Width
		{
			get
			{
				return _bounds.Width;
			}
		}

		public int Height
		{
			get
			{
				return _bounds.Height;
			}
		}

		#endregion // Properties

		public HelpMessage(CoordinateManager manager)
		{
			if (manager == null)
			{
				throw new ArgumentNullException("manager");
			}

			_manager = manager;
			var font = new Engine.Text.Font("Fonts/Courier.txt", _manager);
			var lightColor = Color.FromArgb(0, 255, 196);
			var darkColor = Color.FromArgb(6, 122, 95);
			_style = new TextStyle(font, darkColor, 2.0f);
			_title = new TextLine("Controls", _style, _manager);
			_hints = new TextLine[]
			{
				//new TextLine("W - move up", _style, _manager),
				//new TextLine("A - move left", _style, _manager),
				//new TextLine("S - move right", _style, _manager),
				//new TextLine("D - move down", _style, _manager),
				new TextLine("WASD / arrows - move ", _style, _manager),
				new TextLine("H - heal", _style, _manager),
				new TextLine("Mouse left - sword", _style, _manager),
				new TextLine("Mouse right - fireball", _style, _manager),
				new TextLine(" ", _style, _manager),
				new TextLine("Esc - pause", _style, _manager),
				new TextLine("F11 - fullscreen / windowed", _style, _manager)
			};
			_margin = new Point(30, 30);
			_lineSpan = 10;
			_groupSpan = 20;

			var width = 0;
			var height = _title.Height + _groupSpan;
			foreach (var line in _hints)
			{
				if (line.Width > width)
				{
					width = line.Width;
				}
				height += line.Height + _lineSpan;
			}

			var close = new TextLine("OK", _style, _manager);
			height += _groupSpan + close.Height;

			var rect = new Rectangle(0, 0, width + 2 * _margin.X, height + 2 * _margin.Y);
			_bounds = new AxisAlignedBoundingBox(rect);
			_background = new Sprite(Texture.FromColor(Color.FromArgb(160, Color.WhiteSmoke)), rect, _manager);
			
			_close = new Button(new DelegateCommand(() => { Active = false; }), 0, 0, close, new TextLine(close, lightColor), _manager);
			_buttonTranslation = new Point((Width - _close.Width) / 2, height);
		}

		public void OnClick(object sender, MouseButtonEventArgs e)
		{
			if (Active)
			{
				_close.OnClick(sender, e);
			}
		}

		public void OnHover(object sender, MouseMoveEventArgs e)
		{
			if (Active)
			{
				_close.OnHover(sender, e);
			}
		}

		public void Render()
		{
			GL.PushMatrix();

			GL.LoadIdentity();
			GL.Translate(-1, 1, 0);

			_manager.Translate(X, -Y - Height);
			_background.Render();

			GL.PushMatrix();
			_manager.Translate((Width - _title.Width) / 2, Height - _title.Height - _margin.Y);
			_title.Render();
			GL.PopMatrix();

			int height = _margin.Y + _groupSpan;

			foreach (var line in _hints)
			{
				GL.PushMatrix();
				_manager.Translate(_margin.X, Height - _margin.Y - line.Height - height);
				line.Render();
				GL.PopMatrix();
				height += line.Height + _lineSpan;
			}

			var x = _close.X;
			_close.Render();

			GL.PopMatrix();
		}
	}
}
