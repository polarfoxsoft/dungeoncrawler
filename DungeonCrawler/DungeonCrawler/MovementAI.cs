﻿using Engine.Core;
using Engine.Core.PathFinding;
using Engine.Graphics;
using Engine.MapRender;
using OpenTK.Graphics;
using System.Collections.Generic;
using System.Drawing;

namespace DungeonCrawler
{
	public class MovementAI
	{
		private AStar _pathFinder;
		private List<Node> _currentPath;
		private Node _lastGoalNode;
		private Node _lastBodyNode;
		private Map _map;
		private Character _body;

		public MovementAI(Character body, Map map, AStar pathFinder)
		{
			_body = body;
			_map = map;
			_pathFinder = pathFinder;
		}

		public static int GetSquaredDistance(Point from, Point to)
		{
			var x = to.X - from.X;
			var y = to.Y - from.Y;
			return x * x + y * y;
		}

		public void Update(Node currentNode, Node goalNode, Point dir)
		{
			var bodyCenter = new Point(_body.X + _body.Width / 2, _body.Y + _body.Height / 2);

			if (_lastGoalNode == null || _lastBodyNode == null || !_lastGoalNode.Equals(goalNode) || !_lastBodyNode.Equals(currentNode))
			{
				_currentPath = _pathFinder.FindPath(currentNode, goalNode);
				_lastBodyNode = currentNode;
				_lastGoalNode = goalNode;
			}

			if (_currentPath.Count > 0)
			{
				Point direction;
				if (_currentPath.Count > 1)
				{
					direction = new Point(_currentPath[1].X - _currentPath[0].X, _currentPath[1].Y - _currentPath[0].Y);
				}
				else
				{
					direction = new Point(dir.X, dir.Y);
				}

				_body.Direction.FromVector(direction);

				_body.UpdateX();
				var collision = _map.CheckCollision(_body);
				if (collision.Detected)
				{
					_body.X += collision.X;
					if (direction.Y == 0)
					{
						direction.Y = (_currentPath[0].Y * _map.TileSize + _map.TileSize / 2) - bodyCenter.Y;
						_body.Direction.FromVector(direction);
					}
				}
				_body.UpdateY();
				collision = _map.CheckCollision(_body);
				if (collision.Detected)
				{
					_body.Y += collision.Y;
					if (direction.X == 0)
					{
						direction.X = (_currentPath[0].X * _map.TileSize + _map.TileSize / 2) - bodyCenter.X;
						_body.Direction.FromVector(direction); _body.UpdateX();
						collision = _map.CheckCollision(_body);
						if (collision.Detected)
						{
							_body.X += collision.X;
						}
					}
				}
			}
		}

		public void RenderPath(CoordinateManager manager, Sprite pathSprite)
		{
			if (_currentPath != null)
			{
				foreach (var node in _currentPath)
				{
					GL.PushMatrix();
					manager.Translate(node.X * _map.TileSize, node.Y * _map.TileSize);
					pathSprite.Render();
					GL.PopMatrix();
				}
			}
		}
	}
}
