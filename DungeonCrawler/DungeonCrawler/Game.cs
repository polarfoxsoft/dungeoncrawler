﻿using System;
using System.Diagnostics;
using System.IO;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using OpenTK.Graphics;
using OpenTK.Input;
using Engine.Core;
using Engine.Core.Collisions;
using Engine.Graphics;
using Engine.MapRender;
using Engine.StoredMap;
using Engine.Text;
using Engine.UI;
using Engine.UI.Controls;
using Engine.Core.Items;
using Engine.Core.PathFinding;

namespace DungeonCrawler
{
	public class Game
	{
		private class DeadEnemy
		{
			public FighterAI Enemy;
			public int Lifetime;

			public DeadEnemy(FighterAI enemy, int lifetime)
			{
				Enemy = enemy;
				Lifetime = lifetime;
			}
		}

		#region Fields

		private Player _player;
		private List<FighterAI> _enemies;
		private List<DeadEnemy> _deadEnemies;
		private List<Projectile> _projectiles;
		private TileSet _tileset;
		private Map _map;
		private AStar _pathFinder;
		private NodeNetwork _nodeNetwork;
		private AnimationManager _fireball;
		private AnimationManager _greenFireball;
		private AnimationManager _ha;
		private AnimationManager _hl;
		private AnimationManager _hw;
		private AnimationManager _sa;
		private AnimationManager _sl;
		private AnimationManager _sw;
		private AnimationManager _na;
		private AnimationManager _nl;
		private AnimationManager _nw;
		private TextStyle _playerHpStyle;
		private TextStyle _enemyHpStyle;
		private bool _active;
		private bool _paused;
		private CoordinateManager _coordManager;
		private Engine.Text.Font _font;
		private Point _translation;
		private List<Projectile> _enemyProjectiles;
		private NecromancerAI _boss;
		private Engine.UI.Windows.MessageBox _gameover;
		private Random _random;
		private List<string> _scenario;
		private int _scenarioIndex;
		private List<Item> _items;
		private KeyItem _exitKey;
		private bool _exitKeyDropped;
		private Sprite _keySprite;
		private readonly int HealFrequency = 5;
		private readonly int CreationHealFrequency = 10;
		private Stopwatch _runTime;
		private bool _shooting;
		private bool _attacking;
		private Point _cursorPosition;

		private Texture _backgroundFilter;

		#endregion // Fields

		public bool Active
		{
			get
			{
				return _active;
			}
			set
			{
				_active = value;
				if (!_active)
				{
					Pause();
				}
				else if (IsOver())
				{
					NewGame();
				}
			}
		}

		public bool Paused
		{
			get
			{
				return _paused;
			}

			set
			{
				if (value)
				{
					Pause();
				}
				else
				{
					Unpause();
				}
			}
		}

		public Game(CoordinateManager manager)
		{
			if (manager == null)
			{
				throw new ArgumentNullException("manager");
			}

			_coordManager = manager;
			//_font = new Engine.Text.Font("Fonts/Consolas.txt", _coordManager);
			_font = new Engine.Text.Font("Fonts/Courier.txt", _coordManager);
			_tileset = TileSet.FromXml("textures/tileset.txt", _coordManager);
			_fireball = Application.FromXml("textures/fireball.txt", _coordManager);
			_greenFireball = Application.FromXml("textures/fireball_green.txt", _coordManager);
			_hl = Application.FromXml("textures/h5l.txt", _coordManager);
			_ha = Application.FromXml("textures/h5a.txt", _coordManager);
			_hw = Application.FromXml("textures/h5w.txt", _coordManager);
			_sl = Application.FromXml("textures/s13l.txt", _coordManager);
			_sa = Application.FromXml("textures/s13a.txt", _coordManager);
			_sw = Application.FromXml("textures/s13w.txt", _coordManager);
			_nl = Application.FromXml("textures/n5l.txt", _coordManager);
			_na = Application.FromXml("textures/n5a.txt", _coordManager);
			_nw = Application.FromXml("textures/n5w.txt", _coordManager);

			var hpfont = new Engine.Text.Font("Fonts/digits.txt", _coordManager);
			_playerHpStyle = new TextStyle(hpfont, Color.LawnGreen, 1.0f);
			_enemyHpStyle = new TextStyle(hpfont, Color.Red, 1.0f);
			//_playerHpStyle = new TextStyle(_font, Color.LawnGreen, 0.3f);
			//_enemyHpStyle = new TextStyle(_font, Color.Red, 0.3f);

			_runTime = new Stopwatch();
			_random = new Random();
			_scenario = new List<string>();
			_scenario.Add("Maps/map1.txt");
			_scenario.Add("Maps/map2.txt");
			//using (var reader = new StreamReader("scenario.txt"))
			//{
			//	while (!reader.EndOfStream)
			//	{
			//		_scenario.Add(reader.ReadLine());
			//	}
			//}

			_backgroundFilter = Texture.FromColor(Color.FromArgb(64, 64, 80, 64));

			SmallHealingPack.Init(_coordManager);
			_keySprite = new Sprite(Texture.FromImage("textures/h1.png"), new Rectangle(64, 0, 64, 64), manager);
			KeyItem.Init(_keySprite);
			NewGame();

			_translation = new Point();
			_paused = true;
		}

		#region Methods

		public void NewGame()
		{
			_gameover = null;
			_hw.SetAnimation("down");
			_player = new Player(5, 250, 0, 500, _playerHpStyle, _hl, _hw, _ha, new AxisAlignedBoundingBox(0, 0, _hw.CurrentAnimation.Width, _hw.CurrentAnimation.Height), _coordManager, new AxisAlignedBoundingBox(0, 0, 95, 95), 95, 23, 27, 800);
			//var gameover = new TextBlock("Game over", _coordManager.Width, _font, _coordManager, Color.DarkRed);
			var gameover = new TextBlock("Game over", _coordManager.Width, _font, _coordManager, Color.DarkRed, 4.0f);
			//var button = new TextLine("Menu", _font, _coordManager, Color.White, 0.5f);
			var button = new TextLine("Menu", _font, _coordManager, Color.White, 2.0f);
			_player.Killed += () => _gameover = new Engine.UI.Windows.MessageBox(_coordManager, gameover, button, new TextLine(button, Color.Gold), Texture.FromColor(Color.Black));

			_scenarioIndex = 0;
			NewMap(_scenario[_scenarioIndex]);
			_runTime.Restart();
		}

		private void NewMap(string path)
		{
			var idsMap = MapReader.Read(path);
			_map = new Map(_coordManager, idsMap, _tileset);
			_exitKey = _map.ExitKey;
			_exitKeyDropped = false;

			_nodeNetwork = new NodeNetwork(idsMap);
			_pathFinder = new AStar(_nodeNetwork, EuclideanHeuristic.GetInstance());

			_projectiles = new List<Projectile>();
			_enemyProjectiles = new List<Projectile>();
			_items = new List<Item>();

			_sw.SetAnimation("down");
			_nw.SetAnimation("down");

			_player.X = _map.Enterance.X;
			_player.Y = _map.Enterance.Y;
			var enemiesPositions = _map.GetEnemies();
			var bossPositions = _map.GetBoss();
			_enemies = new List<FighterAI>();
			_deadEnemies = new List<DeadEnemy>();
			Character enemy;
			foreach (var pos in enemiesPositions)
			{
				enemy = new Character(4, 100, 0, 0, _enemyHpStyle, new AnimationManager(_sl), new AnimationManager(_sw), new AnimationManager(_sa), new AxisAlignedBoundingBox(0, 0, _sw.CurrentAnimation.Width, _sw.CurrentAnimation.Height), _coordManager, new AxisAlignedBoundingBox(0, 0, 95, 95), 95, 9, 0, 0);
				while (_map.CheckCollision(enemy).Detected)
				{
					enemy.X = pos.X;
					enemy.Y = pos.Y;
					var temp = enemy;
					if (_random.Next(HealFrequency) == 0)
					{
						enemy.Killed += () => { _items.Add(new SmallHealingPack()); _items[_items.Count - 1].Drop(_map, new Point(temp.X, temp.Y + temp.Height - 60)); };
					}
					enemy.Killed += () => DropExitKey(new Point(temp.X, temp.Y + temp.Height - 60));
				}
				_enemies.Add(new FighterAI(enemy, 800, _player, _map, _coordManager, _pathFinder));
			}
			if (bossPositions.Count > 0)
			{
				enemy = new Character(2, 1500, 40, 0, _enemyHpStyle, _nl, _nw, _na, new AxisAlignedBoundingBox(0, 0, _nw.CurrentAnimation.Width, _nw.CurrentAnimation.Height), _coordManager, new AxisAlignedBoundingBox(0, 0, 95, 95), 95, 0, 20, 3000);
				while (_map.CheckCollision(enemy).Detected)
				{
					enemy.X = bossPositions[0].X;
					enemy.Y = bossPositions[0].Y;
				}
				_boss = new NecromancerAI(enemy, 1000, 200, _player, _map, _coordManager, _greenFireball, _enemyProjectiles, _enemies, SpawnSkeletons, _pathFinder);
				enemy.Killed += () => DropExitKey(new Point(_boss.Body.X, _boss.Body.Y + _boss.Body.Height - 60));
			}
			else
			{
				_boss = null;
			}
		}

		public void Render()
		{
			GL.PushMatrix();

			HoldMapOnScreen();
			_coordManager.Translate(-_player.Width / 2, -_player.Height / 2);

			GL.PushMatrix();

			_coordManager.Translate(-_player.X, -_player.Y);
			_map.Render();

			GL.PushMatrix();
			GL.LoadIdentity();
			GL.Translate(-1, -1, 0);
			new Sprite(_backgroundFilter, _coordManager.Height, _coordManager.Width, _coordManager).Render();
			GL.PopMatrix();

			foreach (var enemy in _deadEnemies)
			{
				enemy.Enemy.Render();
				enemy.Lifetime--;
			}

			foreach (var item in _items)
			{
				if (item.OnGround)
				{
					item.Render();
				}
			}

			foreach (var enemy in _enemies)
			{
				enemy.Render();
			}
			if (_boss != null)
			{
				_boss.Render();
			}
			

			foreach (var projectile in _projectiles)
			{
				projectile.Render();
			}

			foreach (var projectile in _enemyProjectiles)
			{
				projectile.Render();
			}

			if (!_map.Exit.Closed)
			{
				GL.PushMatrix();
				GL.LoadIdentity();
				GL.Translate(-1, 1, 0);
				_coordManager.Translate(10, -2 * _keySprite.Height - 20);
				_keySprite.Render();
				GL.PopMatrix();
			}
			
			GL.PopMatrix();

			_player.Render();
			if (_gameover != null && !_gameover.Closed)
			{
				GL.PushMatrix();
				GL.LoadIdentity();
				GL.Begin(BeginMode.Quads);
				GL.Color3(Color.Black);
				GL.Vertex2(1, 1);
				GL.Vertex2(1, -1);
				GL.Vertex2(-1, -1);
				GL.Vertex2(-1, 1);
				GL.End();
				GL.PopMatrix();
				_gameover.X = (_coordManager.Width - _gameover.Width) / 2;
				_gameover.Y = (_coordManager.Height - _gameover.Height) / 2;
				_gameover.Render();
			}
			
			GL.PopMatrix();
		}

		public void Update()
		{
			if (IsOver() && _gameover != null && _gameover.Closed)
			{
				Active = false;
			}
			if (Active && !_paused && _gameover == null)
			{
				_player.UpdateX();
				_map.LootChests(_player);
				var collision = _map.CheckCollision(_player);
				if (collision.Detected)
				{
					_player.X += collision.X;
				}

				_player.UpdateY();
				_map.LootChests(_player);
				collision = _map.CheckCollision(_player);
				if (collision.Detected)
				{
					_player.Y += collision.Y;
				}

				if (_shooting)
				{
					var direction = new Point(_cursorPosition.X - _coordManager.Width / 2 - _translation.X, _coordManager.Height / 2 - _cursorPosition.Y - _translation.Y);
					_player.Shoot(direction, _fireball, _projectiles);
				}
				if (_attacking)
				{
					var direction = new Point(_cursorPosition.X - _coordManager.Width / 2 - _translation.X, _coordManager.Height / 2 - _cursorPosition.Y - _translation.Y);
					_player.Attack(direction);
				}

				foreach (var item in _items)
				{
					if (GameObject.CheckAabbCollision(item, _player).Detected)
					{
						if (item == _exitKey)
						{
							_map.Exit.Open(_exitKey);
							item.PickUp();
						}
						else
						{
							_player.PickUp(item);
						}
					}
				}

				if (GameObject.CheckAabbCollision(_player, _map.Exit).Detected)
				{
					if (!_map.Exit.Closed && _scenarioIndex < _scenario.Count - 1)
					{
						_scenarioIndex++;
						NewMap(_scenario[_scenarioIndex]);
					}
					else if (!_map.Exit.Closed)
					{
						var text = string.Format("Run time {0}:{1}:{2}!", _runTime.Elapsed.Hours, _runTime.Elapsed.Minutes, _runTime.Elapsed.Seconds);
						text = string.Format("Run time {0} sec!", (int)_runTime.Elapsed.TotalSeconds);
						//var gameover = new TextBlock(text, _coordManager.Width, _font, _coordManager, Color.ForestGreen);
						var gameover = new TextBlock(text, _coordManager.Width, _font, _coordManager, Color.ForestGreen, 4.0f);
						//var button = new TextLine("Menu", _font, _coordManager, Color.White, 0.5f);
						var button = new TextLine("Menu", _font, _coordManager, Color.White, 2.0f);
						_gameover = new Engine.UI.Windows.MessageBox(_coordManager, gameover, button, new TextLine(button, Color.Gold), Texture.FromColor(Color.Black));
					}
				}

				if (_boss != null)
				{
					_boss.Update();
				}

				_deadEnemies.RemoveAll(e => e.Lifetime < 0);
				foreach (var enemy in _enemies)
				{
					enemy.Update();
					if (!enemy.Body.Alive)
					{
						_deadEnemies.Add(new DeadEnemy(enemy, 1000));
					}
				}
				_enemies.RemoveAll(e => !e.Body.Alive);

				foreach (var projectile in _projectiles)
				{
					projectile.UpdateX();
					projectile.UpdateY();
					if (_map.CheckCollision(projectile).Detected)
					{
						projectile.Destroy();
					}

					foreach (var enemy in _enemies)
					{
						if (enemy.Body.Alive)
						{
							projectile.Apply(enemy.Body);
						}
					}

					if (_boss != null && _boss.Body.Alive)
					{
						projectile.Apply(_boss.Body);
					}
				}
				_projectiles.RemoveAll(p => !p.Active && !p.AnimationPlaying);
								
				foreach (var projectile in _enemyProjectiles)
				{
					projectile.UpdateX();
					projectile.UpdateY();
					if (_map.CheckCollision(projectile).Detected)
					{
						projectile.Destroy();
					}
					projectile.Apply(_player);
				}
				_enemyProjectiles.RemoveAll(p => !p.Active && !p.AnimationPlaying);
			}
		}

		public void Pause()
		{
			if (!_paused)
			{
				_runTime.Stop();
				_paused = true;
				_player.Pause();
				foreach (var enemy in _enemies)
				{
					enemy.Body.Pause();
				}
				foreach (var projectile in _projectiles)
				{
					projectile.Pause();
				}
				foreach (var projectile in _enemyProjectiles)
				{
					projectile.Pause();
				}
				if (_boss != null)
				{
					_boss.Body.Pause();
				}				
			}
		}

		public void Unpause()
		{
			if (Active && _paused)
			{
				_paused = false;
				_player.Unpause();
				foreach (var enemy in _enemies)
				{
					enemy.Body.Unpause();
				}
				foreach (var projectile in _projectiles)
				{
					projectile.Unpause();
				}
				foreach (var projectile in _enemyProjectiles)
				{
					projectile.Unpause();
				}
				if (_boss != null)
				{
					_boss.Body.Unpause();
				}
				_runTime.Start();
			}
		}

		public bool IsOver()
		{
			return _player == null || !_player.Alive || _scenarioIndex == _scenario.Count - 1;
		}

		public void OnMouseDown(object sender, MouseButtonEventArgs e)
		{
			if (!_paused)
			{
				if (_gameover != null && !_gameover.Closed)
				{
					_gameover.OnClick(sender, e);
				}
				if (_player.Alive)
				{
					switch (e.Button)
					{
						case MouseButton.Left:
							_attacking = true;
							_shooting = false;
							break;
						case MouseButton.Right:
							_attacking = false;
							_shooting = true;
							break;
					}
					//var direction = new Point(e.X - _coordManager.Width / 2 - _translation.X, _coordManager.Height / 2 - e.Y - _translation.Y);
					//_player.Shoot(direction, _fireball, _projectiles);
				}
			}
		}

		public void OnMouseUp(object sender, MouseButtonEventArgs e)
		{
			if (!_paused)
			{
				switch (e.Button)
				{
					case MouseButton.Left:
						_attacking = false;
						break;
					case MouseButton.Right:
						_shooting = false;
						break;
				}
			}
		}

		public void OnMouseMove(object sender, MouseMoveEventArgs e)
		{
			if (!_paused)
			{
				if (_gameover != null && !_gameover.Closed)
				{
					_gameover.OnHover(sender, e);
				}
				_cursorPosition = e.Position;
			}
		}

		public void OnKeyDown(KeyboardKeyEventArgs e)
		{
			if (!_paused)
			{
				switch (e.Key)
				{
					case Key.Left:
					case Key.A:
						_player.Direction.Left = true;
						break;
					case Key.Right:
					case Key.D:
						_player.Direction.Right = true;
						break;
					case Key.Up:
					case Key.W:
						_player.Direction.Up = true;
						break;
					case Key.Down:
					case Key.S:
						_player.Direction.Down = true;
						break;
					case Key.H:
						_player.Heal();
						break;
				}
			}
		}

		public void OnKeyUp(KeyboardKeyEventArgs e)
		{
			if (!_paused)
			{
				switch (e.Key)
				{
					case Key.Left:
					case Key.A:
						_player.Direction.Left = false;
						break;
					case Key.Right:
					case Key.D:
						_player.Direction.Right = false;
						break;
					case Key.Up:
					case Key.W:
						_player.Direction.Up = false;
						break;
					case Key.Down:
					case Key.S:
						_player.Direction.Down = false;
						break;
				}
			}
		}

		private void HoldMapOnScreen()
		{
			var translation = new Point();

			var right = _map.Width - _coordManager.Width / 2 - _player.X - _player.Width / 2;
			var top = _map.Height - _coordManager.Height / 2 - _player.Y - _player.Height / 2;
			if (right < 0)
			{
				translation.X = -right;
			}
			if (top < 0)
			{
				translation.Y = -top;
			}

			var left = _coordManager.Width / 2 - _player.X - _player.Width / 2;
			var bottom = _coordManager.Height / 2 - _player.Y - _player.Height / 2;
			if (left > 0)
			{
				translation.X = -left;
			}
			if (bottom > 0)
			{
				translation.Y = -bottom;
			}

			_translation = translation;
			_coordManager.Translate(translation.X, translation.Y);
		}

		private void DropExitKey(Point location)
		{
			if (CheckExitKeyDrop())
			{
				_exitKey.Drop(_map, location);
				_items.Add(_exitKey);
				_exitKeyDropped = true;
			}
		}

		private bool CheckExitKeyDrop()
		{
			if (_exitKeyDropped)
			{
				return false;
			}
			if (_boss != null)
			{
				return !_boss.Body.Alive;
			}
			else
			{
				return _random.Next(_enemies.Count(e => e.Body.Alive)) == 0;
			}
		}

		private void SpawnSkeletons(int count)
		{
			for (int i = 0; i < count; i++)
			{
				var createon = new Character(2, 50, 0, 0, _enemyHpStyle, new AnimationManager(_sl), new AnimationManager(_sw), new AnimationManager(_sa), new AxisAlignedBoundingBox(0, 0, _sw.CurrentAnimation.Width, _sw.CurrentAnimation.Height), _coordManager, new AxisAlignedBoundingBox(0, 0, 95, 95), 95, 4, 0, 0);
				while (_map.CheckCollision(createon).Detected)
				{
					createon.X = _random.Next(_player.X - 500 > 0 ? _player.X - 500 : 0,
						_player.X + 500 < _map.Width * _map.TileSize ? _player.X + 500 : _map.Width * _map.TileSize);
					createon.Y = _random.Next(_player.Y - 500 > 0 ? _player.Y - 500 : 0,
						_player.Y + 500 < _map.Height * _map.TileSize ? _player.Y + 500 : _map.Height * _map.TileSize);
				}
				if (_random.Next(CreationHealFrequency) == 0)
				{
					createon.Killed += () => { _items.Add(new SmallHealingPack()); _items[_items.Count - 1].Drop(_map, new Point(createon.X, createon.Y + createon.Height - 60)); };
				}
				_enemies.Add(new FighterAI(createon, 800, _player, _map, _coordManager, _pathFinder));
			}
		}

		private void RenderPathFindingDebug()
		{
			var plNode = new Node(_map.GetColumnIndex(_player.X), _map.GetRowIndex(_player.Y), true, 1);
			var path = _pathFinder.FindPath(new Node(1, 3, true, 1), plNode);
			var size = _map.TileSize;
			var ps = new Sprite(Texture.FromColor(Color.FromArgb(80, Color.Red)), size, size, _coordManager);
			var ns = new Sprite(Texture.FromColor(Color.FromArgb(80, Color.Blue)), size, size, _coordManager);
			var nes = new Sprite(Texture.FromColor(Color.FromArgb(80, Color.Green)), size, size, _coordManager);
			var pls = new Sprite(Texture.FromColor(Color.FromArgb(100, Color.Magenta)), size, size, _coordManager);

			foreach (var node in _nodeNetwork.ValidNodes)
			{
				GL.PushMatrix();
				_coordManager.Translate(node.X * size, node.Y * size);
				ns.Render();
				GL.PopMatrix();
			}

			foreach (var node in path)
			{
				GL.PushMatrix();
				_coordManager.Translate(node.X * size, node.Y * size);
				ps.Render();
				GL.PopMatrix();
			}

			foreach (var node in _nodeNetwork.GetNeighbours(new Node(7, 3, true, 1)))
			{
				GL.PushMatrix();
				_coordManager.Translate(node.X * size, node.Y * size);
				nes.Render();
				GL.PopMatrix();
			}

			GL.PushMatrix();
			_coordManager.Translate(plNode.X * size, plNode.Y * size);
			pls.Render();
			GL.PopMatrix();
		}

		#endregion // Methods
	}
}