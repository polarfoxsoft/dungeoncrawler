﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using Engine.Graphics;
using Engine.Text;
using Engine.UI;
using Engine.UI.Controls;
using OpenTK;
using Engine.Core.Collisions;
using OpenTK.Input;

namespace DungeonCrawler.Menu
{
	public class ResolutionSettingsMenu : IControl
	{
		#region Fields

		private CoordinateManager _manager;
		private GameWindow _screen;
		private ControlContainer _buttons;
		private Point[] _resolutions = { new Point(800, 600), new Point(1280, 1024), new Point(1280, 720), new Point(1600, 900), new Point(1920, 1080) };
		private int _index;

		#endregion // Fields;

		#region Properties

		public int X
		{
			get
			{
				return _buttons.X;
			}

			set
			{
				_buttons.X = value;
			}
		}

		public int Y
		{
			get
			{
				return _buttons.Y;
			}

			set
			{
				_buttons.Y = value;
			}
		}

		public AxisAlignedBoundingBox Bounds
		{
			get
			{
				return _buttons.Bounds;
			}
		}

		public int Width
		{
			get
			{
				return _buttons.Width;
			}
		}

		public int Height
		{
			get
			{
				return _buttons.Height;
			}
		}

		public bool Active
		{
			get
			{
				return _buttons.Active;
			}
			set
			{
				_buttons.Active = value;
			}
		}

		#endregion // Properties

		public ResolutionSettingsMenu(CoordinateManager manager, GameWindow screen, Engine.Text.Font font)
		{
			if (manager == null)
			{
				throw new ArgumentNullException("manager");
			}
			if (screen == null)
			{
				throw new ArgumentNullException("screen");
			}
			if (font == null)
			{
				throw new ArgumentNullException("font");
			}

			_manager = manager;
			_screen = screen;
			
			var buttons = new List<IControl>();
			for (int i = 0; i < _resolutions.Length; i++)
			{
				var text = new TextLine(string.Format("{0}x{1}", _resolutions[i].X, _resolutions[i].Y), font, manager);
				var temp = i;
				buttons.Add(new Button(new DelegateCommand(() => _index = temp), text, new TextLine(text, Color.Gold), _manager));
			}
			var applyText = new TextLine("Apply", font, manager);
			buttons.Add(new Button(new DelegateCommand(() => UpdateResolution()), applyText, new TextLine(applyText, Color.Gold), _manager));
			_buttons = new ControlContainer(ControlArranger.ArrangeVertical(buttons, 0, 0, ControlArranger.Alignment.Middle, 2));
		}

		#region Methods

		public void Render()
		{
			_buttons.Render();
		}

		private void UpdateResolution()
		{
			if (_screen.Width != _resolutions[_index].X || _screen.Height != _resolutions[_index].Y)
			{
				_screen.ClientRectangle = new Rectangle(_screen.ClientRectangle.X, _screen.ClientRectangle.Y, _resolutions[_index].X, _resolutions[_index].Y);
				_manager.Update();
			}
		}

		public void OnClick(object sender, MouseButtonEventArgs e)
		{
			_buttons.OnClick(sender, e);
		}

		public void OnHover(object sender, MouseMoveEventArgs e)
		{
			_buttons.OnHover(sender, e);
		}

		#endregion // Methods
	}
}
