﻿using System;
using System.Drawing;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK.Graphics.OpenGL;
using Engine.Core;
using Engine.Core.Collisions;
using Engine.MapRender;
using Engine.Graphics;
using Engine.Core.PathFinding;

namespace DungeonCrawler
{
	public class NecromancerAI
	{
		private Player _player;
		private Character _body;
		private Map _map;
		private int _detectionDistance;
		private int _safeDistance;
		private CoordinateManager _manager;
		private AnimationManager _fireball;
		private List<Projectile> _projectiles;
		private List<FighterAI> _enemies;
		private Stopwatch _fireballCoolDown;
		private int _fireballCoolDownValue;
		private Point[] _fireballDirections;

		private int _riseCoolDownValue;
		private int _riseNumber;
		private Stopwatch _riseCoolDown;
		private Action<int> _rise;
		
		private MovementAI _movementAI;

		public Character Body { get { return _body; } }

		public NecromancerAI(Character body, int detectionDistance, int safeDistance, Player player, Map map, CoordinateManager manager, AnimationManager fireball, List<Projectile> projectiles, List<FighterAI> enemies, Action<int> rise, AStar pathFinder)
		{
			_body = body;
			_player = player;
			_map = map;
			_detectionDistance = detectionDistance * detectionDistance;
			_safeDistance = safeDistance * safeDistance;
			_manager = manager;
			_fireball = fireball;
			_projectiles = projectiles;
			_enemies = enemies;
			_fireballCoolDown = new Stopwatch();
			_fireballCoolDownValue = 7000;
			_riseCoolDown = new Stopwatch();
			_riseCoolDownValue = 11000;
			_riseNumber = 1;
			_rise = rise;
			_movementAI = new MovementAI(_body, _map, pathFinder);
			_riseCoolDown.Start();
			_fireballCoolDown.Start();

			int fireballCount = 20;
			var radius = 100;
			var angle = 0.0;
			var dAndle = Math.PI * 2 / fireballCount;

			_fireballDirections = new Point[fireballCount];
			for (int i = 0; i < fireballCount; i++)
			{
				var x = (int)(radius * Math.Cos(angle));
				var y = (int)(radius * Math.Sin(angle));
				_fireballDirections[i] = new Point(x, y);
				angle += dAndle;
			}
		}

		public void Update()
		{
			var playerCenter = new Point(_player.X + _player.Width / 2, _player.Y + _player.Height / 2);
			var bodyCenter = new Point(_body.X + _body.Width / 2, _body.Y + _body.Height / 2);
			var distanceSqr = MovementAI.GetSquaredDistance(bodyCenter, playerCenter);
			var dir = new Point(playerCenter.X - bodyCenter.X, playerCenter.Y - bodyCenter.Y);

			if (_body.Alive && distanceSqr < _detectionDistance)
			{
				if (distanceSqr > _safeDistance)
				{
					var currentNode = new Node(_map.GetColumnIndex(bodyCenter.X), _map.GetRowIndex(bodyCenter.Y), true, 1);
					var playerNode = new Node(_map.GetColumnIndex(playerCenter.X), _map.GetRowIndex(playerCenter.Y), true, 1);
					_movementAI.Update(currentNode, playerNode, dir);
				}
				else
				{
					_body.Direction.Reset();
				}

				if (_riseCoolDown.ElapsedMilliseconds >= _riseCoolDownValue || !_riseCoolDown.IsRunning)
				{
					_body.Attack();
					_rise(_riseNumber);
					_riseCoolDown.Restart();
					_fireballCoolDown.Restart();
					_riseNumber++;
				}

				if (_fireballCoolDown.ElapsedMilliseconds >= _fireballCoolDownValue)
				{
					var speed = 6;
					var damage = 20;
					var position = new Point(_body.X, _body.Y);
					var size = new Size(32, 32);
					var translation = new Point(16, 16);

					foreach (var direction in _fireballDirections)
					{
						_projectiles.Add(new Projectile(damage, position, direction, speed, size, _fireball, translation, _manager));
					}

					_fireballCoolDown.Restart();
				}
				else
				{
					_body.Shoot(new Point(_player.X - _body.X, _player.Y - _body.Y), _fireball, _projectiles);
				}
			}
			else
			{
				_body.Direction.Reset();
			}

			#region Old movement logic
			//var x = _player.X - _body.X;
			//var y = _player.Y - _body.Y;
			//var distanceSqr = x * x + y * y;
			//if (distanceSqr < _detectionDistance)
			//{
			//	var absX = Math.Abs(x);
			//	var absY = Math.Abs(y);
			//	var dirX = absX >= _body.Speed ? x / absX : 0;
			//	var dirY = absY >= _body.Speed ? y / absY : 0;
			//	if (distanceSqr > _safeDistance)
			//	{
			//		_body.Direction.FromVector(new Point(dirX, dirY));
			//		_body.UpdateX();
			//		var collision = _map.CheckCollision(_body);
			//		if (collision.Detected)
			//		{
			//			_body.X += collision.X;
			//		}
			//		_body.UpdateY();
			//		collision = _map.CheckCollision(_body);
			//		if (collision.Detected)
			//		{
			//			_body.Y += collision.Y;
			//		}
			//	}
			//	else if (distanceSqr < _safeDistance)
			//	{
			//		_body.Direction.FromVector(new Point(0, 0));
			//	}

			//	if (Body.Alive && _riseCoolDown.ElapsedMilliseconds >= _riseCoolDownValue || !_riseCoolDown.IsRunning)
			//	{
			//		_body.Attack();
			//		_rise(_riseNumber);
			//		_riseCoolDown.Restart();
			//		_fireballCoolDown.Restart();
			//		_riseNumber++;
			//	}

			//	if (Body.Alive && _fireballCoolDown.ElapsedMilliseconds >= _fireballCoolDownValue)
			//	{
			//		var speed = 6;
			//		var damage = 20;
			//		var position = new Point(_body.X, _body.Y);
			//		var size = new Size(32, 32);
			//		var translation = new Point(16, 16);

			//		foreach (var direction in _fireballDirections)
			//		{
			//			_projectiles.Add(new Projectile(damage, position, direction, speed, size, _fireball, translation, _manager));
			//		}

			//		_fireballCoolDown.Restart();
			//	}
			//	else
			//	{
			//		_body.Shoot(new Point(_player.X - _body.X, _player.Y - _body.Y), _fireball, _projectiles);
			//	}
			//}
			//else
			//{
			//	_body.Direction.Reset();
			//}
			//_player.ApplyAttack(_body);
			#endregion
		}

		public void Render()
		{
			GL.PushMatrix();
			_manager.Translate(_body.X, _body.Y);
			_body.Render();
			GL.PopMatrix();
		}
	}
}
