﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using Engine;
using Engine.MapRender;
using Engine.Core;
using Engine.Core.Collisions;
using Engine.Graphics;
using OpenTK.Graphics;
using Engine.Core.PathFinding;

namespace DungeonCrawler
{
	public class FighterAI
	{
		private Player _player;
		private Character _body;
		private Map _map;
		private int _detectionDistance;
		private CoordinateManager _manager;
		private Sprite _pathSprite;
		private MovementAI _movementAI;

		public Character Body { get { return _body; } }

		public FighterAI(Character body, int detectionDistance, Player player, Map map, CoordinateManager manager, AStar pathFinder)
		{
			_body = body;
			_player = player;
			_map = map;
			_detectionDistance = detectionDistance * detectionDistance;
			_manager = manager;
			_movementAI = new MovementAI(_body, _map, pathFinder);
			_pathSprite = new Sprite(Texture.FromColor(Color.FromArgb(80, Color.Yellow)), _map.TileSize, _map.TileSize, _manager);
		}

		public void Update()
		{
			var playerCenter = new Point(_player.X + _player.Width / 2, _player.Y + _player.Height / 2);
			var bodyCenter = new Point(_body.X + _body.Width / 2, _body.Y + _body.Height / 2);
			var distanceSqr = MovementAI.GetSquaredDistance(bodyCenter, playerCenter);
			var dir = new Point(playerCenter.X - bodyCenter.X, playerCenter.Y - bodyCenter.Y);

			//var x = _player.X - _body.X;
			//var y = _player.Y - _body.Y;
			//var distanceSqr = x * x + y * y;
			//if (distanceSqr < _detectionDistance && distanceSqr >= _player.Width * _player.Width)

			if (distanceSqr < _detectionDistance && !GameObject.CheckAabbCollision(_body, _player).Detected)
			{
				var currentNode = new Node(_map.GetColumnIndex(bodyCenter.X), _map.GetRowIndex(bodyCenter.Y), true, 1);
				var playerNode = new Node(_map.GetColumnIndex(playerCenter.X), _map.GetRowIndex(playerCenter.Y), true, 1);
				_movementAI.Update(currentNode, playerNode, dir);

				#region Old movement logic
				//var absX = Math.Abs(x);
				//var absY = Math.Abs(y);
				//var dirX = absX >= _body.Speed ? x / absX : 0;
				//var dirY = absY >= _body.Speed ? y / absY : 0;
				//_body.Direction.FromVector(new System.Drawing.Point(dirX, dirY));

				//_body.UpdateX();
				//var collision = _map.CheckCollision(_body);
				//if (collision.Detected)
				//{
				//	_body.X += collision.X;
				//}
				//_body.UpdateY();
				//collision = _map.CheckCollision(_body);
				//if (collision.Detected)
				//{
				//	_body.Y += collision.Y;
				//}
				#endregion
			}
			else
			{
				_body.Direction.Reset();
			}

			if (GameObject.CheckAabbCollision(_body, _player).Detected)
			{
				_body.Attack(dir);
			}
			_body.ApplyAttack(_player);
			_player.ApplyAttack(_body);
		}

		public void Render()
		{
			GL.PushMatrix();
			_manager.Translate(_body.X, _body.Y);
			_body.Render();
			GL.PopMatrix();
		}

		public void RenderPath()
		{
			_movementAI.RenderPath(_manager, _pathSprite);
		}
	}
}
