﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Input;
using Engine.Core;
using Engine.Core.Collisions;
using Engine.Graphics;
using Engine.MapRender;
using Engine.StoredMap;
using Engine.UI.Controls;
using System.Xml;
using System.IO;
using Engine.Xml;
using Engine.Text;
using DungeonCrawler.MapEditor;
using DungeonCrawler.Settings;
using DungeonCrawler.Audio;
using NAudio.Wave;
using System.ComponentModel;


namespace DungeonCrawler
{
	public class Application : GameWindow
	{
		private readonly string _version = "ver 0.1.6.0 (alpha)";
		private CoordinateManager _coordManager;
		private InputManager _input;
		private Engine.Text.Font _font;
		
		Sprite _guiText;
		TextLine _gameVersionText;
		TextBlock _line;
		Sprite _background1;
		Sprite _background2;
		Texture _background3;

		ControlContainer _buttons;
		ControlContainer _mainMenu;

		Animation[] _skeleton;
		Animation[] _necromancer;
		Map _bgMap;
		Texture _bgFilter;
		Point _position;
		float _angle;
		
		AudioTrack _trackMain;
		AudioTrack _trackGame;
		AudioTrack _trackEditor;
		//ResolutionSettingsMenu _resMenu;

		private TextLine _pausedMsg;

		Game _game;
		EditorView _editor;
		ResolutionSettings _resolution;
		HelpMessage _help;

		[STAThread]
		static void Main()
		{
			LoadPackages();
			using (var app = new Application())
			{
				app.Run();
			}
		}

		Application()
			: base(800, 600)
		{
			//GenerateRandomMap(45, 42, 3);
			Title = "Dungeon";
			Icon = Properties.Resources.skull;
			_coordManager = new CoordinateManager(this);
			KeyDown += OnKeyDown;
			KeyUp += OnKeyUp;
			_input = new InputManager(this);
			//_font = new Engine.Text.Font("Fonts/Consolas.txt", _coordManager);
			_font = new Engine.Text.Font("Fonts/Courier.txt", _coordManager);
			var lightColor = Color.FromArgb(0, 255, 196);
			var darkColor = Color.FromArgb(6, 122, 95);
			var style = new TextStyle(_font, Color.White, 5.0f);

			_trackMain = new AudioTrack(LoadMp3Loop("Music/Menu.mp3"));
			_trackMain.LoopStream.StartTime = new TimeSpan(0, 0, 2);
			_trackGame = new AudioTrack(LoadMp3Loop("Music/Game.mp3"));
			_trackEditor = new AudioTrack(LoadMp3Loop("Music/Editor.mp3"));

			_game = new Game(_coordManager);
			MouseDown += _game.OnMouseDown;
			MouseUp += _game.OnMouseUp;
			MouseMove += _game.OnMouseMove;

			var exitCommand = new DelegateCommand(() => Exit());
			
			var mainMenu1 = new TextLine("New game", style, _coordManager);
			var mainMenu2 = new TextLine("Continue", style, _coordManager);
			var mainMenu3 = new TextLine("Map editor", style, _coordManager);
			var mainMenu4 = new TextLine("Settings", style, _coordManager);
			var mainMenu5 = new TextLine("Controls", style, _coordManager);
			var mainMenuExit = new TextLine("Exit", style, _coordManager);
			var mainMenu = new List<IControl>();
			var selectedColor = Color.Gold;
			mainMenu.Add(new Button(new DelegateCommand(() => { _game.Active = true; _buttons.Active = true; _mainMenu.Active = false; }), mainMenu2, new TextLine(mainMenu2, selectedColor), _coordManager));
			mainMenu.Add(new Button(new DelegateCommand(() => { _game.NewGame(); _game.Active = true; _buttons.Active = true; _mainMenu.Active = false; }), mainMenu1, new TextLine(mainMenu1, selectedColor), _coordManager));
			mainMenu.Add(new Button(new DelegateCommand(() => { _editor.NewMap(); _editor.Active = true;  _mainMenu.Active = false; }), mainMenu3, new TextLine(mainMenu3, selectedColor), _coordManager));
			mainMenu.Add(new Button(new DelegateCommand(() => { _resolution.SetResolution(Width, Height); _resolution.Fullscreen = WindowState == WindowState.Fullscreen; _resolution.Active = true; _mainMenu.Active = false; }), mainMenu4, new TextLine(mainMenu4, selectedColor), _coordManager));
			mainMenu.Add(new Button(new DelegateCommand(() => { _help.Active = true; _mainMenu.Active = false; }), mainMenu5, new TextLine(mainMenu5, selectedColor), _coordManager));
			mainMenu.Add(new Button(exitCommand, mainMenuExit, new TextLine(mainMenuExit, selectedColor), _coordManager));
			ControlArranger.ArrangeVertical(mainMenu, 0, 0, ControlArranger.Alignment.Middle, 5);
			_mainMenu = new ControlContainer(mainMenu);
			MouseDown += _mainMenu.OnClick;
			MouseMove += _mainMenu.OnHover;
			_mainMenu.Active = true;

			style.Size = 3.0f;
			var btn1text = new TextLine("Resume", style, _coordManager);
			var btn2text = new TextLine("1280x720", style, _coordManager);
			var btn3text = new TextLine("800x600", style, _coordManager);
			var btn4text = new TextLine("Main menu", style, _coordManager);
			var btn5text = new TextLine("Exit", style, _coordManager);
			var btn6text = new TextLine("Controls", style, _coordManager);
			var btns = new List<IControl>();
			btns.Add(new Button(new DelegateCommand(() => { _game.Unpause(); _buttons.Active = false; }), btn1text, new TextLine(btn1text, selectedColor), _coordManager));
			//btns.Add(new Button(new DelegateCommand(() => { Width = 1280; Height = 720; }), btn3text, new TextLine(btn3text, selectedColor), _coordManager));
			//btns.Add(new Button(new DelegateCommand(() => { Width = 800; Height = 600; }), btn4text, new TextLine(btn4text, selectedColor), _coordManager));
			//var bvl_res = new List<IRenderable>();
			//var svl_res = new List<IRenderable>();
			//var sl_res = new List<string>();
			//bvl_res.Add(btn2text);
			//bvl_res.Add(btn3text);
			//svl_res.Add(new TextLine(btn2text, selectedColor));
			//svl_res.Add(new TextLine(btn3text, selectedColor));
			//sl_res.Add("1280x720");
			//sl_res.Add("800x600");
			//btns.Add(new StatesButton<string>(bvl_res, svl_res, sl_res, _coordManager));
			btns.Add(new Button(new DelegateCommand(() => { _game.Active = false; _buttons.Active = false; _mainMenu.Active = true; _editor.Active = false; }), btn4text, new TextLine(btn4text, selectedColor), _coordManager));
			btns.Add(new Button(new DelegateCommand(() => { _help.Active = true; }), btn6text, new TextLine(btn6text, selectedColor), _coordManager));
			btns.Add(new Button(exitCommand, btn5text, new TextLine(btn5text, selectedColor), _coordManager));

			ControlArranger.ArrangeVertical(btns, 10, 10, ControlArranger.Alignment.Middle, 0);
			_buttons = new ControlContainer(btns);
			MouseDown += _buttons.OnClick;
			MouseMove += _buttons.OnHover;

			//Bitmap text_bmp = new Bitmap(800, 100);
			//Graphics gfx = Graphics.FromImage(text_bmp);
			//gfx.Clear(Color.FromArgb(0, 255, 255, 255));
			//Font font = new Font(FontFamily.GenericSansSerif, 50.0f);
			//gfx.DrawString("System fonts test", font, Brushes.White, new PointF(30, 30));
			//_guiText = new Sprite(new Texture(text_bmp), _coordManager);

			//_gameVersionText = new TextLine(_version, _font, _coordManager, 0.25f);
			_gameVersionText = new TextLine(_version, _font, _coordManager, 2.0f);

			var msg = "A dungeon crawl is a type of scenario in fantasy role-playing games in which heroes navigate a labyrinthine environment (a 'dungeon'), battling various monsters, and looting any treasure they may find. Because of its simplicity, a dungeon crawl can be easier for a gamemaster to run than more complex adventures, and the 'hack and slash' style of play is appreciated by players who focus on action and combat.";
			_line = new TextBlock(msg, 600, _font, _coordManager, 0.3f);
			_background1 = new Sprite(Texture.FromColor(Color.FromArgb(150, Color.Black)), new Rectangle(0, 0, _line.Width, _line.Height), _coordManager);
			//_background2 = new Sprite(Texture.FromImage("textures/s13w.png"), _coordManager);
			_background3 = Texture.FromColor(Color.FromArgb(150, Color.Black));
			
			_pausedMsg = new TextLine("Game on pause", style, _coordManager);
			_editor = new EditorView(_coordManager, _input);
			MouseDown += _editor.OnMouseDown;
			MouseMove += _editor.OnMouseMove;

			_resolution = new ResolutionSettings(_coordManager, () => { ApplyResolutionSettings(); }, () => { ApplyVolume(); },
				() => { _resolution.SetResolution(Width, Height); _resolution.Volume = _resolution.PrevVolume; _resolution.Mute = _resolution.PrevMute; });
			MouseDown += _resolution.OnClick;
			MouseMove += _resolution.OnHover;
			_resolution.LoadSettings();
			_resolution.PrevVolume = _resolution.Volume;
			_resolution.PrevMute = _resolution.Mute;
			ApplyResolutionSettings();

			InitMenuBackground();
			InitMenuBgAnimations();
			_help = new HelpMessage(_coordManager);
			MouseDown += _help.OnClick;
			MouseMove += _help.OnHover;
		}

		private static void LoadPackages()
		{
			var storage = Engine.FileStorage.StorageManager.Instance;
			var packageNames = new string[]
			{
				"game/fonts.dca",
				"game/textures.dca",
				"game/maps.dca",
				"game/music.dca"
			};
			foreach (var name in packageNames)
			{
				if (File.Exists(name))
				{
					storage.AddStorage(new Engine.FileStorage.Package(name));
				}
			}
		}

		private static void CreatePackages()
		{
			Engine.FileStorage.Package.Pack("game/fonts.dca", "Fonts");
			Engine.FileStorage.Package.Pack("game/textures.dca", "textures", e => !(e.Contains("\\not_in_use\\") || e.Contains("\\shadows_off\\") || e.Contains("\\skeleton_old\\")));
			//Engine.FileStorage.Package.Pack("game/textures.dca", "textures");
			Engine.FileStorage.Package.Pack("game/maps.dca", "Maps");
			Engine.FileStorage.Package.Pack("game/music.dca", new string[] { "Music/Editor.mp3", "Music/Game.mp3", "Music/Menu.mp3" });
			//Engine.FileStorage.Package.Pack("game/music.dca", "Music");
		}

		private static LoopStream LoadMp3Loop(string fileName)
		{
			var file = Engine.FileStorage.StorageManager.Instance.GetFile(fileName);
			return new LoopStream(new Mp3FileReader(file));
		}

		public static AnimationManager FromXml(string path, CoordinateManager manager)
		{
			var am = new AnimationManager();
			var doc = new XmlDocument();
			doc.Load(Engine.FileStorage.StorageManager.Instance.GetFile(path));
			var texture = Texture.FromImage(doc.DocumentElement.GetAttribute("texture"));

			foreach (var item in doc.DocumentElement.GetElementsByTagName(XmlLoader.AnimationTagName).Cast<XmlElement>())
			{
				am.Add(item.GetAttribute("name"), XmlLoader.LoadAnimation(item, texture, manager));
			}
			return am;
		}

		private IDsMap GenerateRandomMap(int mi, int mj, int randomFactor)
		{
			var m = new int[mi][];
			for (int i = 0; i < mi; i++)
			{
				m[i] = new int[mj];
				m[i][0] = 1;
				m[i][mj - 1] = 1;
			}
			for (int j = 0; j < mj; j++)
			{
				m[0][j] = 1;
				m[mi - 1][j] = 1;
			}
			var rand = new Random();
			var margin = 2;
			for (int i = margin; i < mi - margin; i++)
			{
				for (int j = margin; j < mj - margin; j++)
				{
					if (rand.Next(randomFactor) == 1)
					{
						m[i][j] = 1;
					}
				}
			}
			return new IDsMap(mi, mj, m);
		}

		private void ApplyResolutionSettings()
		{
			if (_resolution.Fullscreen)
			{
				GoFullscreen(true);
			}
			else
			{
				GoFullscreen(false);
				int x = X;
				int y = Y;
				if (Width != _resolution.ScreenWidth)
				{
					x -= (_resolution.ScreenWidth - Width) / 2;
					//Width = _resolution.ScreenWidth;
					ClientSize = new Size(_resolution.ScreenWidth, ClientSize.Height);
				}
				if (Height != _resolution.ScreenHeight)
				{
					y -= (_resolution.ScreenHeight - Height) / 2;
					//Height = _resolution.ScreenHeight;
					ClientSize = new Size(ClientSize.Width, _resolution.ScreenHeight);
				}
				X = x;
				Y = y;
			};
			var volume = _resolution.Volume * 0.1f;
			_trackMain.Volume = volume;
			_trackMain.Mute = _resolution.Mute;
			_trackGame.Volume = volume;
			_trackGame.Mute = _resolution.Mute;
			_trackEditor.Volume = volume;
			_trackEditor.Mute = _resolution.Mute;
			_resolution.PrevVolume = _resolution.Volume;
			_resolution.PrevMute = _resolution.Mute;
		}

		private void ApplyVolume()
		{
			_trackMain.Volume = _resolution.Volume * 0.1f;
			_trackMain.Mute = _resolution.Mute;
		}

		private void GoFullscreen(bool toFullscreen)
		{
			if (toFullscreen)
			{
				WindowState = WindowState.Fullscreen;
			}
			else
			{
				WindowState = WindowState.Normal;
			}
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			WindowBorder = WindowBorder.Fixed;
			GL.Enable(EnableCap.Blend);
			GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
			GL.TexEnv(TextureEnvTarget.TextureEnv, TextureEnvParameter.TextureEnvMode, Color.Red);
		}

		protected override void OnUpdateFrame(FrameEventArgs e)
		{
			base.OnUpdateFrame(e);
			_input.Update();
		}

		protected override void OnRenderFrame(FrameEventArgs e)
		{
			base.OnRenderFrame(e);
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
			//GL.DepthFunc(DepthFunction.Lequal);
			//GL.Enable(EnableCap.DepthTest);
			GL.MatrixMode(MatrixMode.Modelview);
			
			GL.PushMatrix();

			if (_game.Active)
			{
				_trackMain.Stop();
				_trackEditor.Stop();
				_game.Update();
				if (_game.Active)
				_game.Render();

				if (_buttons.Active)
				{
					_trackGame.Pause();
					GL.LoadIdentity();
					GL.PushMatrix();
					GL.Translate(-1, -1, 0);
					new Sprite(_background3, new Rectangle(0, 0, Width, Height), _coordManager).Render();
					GL.PopMatrix();
					if (_help.Active)
					{
						_help.Render();
					}
					else
					{
						_coordManager.Translate(-_pausedMsg.Width / 2, -_pausedMsg.Height - 20);
						GL.Translate(0, 1, 0);
						_pausedMsg.Render();
						_buttons.Render();
					}
				}
				else
				{
					_trackGame.Play();
				}
			}
			else if (_editor.Active)
			{
				_trackMain.Stop();
				_trackGame.Stop();
				_trackEditor.Play();
				RenderMenuBackground();
				RenderMenuBgAnimations();
				_editor.Update();
				_editor.Render();
			}
			else if (_resolution.Active)
			{
				RenderMenuBackground();
				RenderMenuBgAnimations();
				RenderGameVersion();
				_resolution.Render();
			}
			else if (_help.Active)
			{
				RenderMenuBackground();
				RenderMenuBgAnimations();
				RenderGameVersion();
				_help.Render();
			}
			else
			{
				_mainMenu.Active = true;
				_trackGame.Stop();
				_trackEditor.Stop();
				_trackMain.Play();
				//_coordManager.Translate(-_background2.Width / 2, -_background2.Height / 2);
				//_background2.Render();
				RenderMenuBackground();
				RenderMenuBgAnimations();
				RenderGameVersion();
				_mainMenu.Render();
			}

			GL.PopMatrix();

			SwapBuffers();
		}

		private void RenderMenuBackground()
		{
			GL.PushMatrix();
			GL.LoadIdentity();
			GL.Translate(-1, -1, 0);
			_coordManager.Translate(-_position.X - (int)(Math.Cos(_angle) * 300), -_position.Y - (int)(Math.Sin(_angle) * 300));
			_angle += 0.005f;
			_bgMap.Render();
			GL.LoadIdentity();
			GL.Translate(-1, -1, 0);
			new Sprite(_bgFilter, Height, Width, _coordManager).Render();
			GL.PopMatrix();
		}

		private void RenderMenuBgAnimations()
		{
			int xMargin = 10;
			int yMargin = 20;
			GL.PushMatrix();
			GL.LoadIdentity();
			GL.Translate(1, -1, 0);
			_coordManager.Translate(-_skeleton[0].Width - xMargin, yMargin);
			_coordManager.Translate(-_skeleton[0].Width - xMargin, yMargin);
			GL.Scale(2, 2, 1);
			foreach (var item in _skeleton)
			{
				item.Render();
			}

			GL.LoadIdentity();
			GL.Translate(-1, -1, 0);
			_coordManager.Translate(xMargin, yMargin);
			_coordManager.Translate(xMargin, yMargin);
			GL.Scale(2, 2, 1);
			foreach (var item in _necromancer)
			{
				item.Render();
			}
			GL.PopMatrix();
		}

		private void RenderGameVersion()
		{
			GL.PushMatrix();
			GL.Translate(-1, -1, 0);
			_coordManager.Translate(5, 1);
			_gameVersionText.Render();
			GL.PopMatrix();
		}

		private void InitMenuBackground()
		{
			_bgMap = new Map(_coordManager, new IDsMap(50, 40), TileSet.FromXml("textures/tileset.txt", _coordManager));
			//_bgMap = new Map(_coordManager, GenerateRandomMap(50, 40, 10), TileSet.FromXml("textures/tileset.txt", _coordManager));
			_bgFilter = Texture.FromColor(Color.FromArgb(0x7f4f4f4f));
			_bgFilter = Texture.FromColor(Color.FromArgb(200, 32, 32, 32));
			_bgFilter = Texture.FromColor(Color.FromArgb(0x7f1f0f1f));
			_position = new Point(_bgMap.TileSize * 20, _bgMap.TileSize * 30);
			_angle = 0;
		}

		private void InitMenuBgAnimations()
		{
			_skeleton = new Animation[2];
			var tempAM = FromXml("textures/s13l.txt", _coordManager);
			tempAM.SetAnimation("down");
			_skeleton[0] = tempAM.CurrentAnimation;
			tempAM = FromXml("textures/s13a.txt", _coordManager);
			tempAM.SetAnimation("down");
			_skeleton[1] = tempAM.CurrentAnimation;
			_skeleton[0].Play();
			_skeleton[1].Play();

			_necromancer = new Animation[2];
			tempAM = FromXml("textures/h5l.txt", _coordManager);
			tempAM.SetAnimation("down");
			_necromancer[0] = tempAM.CurrentAnimation;
			tempAM = FromXml("textures/h5w.txt", _coordManager);
			tempAM.SetAnimation("down");
			_necromancer[1] = tempAM.CurrentAnimation;
			_necromancer[0].Play();
			_necromancer[1].Play();

			//tempAM = FromXml("textures/h5l.txt", _coordManager);
			//tempAM = FromXml("textures/h5w.txt", _coordManager);
		}

		protected override void OnResize(EventArgs e)
		{
			GL.Viewport(ClientRectangle);
			GL.MatrixMode(MatrixMode.Projection);
			GL.Ortho(-1, 1, -1, 1, 1, -1);
			_coordManager.Update();
			_buttons.X = (Width - _buttons.Width) / 2;
			_buttons.Y = (Height - _buttons.Height) / 2;
			_mainMenu.X = (Width - _mainMenu.Width) / 2;
			_mainMenu.Y = (Height - _mainMenu.Height) / 2;
			_resolution.X = (Width - _resolution.Width) / 2;
			_resolution.Y = (Height - _resolution.Height) / 2;
			_help.X = (Width - _help.Width) / 2;
			_help.Y = (Height - _help.Height) / 2;
		}

		protected override void OnMove(EventArgs e)
		{
			base.OnMove(e);
		}
		
		protected void OnKeyDown(object sender, KeyboardKeyEventArgs e)
		{
			_game.OnKeyDown(e);
			switch (e.Key)
			{
				case Key.F11:
					GoFullscreen(WindowState != WindowState.Fullscreen) ;
					break;
				case Key.Escape:
					if (_game.Active)
					{
						_game.Paused = !_game.Paused;
						_buttons.Active = _game.Paused;
					}
					break;
			}
		}

		protected void OnKeyUp(object sender, KeyboardKeyEventArgs e)
		{
			_game.OnKeyUp(e);
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			base.OnClosing(e);
			_trackMain.Stop();
			_trackGame.Stop();
			_trackEditor.Stop();
			_resolution.SaveSettings();
		}
	}
}
