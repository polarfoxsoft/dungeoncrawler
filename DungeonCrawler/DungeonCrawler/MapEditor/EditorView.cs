﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Xml;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using Engine.Core;
using Engine.Graphics;
using Engine.MapRender;
using Engine.StoredMap;
using Engine.Text;
using Engine.UI;
using Engine.UI.Controls;
using Engine.UI.Windows;
using Engine.Xml;

namespace DungeonCrawler.MapEditor
{
	public class EditorView
	{
		#region Fields

		private readonly float MinZoom = 0.1f;

		private CoordinateManager _manager;
		private InputManager _input;
		private Point _position;
		private int _scrollSpeed;
		private Map _map;
		private GridLines _grid;
		private Sprite _highlight;
		private Point _highlightPosition;

		private Texture _uiBackground;
		private ControlContainer _menu;
		private CheckBox _buildButton;
		private CheckBox _destroyButton;
		private CheckBox _moveButton;
		private Button _exitButton;
		private MapSizeView _mapSize;
		private float _zoom;
		private bool _moving;
		private int _movingId;
		private Point _oldPosition;

		private TextStyle _style;
		private TextStyle _styleSelected;
		private MessageBox _savedMessage;

		#endregion // Fields

		public bool Active { get; set; }

		private float Zoom
		{
			get
			{
				return _zoom;
			}
			set
			{
				_zoom = value > 1 ? 1 : value;
				_zoom = _zoom < MinZoom ? MinZoom : _zoom;
			}
		}

		public EditorView(CoordinateManager manager, InputManager input)
		{
			if (manager == null)
			{
				throw new ArgumentNullException("manager");
			}
			if (input == null)
			{
				throw new ArgumentNullException("input");
			}

			_manager = manager;
			_input = input;
			_position = new Point();
			_scrollSpeed = 20;
			InitUI();
			NewMap();

			//_highlight = new Sprite(Texture.FromColor(Color.FromArgb(0x5FFF0000)), _map.TileSize, _map.TileSize, _manager);
			//_highlightPosition = new Point();

			_zoom = 1;
			//var font = new Engine.Text.Font("Fonts/Consolas.txt", _manager);
			var font = new Engine.Text.Font("Fonts/Courier.txt", _manager);
			//_style = new TextStyle(font, Color.FromArgb(6, 122, 95), 0.5f);
			_style = new TextStyle(font, Color.FromArgb(6, 122, 95), 2.0f);
			_styleSelected = new TextStyle(font, Color.FromArgb(0, 255, 196), 0.5f);
		}

		#region Methods

		public void NewMap()
		{
			int rowsCount = 40;
			int columnsCount = 30;
			if (_map != null)
			{
				rowsCount = _map.ColumnsCount;
				columnsCount = _map.RowsCount;
			}
			_mapSize = new MapSizeView(columnsCount, rowsCount, _manager, () => CreateMap(_mapSize.RowsCount, _mapSize.ColumnsCount), () => { });
			_mapSize.Active = true;
		}

		public void Update()
		{
			if (Active && _map != null)
			{
				var direction = new Point();
				if (_input.KeyDown(Key.W) || _input.KeyDown(Key.Up))
				{
					direction.Y += 1;
				}
				if (_input.KeyDown(Key.S) || _input.KeyDown(Key.Down))
				{
					direction.Y -= 1;
				}
				if (_input.KeyDown(Key.A) || _input.KeyDown(Key.Left))
				{
					direction.X -= 1;
				}
				if (_input.KeyDown(Key.D) || _input.KeyDown(Key.Right))
				{
					direction.X += 1;
				}
				if (_input.KeyDown(Key.Z))
				{
					Zoom -= 0.01f;
				}
				if (_input.KeyDown(Key.X))
				{
					Zoom += 0.01f;
				}
				if (_input.KeyDown(Key.C))
				{
					Zoom = 1;
				}

				_position.X += direction.X * _scrollSpeed;
				_position.Y += direction.Y * _scrollSpeed;

				HoldMapOnScreen();
			}
		}

		public void Render()
		{
			GL.PushMatrix();

			if (_map != null)
			{
				GL.Translate(-1, -1, 0);
				_manager.Translate(-_position.X, -_position.Y);
				GL.Scale(Zoom, Zoom, 1);
				_map.Render();
				_grid.Render();
			}

			RenderUI();

			GL.PopMatrix();
		}

		public void OnMouseMove(object sender, MouseMoveEventArgs e)
		{
			if (_savedMessage == null || _savedMessage.Closed)
			{
				_exitButton.OnHover(sender, e);
				_menu.OnHover(sender, e);
				_mapSize.OnHover(sender, e);
			}
			else
			{
				_savedMessage.OnHover(sender, e);
			}
		}

		public void OnMouseDown(object sender, MouseButtonEventArgs e)
		{
			if (Active && (_savedMessage == null || _savedMessage.Closed))
			{
				_mapSize.OnClick(sender, e);

				if (e.Y <= _exitButton.Height)
				{
					var build = _buildButton.Checked;
					var destroy = _destroyButton.Checked;
					var move = _moveButton.Checked;

					_menu.OnClick(sender, e);

					if (build != _buildButton.Checked && _buildButton.Checked == true)
					{
						_destroyButton.Checked = false;
						_moveButton.Checked = false;
					}
					else if (destroy != _destroyButton.Checked && _destroyButton.Checked == true)
					{
						_buildButton.Checked = false;
						_moveButton.Checked = false;
					}
					else if (move != _moveButton.Checked && _moveButton.Checked == true)
					{
						_buildButton.Checked = false;
						_destroyButton.Checked = false;
					}

					_exitButton.OnClick(sender, e);
				}
				else if (_map != null && !_mapSize.Active && (_buildButton.Checked || _destroyButton.Checked || _moveButton.Checked))
				{
					var mapPoint = new Point();
					mapPoint.X = (int)((e.X + _position.X) / Zoom);
					mapPoint.Y = (int)((_manager.Height - e.Y + _position.Y) / Zoom);
					var selectedTile = _map.GetTile(mapPoint.X, mapPoint.Y);
					//_highlightPosition.X = selectedTile.X;
					//_highlightPosition.Y = selectedTile.Y;

					if (_buildButton.Checked)
					{
						_map.ChangeCell(mapPoint.X, mapPoint.Y, 1);
					}
					else if(_destroyButton.Checked)
					{
						_map.ChangeCell(mapPoint.X, mapPoint.Y, 0);
					}
					else if (_moving)
					{
						_map.ChangeCell(mapPoint.X, mapPoint.Y, _movingId);
						_map.ChangeCell(_oldPosition.X, _oldPosition.Y, 0);
						_moving = false;
					}
					else
					{
						_moving = true;
						_movingId = _map.GetCellId(mapPoint.X, mapPoint.Y);
						_oldPosition = mapPoint;
					}
				}
			}
			else if (Active)
			{
				_savedMessage.OnClick(sender, e);
			}
		}

		private void HoldMapOnScreen()
		{
			if (_map != null)
			{
				var dx = (int)(_map.Width * Zoom) - _manager.Width;
				var dy = (int)(_map.Height * Zoom) - _manager.Height + _exitButton.Height;

				if (_position.X > dx)
				{
					_position.X = dx;
				}
				if (_position.Y > dy)
				{
					_position.Y = dy;
				}

				if (_position.X < 0)
				{
					_position.X = 0;
				}
				if (_position.Y < 0)
				{
					_position.Y = 0;
				}
			}
		}

		private void RenderUI()
		{
			GL.PushMatrix();
			GL.LoadIdentity();
			_manager.Translate(-_manager.Width / 2, _manager.Height / 2 - _exitButton.Height);
			new Sprite(_uiBackground, new Rectangle(0, 0, _manager.Width, _exitButton.Height), _manager).Render();
			GL.PopMatrix();

			_exitButton.X = _manager.Width - _exitButton.Width;
			_exitButton.Render();
			_menu.Render();

			if (_savedMessage != null && !_savedMessage.Closed)
			{
				_savedMessage.X = (_manager.Width - _savedMessage.Width) / 2;
				_savedMessage.Y = (_manager.Height - _savedMessage.Height) / 2;
				_savedMessage.Render();
			}

			if (_mapSize.Active)
			{
				_mapSize.X = (_manager.Width - _mapSize.Width) / 2;
				_mapSize.Y = (_manager.Height - _mapSize.Height) / 2;
				_mapSize.Render();
			}
		}

		private void InitUI()
		{
			_uiBackground = Texture.FromColor(Color.FromArgb(200, Color.WhiteSmoke));
			var buttons = Texture.FromImage("textures/editor_buttons.png");
			var bSize = 64;
			_menu = new ControlContainer();

			var doc = new XmlDocument();
			doc.Load(Engine.FileStorage.StorageManager.Instance.GetFile("textures/editor_buttons.txt"));
			var buttonViews = doc.DocumentElement.ChildNodes.Cast<XmlElement>();

			int index = 0;

			var build = new Sprite(buttons, XmlLoader.LoadRectangle(buttonViews.First(e => e.GetAttribute("name") == "build").ChildNodes.Cast<XmlElement>().First()), _manager);
			var buildA = new Sprite(buttons, XmlLoader.LoadRectangle(buttonViews.First(e => e.GetAttribute("name") == "build_active").ChildNodes.Cast<XmlElement>().First()), _manager);
			_buildButton = new CheckBox(build, build, buildA, buildA, _manager, index * bSize, 0);
			_menu.Add(_buildButton);
			index++;

			//var destroy = new Sprite(buttons, new Rectangle(bSize, bSize, bSize, bSize), _manager);
			//var destroyA = new Sprite(buttons, new Rectangle(2 * bSize, 3 * bSize, bSize, bSize), _manager);
			var destroy = new Sprite(buttons, XmlLoader.LoadRectangle(buttonViews.First(e => e.GetAttribute("name") == "break").ChildNodes.Cast<XmlElement>().First()), _manager);
			var destroyA = new Sprite(buttons, XmlLoader.LoadRectangle(buttonViews.First(e => e.GetAttribute("name") == "break_active").ChildNodes.Cast<XmlElement>().First()), _manager);
			_destroyButton  = new CheckBox(destroy, destroy, destroyA, destroyA, _manager, index * bSize, 0);
			_menu.Add(_destroyButton);
			index++;

			//var move = new Sprite(buttons, new Rectangle(0, 2 * bSize, bSize, bSize), _manager);
			//var moveA = new Sprite(buttons, new Rectangle(bSize, 3 * bSize, bSize, bSize), _manager);
			var move = new Sprite(buttons, XmlLoader.LoadRectangle(buttonViews.First(e => e.GetAttribute("name") == "move").ChildNodes.Cast<XmlElement>().First()), _manager);
			var moveA = new Sprite(buttons, XmlLoader.LoadRectangle(buttonViews.First(e => e.GetAttribute("name") == "move_active").ChildNodes.Cast<XmlElement>().First()), _manager);
			_moveButton = new CheckBox(move, move, moveA, moveA, _manager, index * bSize, 0);
			_menu.Add(_moveButton);
			index++;

			var newMap = new Sprite(buttons, XmlLoader.LoadRectangle(buttonViews.First(e => e.GetAttribute("name") == "play").ChildNodes.Cast<XmlElement>().First()), _manager);
			_menu.Add(new Button(new DelegateCommand(() => NewMap()), index * bSize, 0, newMap, newMap, _manager));
			index++;

			//var save = new Sprite(buttons, new Rectangle(2 * bSize, bSize, bSize, bSize), _manager);
			var save = new Sprite(buttons, XmlLoader.LoadRectangle(buttonViews.First(e => e.GetAttribute("name") == "save").ChildNodes.Cast<XmlElement>().First()), _manager);
			_menu.Add(new Button(new DelegateCommand(() => SaveMap()), index * bSize, 0, save, save, _manager));
			index++;

			//var zoomUp = new Sprite(buttons, new Rectangle(0, 0, bSize, bSize), _manager);
			var zoomUp = new Sprite(buttons, XmlLoader.LoadRectangle(buttonViews.First(e => e.GetAttribute("name") == "more").ChildNodes.Cast<XmlElement>().First()), _manager);
			_menu.Add(new Button(new DelegateCommand(() => Zoom += 0.1f), index * bSize, 0, zoomUp, zoomUp, _manager));
			index++;

			//var zoomDefault = new Sprite(buttons, new Rectangle(0, 3 * bSize, bSize, bSize), _manager);
			var zoomDefault = new Sprite(buttons, XmlLoader.LoadRectangle(buttonViews.First(e => e.GetAttribute("name") == "normalize").ChildNodes.Cast<XmlElement>().First()), _manager);
			_menu.Add(new Button(new DelegateCommand(() => Zoom = 1), index * bSize, 0, zoomDefault, zoomDefault, _manager));
			index++;

			//var zoomDown = new Sprite(buttons, new Rectangle(0, bSize, bSize, bSize), _manager);
			var zoomDown = new Sprite(buttons, XmlLoader.LoadRectangle(buttonViews.First(e => e.GetAttribute("name") == "less").ChildNodes.Cast<XmlElement>().First()), _manager);
			_menu.Add(new Button(new DelegateCommand(() => Zoom -= 0.1f), index * bSize, 0, zoomDown, zoomDown, _manager));
			index++;

			//var exit = new Sprite(buttons, new Rectangle(2 * bSize, 0, bSize, bSize), _manager);
			var exit = new Sprite(buttons, XmlLoader.LoadRectangle(buttonViews.First(e => e.GetAttribute("name") == "exit").ChildNodes.Cast<XmlElement>().First()), _manager);
			_exitButton = new Button(new DelegateCommand(() => Exit()), exit, exit, _manager);

			_menu.Active = true;
		}

		private void CreateMap(int rowsCount, int columnsCount)
		{
			var tileset = TileSet.FromXml("textures/tileset.txt", _manager);
			var ids = new IDsMap(rowsCount, columnsCount);
			ids.CreateFrameOf(1);
			ids.ChangeCell(1, 1, 2);
			ids.ChangeCell(1, 2, 3);
			ids.ChangeCell(1, 3, 4);
			_map = new Map(_manager, ids, tileset);
			_grid = new GridLines(new Size(_map.TileSize, _map.TileSize), _map.RowsCount, _map.ColumnsCount, _manager);
		}

		private void SaveMap()
		{
			if (_map != null)
			{
				var nameBase = "Editor/map";
				var extension = ".txt";
				var name = string.Concat(nameBase, extension);
				int number = 1;

				while (File.Exists(name))
				{
					name = string.Concat(nameBase, number, extension);
					number++;
				}

				if (!Directory.Exists("Editor"))
				{
					Directory.CreateDirectory("Editor");
				}
				_map.Export(name);

				var message = new TextBlock("Your map was saved as " + name, 400, _style.Font, _manager, _style.Color, _style.Size);
				var ok = new TextLine("OK", _style, _manager);
				_savedMessage = new MessageBox(_manager, message, ok, new TextLine(ok, _styleSelected.Color), _uiBackground);
			}
		}

		private void Exit()
		{
			Active = false;
			_buildButton.Checked = false;
			_destroyButton.Checked = false;
			_moveButton.Checked = false;
		}

		#endregion // Methods
	}
}
