﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK.Input;
using OpenTK.Graphics.OpenGL;
using Engine.Graphics;
using Engine.UI;
using Engine.UI.Controls;
using Engine.Text;
using Engine.Core.Collisions;

namespace DungeonCrawler.MapEditor
{
	public class MapSizeView : IControl
	{
		#region Fields

		private readonly int MinColumnsCount = 1;
		private readonly int MinRowsCount = 1;

		private int _columnsCount;
		private int _rowsCount;
		private TextStyle _style;
		private CoordinateManager _manager;
		private TextLine _widthView;
		private TextLine _heightView;
		private TextLine _widthTitle;
		private TextLine _heightTitle;
		private TextLine _title;
		private Sprite _background;
		private ControlContainer _buttons;
		private AxisAlignedBoundingBox _bounds;
		private Point _margin;

		#endregion // Fields

		#region Properties

		public bool Active
		{
			get
			{
				return _buttons.Active;
			}
			set
			{
				_buttons.Active = value;
			}
		}

		public int ColumnsCount
		{
			get
			{
				return _columnsCount;
			}
			set
			{
				_columnsCount = value > MinColumnsCount ? value : MinColumnsCount;
				_widthView = new TextLine(_columnsCount.ToString(), _style, _manager);
			}
		}

		public int RowsCount
		{
			get
			{
				return _rowsCount;
			}
			set
			{
				_rowsCount = value > MinRowsCount ? value : MinRowsCount;
				_heightView = new TextLine(_rowsCount.ToString(), _style, _manager);
			}
		}

		public int X
		{
			get
			{
				return _bounds.X;
			}

			set
			{
				_bounds.X = value;
				_buttons.X = value + _margin.X;
			}
		}

		public int Y
		{
			get
			{
				return _bounds.Y;
			}

			set
			{
				_bounds.Y = value;
				_buttons.Y = value + _margin.Y;
			}
		}

		public AxisAlignedBoundingBox Bounds
		{
			get
			{
				return _bounds;
			}
		}

		public int Width
		{
			get
			{
				return _bounds.Width;
			}
		}

		public int Height
		{
			get
			{
				return _bounds.Height;
			}
		}

		#endregion // Properties

		public MapSizeView(int rowsCount, int columnsCount, CoordinateManager manager, Action onApply, Action onCancel)
		{
			if (manager == null)
			{
				throw new ArgumentNullException("manager");
			}

			_manager = manager;
			//var font = new Engine.Text.Font("Fonts/Consolas.txt", _manager);
			var font = new Engine.Text.Font("Fonts/Courier.txt", _manager);
			var lightColor = Color.FromArgb(0, 255, 196);
			var darkColor = Color.FromArgb(6, 122, 95);
			//_style = new TextStyle(font, darkColor, 0.5f);
			_style = new TextStyle(font, darkColor, 2.0f);
			ColumnsCount = columnsCount;
			RowsCount = rowsCount;

			var rect = new Rectangle(0, 0, 350, 350);
			_bounds = new AxisAlignedBoundingBox(rect);
			_background = new Sprite(Texture.FromColor(Color.FromArgb(230, Color.WhiteSmoke)), rect, _manager);
			_widthTitle = new TextLine("Width", _style, _manager);
			_heightTitle = new TextLine("Height", _style, _manager);
			_title = new TextLine("Select map size", _style, _manager);
			_margin = new Point(30, 130);

			var texture = Texture.FromImage("textures/editor_buttons.png");
			var buttonSize = 32;
			var inc = new Sprite(texture, new Rectangle(224, 0, buttonSize, buttonSize), _manager);
			var dec = new Sprite(texture, new Rectangle(224, buttonSize, buttonSize, buttonSize), _manager);
			var right = Width - 2 * _margin.X - buttonSize;
			var down = 90;
			var bottom = down + 85;
			var apply = new TextLine("Apply", _style, _manager);
			var cancel = new TextLine("Cancel", _style, _manager);

			_buttons = new ControlContainer();

			_buttons.Add(new Button(new DelegateCommand(() => ColumnsCount--), 0, 0, dec, dec, _manager));
			_buttons.Add(new Button(new DelegateCommand(() => ColumnsCount++), right, 0, inc, inc, _manager));
			_buttons.Add(new Button(new DelegateCommand(() => RowsCount--), 0, down, dec, dec, _manager));
			_buttons.Add(new Button(new DelegateCommand(() => RowsCount++), right, down, inc, inc, _manager));
			_buttons.Add(new Button(new DelegateCommand(() => { Active = false; onApply(); }), 25, bottom, apply, new TextLine(apply, lightColor), _manager));
			_buttons.Add(new Button(new DelegateCommand(() => { Active = false; onCancel(); }), Width - 2 * _margin.X - cancel.Width - 20, bottom, cancel, new TextLine(cancel, lightColor), _manager));

			_buttons.X = _margin.X;
			_buttons.Y = _margin.Y;
		}

		public void OnClick(object sender, MouseButtonEventArgs e)
		{
			if (Active)
			{
				_buttons.OnClick(sender, e);
			}
		}

		public void OnHover(object sender, MouseMoveEventArgs e)
		{
			if (Active)
			{
				_buttons.OnHover(sender, e);
			}
		}

		public void Render()
		{
			GL.PushMatrix();

			GL.LoadIdentity();
			GL.Translate(-1, 1, 0);

			_manager.Translate(X, - Y - Height);
			_background.Render();

			GL.PushMatrix();
			_manager.Translate((Width - _title.Width) / 2, Height - _title.Height - 20);
			_title.Render();
			GL.PopMatrix();
			
			GL.PushMatrix();
			_manager.Translate((Width - _widthView.Width) / 2, Height - _margin.Y - _widthView.Height);
			_widthView.Render();
			GL.PopMatrix();

			GL.PushMatrix();
			_manager.Translate((Width - _widthTitle.Width) / 2, Height - _margin.Y - _widthView.Height + _widthView.Height + 10);
			_widthTitle.Render();
			GL.PopMatrix();

			GL.PushMatrix();
			_manager.Translate((Width - _heightView.Width) / 2, Height - _margin.Y - _heightView.Height - 90);
			_heightView.Render();
			GL.PopMatrix();

			GL.PushMatrix();
			_manager.Translate((Width - _heightTitle.Width) / 2, Height - _margin.Y - _heightView.Height - 90 + _heightView.Height + 10);
			_heightTitle.Render();
			GL.PopMatrix();

			_buttons.Render();

			GL.PopMatrix();
		}
	}
}
