﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Drawing;
using Engine.Graphics;
using Engine.MapRender;

namespace Engine.Xml
{
	public static class XmlLoader
	{
		public static readonly string AnimationTagName = "animation";
		public static readonly string RectangleTagName = "rectangle";

		public static Animation LoadAnimation(XmlElement element, Texture texture, CoordinateManager manager)
		{
			var frames = new List<Sprite>();
			foreach (var rect in element.GetElementsByTagName(RectangleTagName).Cast<XmlElement>())
			{
				frames.Add(new Sprite(texture, LoadRectangle(rect), manager));
			}
			int delay;
			if (!int.TryParse(element.GetAttribute("delay"), out delay))
			{
				delay = 0;
			}
			return new Animation(frames, delay);
		}

		public static AdjacentTilesCross LoadAdjacentTilesCross(XmlElement element, Texture texture, CoordinateManager manager)
		{
			var selectors = new Dictionary<string, SpriteSelector>();
			foreach (var item in element.GetElementsByTagName("variant").Cast<XmlElement>())
			{
				selectors.Add(item.GetAttribute("cross"), LoadSpriteSelector(item, texture, manager));
			}
			return new AdjacentTilesCross(selectors);
		}

		public static SpriteSelector LoadSpriteSelector(XmlElement element, Texture texture, CoordinateManager manager)
		{
			var sprites = new List<Sprite>();
			var frequencies = new List<int>();
			foreach (var item in element.GetElementsByTagName(RectangleTagName).Cast<XmlElement>())
			{
				sprites.Add(new Sprite(texture, LoadRectangle(item), manager));
				int temp;
				int.TryParse(item.GetAttribute("frequency"), out temp);
				frequencies.Add(temp);
			}
			return new SpriteSelector(sprites, frequencies);
		}

		public static Rectangle LoadRectangle(XmlElement element)
		{
			int x;
			int y;
			int width;
			int height;
			if (!int.TryParse(element.GetAttribute("x"), out x))
			{
				x = 0;
			}
			if (!int.TryParse(element.GetAttribute("y"), out y))
			{
				y = 0;
			}
			if (!int.TryParse(element.GetAttribute("width"), out width))
			{
				width = 0;
			}
			if (!int.TryParse(element.GetAttribute("height"), out height))
			{
				height = 0;
			}
			return new Rectangle(x, y, width, height);
		}
	}
}
