﻿using System.IO;

namespace Engine.StoredMap
{
	public static class MapWriter
	{
		public static void Write(IDsMap map, string filename)
		{
			using (var file = new StreamWriter(filename))
			{
				file.WriteLine("{0} {1}", map.RowsCount, map.ColumnsCount);
				for (int i = 0; i < map.RowsCount; i++)
				{
					for (int j = 0; j < map.ColumnsCount - 1; j++)
					{
						file.Write("{0} ", map.Cell(i, j));
					}
					file.WriteLine(map.Cell(i, map.ColumnsCount - 1));
				}
			}
		}
	}
}
