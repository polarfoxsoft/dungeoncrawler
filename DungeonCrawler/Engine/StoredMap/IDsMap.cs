﻿using System;


namespace Engine.StoredMap
{
	public class IDsMap
	{
		#region Variables
		
		private int _columnsCount;
		private int _rowsCount;
		readonly private int[][] _matrix;

		#endregion

		#region Properties
		
		public int ColumnsCount
		{
			get { return _columnsCount; }
			private set
			{
				if (value > 0)
				{
					_columnsCount = value;
				}
				else
				{
					throw new ArgumentOutOfRangeException();
				}
			}
		}

		public int RowsCount
		{
			get { return _rowsCount; }
			private set
			{
				if (value > 0)
				{
					_rowsCount = value;
				}
				else
				{
					throw new ArgumentOutOfRangeException();
				}
			}
		}

		#endregion

		#region methods

		public IDsMap(int rowsCount, int columnsCount, int[][] matrix)
		{
			ColumnsCount = columnsCount;
			RowsCount = rowsCount;
			_matrix = matrix;
		}

		public IDsMap(int rowsCount, int columnsCount)
			:this(rowsCount, columnsCount, GenerateMatrix(rowsCount, columnsCount))
		{
		}

		public int Cell(int rowIndex, int columnIndex)
		{
			if (rowIndex < 0 || rowIndex >= RowsCount)
			{
				throw new ArgumentOutOfRangeException("rowIndex");
			}
			if (columnIndex < 0 || columnIndex >= ColumnsCount)
			{
				throw new ArgumentOutOfRangeException("columnIndex");
			}
			return _matrix[rowIndex][columnIndex];
		}

		public void ChangeCell(int rowIndex, int columnIndex, int id)
		{
			if (rowIndex < 0 || rowIndex >= RowsCount)
			{
				throw new ArgumentOutOfRangeException("rowIndex");
			}
			if (columnIndex < 0 || columnIndex >= ColumnsCount)
			{
				throw new ArgumentOutOfRangeException("columnIndex");
			}
			_matrix[rowIndex][columnIndex] = id;
		}

		public void CreateFrameOf(int id)
		{
			for (int i = 0; i < RowsCount; i++)
			{
				_matrix[i][0] = id;
				_matrix[i][ColumnsCount - 1] = id;
			}
			for (int j = 0; j < ColumnsCount; j++)
			{
				_matrix[0][j] = id;
				_matrix[RowsCount - 1][j] = id;
			}
		}

		static private int[][] GenerateMatrix(int rowsCount, int columnsCount)
		{
			int[][] matrix = new int[rowsCount][];
			for (int i = 0; i < rowsCount; i++)
			{
				matrix[i] = new int[columnsCount];
				for (int j = 0; j < columnsCount; j++)
				{
					matrix[i][j] = 0;
				}
			}
			return matrix;
		}

		#endregion
	}
}
