﻿using System;
using System.IO;

namespace Engine.StoredMap
{
	public static  class MapReader
	{
		public static IDsMap Read(string filename)
		{
			int columnsCount = 0;
			int rowsCount = 0;
			int[][] matrix = null;

			using (var file = new StreamReader(FileStorage.StorageManager.Instance.GetFile(filename)))
			{
				var sizes = file.ReadLine().Split(' ');
				if (!int.TryParse(sizes[0], out rowsCount))
				{
					throw new ArgumentOutOfRangeException();
				}
				if (!int.TryParse(sizes[1], out columnsCount))
				{
					throw new ArgumentOutOfRangeException();
				}

				matrix = new int[rowsCount][];
				for (int i = 0; i < rowsCount; i++)
				{
					var cellIDs = file.ReadLine().Split(' ');
					matrix[i] = new int[columnsCount];
					for (int j = 0; j < columnsCount; j++)
					{
						if (!int.TryParse(cellIDs[j], out matrix[i][j]))
						{
							throw new ArgumentOutOfRangeException();
						}
					}
				}
			}

			return new IDsMap(rowsCount, columnsCount, matrix);
		}
	}
}
