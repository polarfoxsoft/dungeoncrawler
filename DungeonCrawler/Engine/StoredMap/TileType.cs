﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Engine.StoredMap
{
	public enum TileType
	{
		Surface,
		Wall,
		Enterance,
		Exit,
		Chest,
		Enemy,
		Boss
	}

	public static class TileTypeConverter
	{
		private static readonly Dictionary<string, TileType> _types;

		static TileTypeConverter()
		{
			_types = new Dictionary<string, TileType>();
			_types.Add("0", TileType.Surface);
			_types.Add("1", TileType.Wall);
			_types.Add("2", TileType.Enterance);
			_types.Add("3", TileType.Exit);
			_types.Add("4", TileType.Chest);
			_types.Add("8", TileType.Enemy);
			_types.Add("9", TileType.Boss);
		}

		public static string ToString(TileType type)
		{
			switch (type)
			{
				case TileType.Surface:
					return "0";
				case TileType.Wall:
					return "1";
				case TileType.Enterance:
					return "2";
				case TileType.Exit:
					return "3";
				case TileType.Chest:
					return "4";
				case TileType.Enemy:
					return "8";
				case TileType.Boss:
					return "9";
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public static TileType FromString(string type)
		{
			TileType result;
			if (_types.TryGetValue(type, out result))
			{
				return result;
			}
			throw new ArgumentOutOfRangeException();
		}
	}
}
