﻿using System.Drawing;
using System.Collections.Generic;
using OpenTK.Graphics.OpenGL;
using Engine.Graphics;
using Engine.Core.Collisions;
using Engine.Text;
using Engine.Core.Items;

namespace Engine.Core
{
	public class Player : Character
	{
		private Stack<HealingPack> _healing;
		private TextLine _healingCount;
		private TextLine _healingTip;
		private TextStyle _healingCountStyle;

		public Player(int speed, int maxHitPoints, int defense, int invulnerability, TextStyle hpStyle, AnimationManager legs, AnimationManager walk, AnimationManager attack, AxisAlignedBoundingBox aabb, CoordinateManager manager, AxisAlignedBoundingBox attackArea, int attackDistance, int damage, int shootDamage, int shootCoolDown)
			: base(speed, maxHitPoints, defense, invulnerability, hpStyle, legs, walk, attack, aabb, manager, attackArea, attackDistance, damage, shootDamage, shootCoolDown)
		{
			_healing = new Stack<HealingPack>();
			var hp = new Sprite(Texture.FromColor(Color.Red), new Rectangle(0, 0, 32, 32), _coordManager);
			for (int i = 0; i < 10; i++)
			{
				_healing.Push(new SmallHealingPack());
			}
			//_healingCountStyle = new TextStyle(_hpStyle.Font, _hpStyle.Color, 0.5f);
			//_healingTip = new TextLine("(H)", _healingCountStyle.Font, _coordManager, _healingCountStyle.Color, 0.3f);
			_healingCountStyle = new TextStyle(_hpStyle.Font, _hpStyle.Color, 2.0f);
			_healingTip = new TextLine("", _healingCountStyle.Font, _coordManager, _healingCountStyle.Color, 2.0f);
			UpdateHealingCount();
		}

		public void Heal()
		{
			if (_healing.Count != 0 && CurrentHitPoints != MaxHitPoints)
			{
				Heal(_healing.Pop().Use());
				UpdateHealingCount();
			}
		}

		public void PickUp(Item item)
		{
			if (item is HealingPack)
			{
				item.PickUp();
				_healing.Push((HealingPack)item);
				UpdateHealingCount();
			}
		}

		public override void Render()
		{
			base.Render();
			GL.PushMatrix();
			GL.LoadIdentity();
			GL.Translate(-1, 1, 0);
			_coordManager.Translate(10, -SmallHealingPack.Sprite.Height - 10);
			SmallHealingPack.Sprite.Render();
			_coordManager.Translate(SmallHealingPack.Sprite.Width + 5, (SmallHealingPack.Sprite.Height - _healingCount.Height) / 2);
			_healingCount.Render();
			_coordManager.Translate(_healingCount.Width + 5, 0);
			_healingTip.Render();
			GL.PopMatrix();
		}

		private void UpdateHealingCount()
		{
			_healingCount = new TextLine(_healing.Count.ToString(), _healingCountStyle, _coordManager);
		}
	}
}
