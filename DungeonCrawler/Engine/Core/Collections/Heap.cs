﻿using System;
using System.Collections.Generic;

namespace Engine.Core.Collections
{
	public class HeapCursor
	{
	}

	public class Heap<TKey, TValue> where TKey : IComparable<TKey>
	{
		private class HeapItem
		{
			private KeyValuePair<TKey, TValue> _keyValuePair;

			public HeapCursor Cursor { get; set; }

			public KeyValuePair<TKey, TValue> KeyValuePair
			{
				get
				{
					return _keyValuePair;
				}
				set
				{
					_keyValuePair = value;
				}
			}

			public TKey Key
			{
				get
				{
					return _keyValuePair.Key;
				}
				set
				{
					_keyValuePair.Key = value;
				}
			}

			public TValue Value
			{
				get
				{
					return _keyValuePair.Value;
				}
				set
				{
					_keyValuePair.Value = value;
				}
			}

			public HeapItem(TKey key, TValue value, HeapCursor cursor)
			{
				Key = key;
				Value = value;
				Cursor = cursor;
			}
		}

		private HeapItem[] _heap;
		private IComparer<TKey> _comparer;

		#region Properties

		public bool Reducible { get; set; }
		public int Capacity { get; private set; }
		public int Count { get; private set; }

		public bool Empty
		{
			get { return Count == 0; }
		}

		#endregion

		public Heap(IComparer<TKey> comparer, int capacity = 10)
		{
			Capacity = capacity;
			_heap = new HeapItem[Capacity];
			Count = 0;
			_comparer = ReferenceEquals(comparer, null) ? Comparer<TKey>.Default : comparer;
			Reducible = true;
		}

		#region Methods

		public void Clear()
		{
			Count = 0;
			if (Reducible)
			{
				Resize(2);
			}
			else
			{
				for (int i = 0; i < Capacity; i++)
				{
					_heap[i] = null;
				}
			}
		}

		public HeapCursor FindKey(TKey key)
		{
			for (int i = 0; i < Count; i++)
			{
				if (_heap[i].Key.Equals(key))
				{
					return _heap[i].Cursor;
				}
			}
			return null;
		}

		public HeapCursor FindValue(TValue value)
		{
			for (int i = 0; i < Count; i++)
			{
				if (_heap[i].Value.Equals(value))
				{
					return _heap[i].Cursor;
				}
			}
			return null;
		}

		public TKey PeekKey()
		{
			return Peek().Key;
		}

		public TValue PeekValue()
		{
			return Peek().Value;
		}

		public KeyValuePair<TKey, TValue> Peek()
		{
			if (Count > 0)
			{
				return _heap[0].KeyValuePair;
			}
			else
			{
				throw new InvalidOperationException("Queue is empty");
			}
		}

		public HeapCursor Enqueue(TKey key, TValue value)
		{
			if (Count == Capacity)
			{
				Resize(Capacity * 2);
			}

			var cursor = new HeapCursor();
			_heap[Count] = new HeapItem(key, value, cursor);
			SiftUp(Count);
			Count++;
			return cursor;
		}

		public KeyValuePair<TKey, TValue> Dequeue()
		{
			if (Count > 0)
			{
				var result = _heap[0].KeyValuePair;
				Count--;
				_heap[0] = _heap[Count];
				SiftDown(0);
				if (Count < Capacity / 4 && Reducible)
				{
					Resize(Capacity / 2);
				}
				return result;
			}
			else
			{
				throw new InvalidOperationException("Queue is empty");
			}
		}

		public void UpdateKey(HeapCursor cursor, TKey key)
		{
			int index = 0;
			while (index < Count && _heap[index].Cursor != cursor)
			{
				index++;
			}
			if (index < Count)
			{
				var oldKey = _heap[index].Key;
				_heap[index].Key = key;
				if (_comparer.Compare(key, oldKey) == 1)
				{
					SiftDown(index);
				}
				else
				{
					SiftUp(index);
				}
			}
		}

		#endregion

		#region PrivateMethods

		private void SiftDown(int i)
		{
			int left = Left(i);
			int right = Right(i);
			int j = i;

			if (left < Count && _comparer.Compare(_heap[j].Key, _heap[left].Key) == 1)
			{
				j = left;
			}

			if (right < Count && _comparer.Compare(_heap[j].Key, _heap[right].Key) == 1)
			{
				j = right;
			}

			if (j != i)
			{
				Swap(i, j);
				SiftDown(j);
			}
		}

		private void SiftUp(int i)
		{
			int parent = Parent(i);

			while (i > 0 && _comparer.Compare(_heap[i].Key, _heap[parent].Key) != 1)
			{
				Swap(i, parent);
				i = Parent(i);
				parent = Parent(i);
			}
		}

		private void Resize(int newCapacity)
		{
			var buffer = _heap;
			_heap = new HeapItem[newCapacity];
			if (Capacity < newCapacity)
			{
				buffer.CopyTo(_heap, 0);
			}
			else
			{
				for (int i = 0; i < Count; i++)
				{
					_heap[i] = buffer[i];
				}
			}
			Capacity = newCapacity;
		}

		private void Swap(int index1, int index2)
		{
			var buffer = _heap[index1];
			_heap[index1] = _heap[index2];
			_heap[index2] = buffer;
		}

		private int Left(int i)
		{
			return 2 * i + 1;
		}

		private int Right(int i)
		{
			return 2 * i + 2;
		}

		private int Parent(int i)
		{
			return (i - 1) / 2;
		}

		#endregion
	}
}
