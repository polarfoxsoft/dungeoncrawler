﻿using System;
using Engine.Core.Collisions;

namespace Engine.Core
{
	public abstract class GameObject
	{
		private int _x;
		private int _y;

		public event Action XChanged;
		public event Action YChanged;

		public int X
		{
			get
			{
				return _x;
			}
			set
			{
				_x = value;
				var handler = XChanged;
				if (handler != null)
				{
					handler();
				}
			}
		}

		public int Y
		{
			get
			{
				return _y;
			}
			set
			{
				_y = value;
				var handler = YChanged;
				if (handler != null)
				{
					handler();
				}
			}
		}

		public abstract void Render();

		public static Collision CheckAabbCollision(GameObject first, GameObject second)
		{
			if (first == null || second == null)
			{
				return new Collision();
			}

			return AxisAlignedBoundingBox.CheckCollision(first.GetAabb(), second.GetAabb());
		}

		public virtual AxisAlignedBoundingBox GetAabb()
		{
			return null;
		}
	}
}
