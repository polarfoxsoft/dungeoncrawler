﻿using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Input;

namespace Engine.Core
{
	public class InputManager
	{
		private HashSet<Key> _keysDown;
		private HashSet<Key> _keysDownLast;
		private HashSet<MouseButton> _mouseButtons;
		private HashSet<MouseButton> _mouseButtonsLast;

		public InputManager(GameWindow window)
		{
			if (window == null)
			{
				throw new ArgumentNullException();
			}

			_keysDown = new HashSet<Key>();
			_keysDownLast = new HashSet<Key>();
			_mouseButtons = new HashSet<MouseButton>();
			_mouseButtonsLast = new HashSet<MouseButton>();

			window.KeyDown += OnKeyDown;
			window.KeyUp += OnKeyUp;
			window.MouseDown += OnMouseDown;
			window.MouseUp += OnMouseUp;
		}

		#region Methods

		public void Update()
		{
			_keysDownLast = new HashSet<Key>(_keysDown);
			_mouseButtonsLast = new HashSet<MouseButton>(_mouseButtons);
		}

		public bool KeyPressed(Key key)
		{
			return _keysDown.Contains(key) && !_keysDownLast.Contains(key);
		}

		public bool KeyReleased(Key key)
		{
			return !_keysDown.Contains(key) && _keysDownLast.Contains(key);
		}

		public bool KeyDown(Key key)
		{
			return _keysDown.Contains(key);
		}

		public bool MousePressed(MouseButton button)
		{
			return _mouseButtons.Contains(button) && !_mouseButtonsLast.Contains(button);
		}

		public bool MouseReleased(MouseButton button)
		{
			return !_mouseButtons.Contains(button) && _mouseButtonsLast.Contains(button);
		}

		public bool MouseDown(MouseButton button)
		{
			return _mouseButtons.Contains(button);
		}

		#endregion // Methods

		private void OnMouseUp(object sender, MouseButtonEventArgs e)
		{
			_mouseButtons.Remove(e.Button);
		}

		private void OnMouseDown(object sender, MouseButtonEventArgs e)
		{
			_mouseButtons.Add(e.Button);
		}

		private void OnKeyUp(object sender, KeyboardKeyEventArgs e)
		{
			_keysDown.Remove(e.Key);
		}

		private void OnKeyDown(object sender, KeyboardKeyEventArgs e)
		{
			_keysDown.Add(e.Key);
		}
	}
}
