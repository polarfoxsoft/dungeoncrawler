﻿using System;

namespace Engine.Core
{
	public abstract class MoveableObject : GameObject
	{
		public int Speed { get; protected set; }
		public Direction Direction { get; protected set; }
		public bool Moveable { get; protected set; }

		protected MoveableObject(int speed)
		{
			Speed = speed;
			Direction = new Direction();
		}

		public void UpdateX()
		{
			if (Moveable)
			{
				X += Speed * Direction.ToVector().X;
			}
		}

		public void UpdateY()
		{
			if (Moveable)
			{
				Y += Speed * Direction.ToVector().Y;
			}
		}
	}
}
