﻿using System;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics;
using Engine.Core.Collisions;
using Engine.Graphics;

namespace Engine.Core
{
	public class Projectile : GameObject
	{
		protected CoordinateManager _manager;
		protected int _contactDamage;
		protected AxisAlignedBoundingBox _aabb;
		protected AnimationManager _view;
		private AnimationManager _animations;
		private Point _spriteTranslation;
		private bool _paused;
		private Vector2 _position;
		private Vector2 _speed;

		public bool Active { get; private set; }

		public bool AnimationPlaying
		{
			get
			{
				return _animations.CurrentAnimation.IsPlaying;
			}
		}

		public Projectile(int damage, Point position, Point direction, int speed, Size aabbSize, AnimationManager animations, Point animationTranslation, CoordinateManager manager)
		{
			if (animations == null)
			{
				throw new ArgumentNullException("animations");
			}
			if (manager == null)
			{
				throw new ArgumentNullException("manager");
			}

			_animations = new AnimationManager(animations);
			_animations.SetAnimation("flight");
			_spriteTranslation = animationTranslation;
			_contactDamage = damage;
			X = position.X;
			Y = position.Y;
			_position = new Vector2(X, Y);
			_aabb = new AxisAlignedBoundingBox(position.X, position.Y, aabbSize.Width, aabbSize.Height);
			_speed = new Vector2(direction.X, direction.Y);
			_speed.NormalizeFast();
			_speed = Vector2.Multiply(_speed, speed);
			_manager = manager;
			Active = true;

			XChanged += () => _aabb.X = X;
			YChanged += () => _aabb.Y = Y;
			_animations.CurrentAnimation.Play();
		}

		public virtual void UpdateX()
		{
			if (Active)
			{
				_position.X += _speed.X;
				X = (int)_position.X;
			}
		}

		public virtual void UpdateY()
		{
			if (Active)
			{
				_position.Y += _speed.Y;
				Y = (int)_position.Y;
			}
		}

		public void Pause()
		{
			if (Active || AnimationPlaying)
			{
				_paused = true;
				_animations.CurrentAnimation.Pause();
			}
		}

		public void Unpause()
		{
			_paused = false;
			_animations.CurrentAnimation.Unpause();
		}

		public override void Render()
		{
			GL.PushMatrix();
			_manager.Translate(X + _spriteTranslation.X, Y + _spriteTranslation.Y);
			if (AnimationPlaying || _paused) _animations.CurrentAnimation.Render();
			GL.PopMatrix();
		}

		public void Destroy()
		{
			if (Active)
			{
				Active = false;
				OnDestroyed();
			}
		}

		public override AxisAlignedBoundingBox GetAabb()
		{
			return _aabb;
		}

		public void Apply(Character target)
		{
			if (Active && CheckAabbCollision(this, target).Detected)
			{
				target.TakeDamage(_contactDamage);
				OnDealingDamage();
			}
		}

		protected virtual void OnDealingDamage()
		{
			Destroy();
		}

		protected virtual void OnDestroyed()
		{
			_animations.CurrentAnimation.Stop();
			_animations.SetAnimation("destruction");
			_animations.CurrentAnimation.PlayOnce();
		}
	}
}
