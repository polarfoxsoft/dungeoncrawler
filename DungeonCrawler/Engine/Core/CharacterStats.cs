﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Engine.Core
{
	public class CharacterStats
	{
		public int Speed { get; set; }
		public int Defense { get; set; }
		public int MaxHP { get; set; }
	}
}
