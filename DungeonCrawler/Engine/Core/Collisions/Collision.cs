﻿using System;

namespace Engine.Core.Collisions
{
	public class Collision
	{
		public bool Detected { get; private set; }
		public int X { get; private set; }
		public int Y { get; private set; }

		public Collision()
			: this(0, 0)
		{
		}

		public Collision(int x, int y)
		{
			X = x;
			Y = y;
			Detected = x != 0 || y != 0;
		}
	}
}
