﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;

namespace Engine.Core.Collisions
{
	public class AxisAlignedBoundingBox
	{
		public int X { get; set; }
		public int Y { get; set; }
		public int Width { get; set; }
		public int Height { get; set; }

		public AxisAlignedBoundingBox(Rectangle rect)
			: this(rect.X, rect.Y, rect.Width, rect.Height)
		{
		}

		public AxisAlignedBoundingBox(int x, int y, int width, int height)
		{
			X = x;
			Y = y;
			Height = height;
			Width = width;
		}

		public static Collision CheckCollision(AxisAlignedBoundingBox first, AxisAlignedBoundingBox second)
		{
			if (first == null || second == null)
			{
				return new Collision();
			}

			return first.CheckCollision(second);
		}

		public static Collision CheckCollision(AxisAlignedBoundingBox aabb, int x, int y)
		{
			if (aabb == null)
			{
				return new Collision();
			}

			return aabb.CheckCollision(x, y);
		}

		public static AxisAlignedBoundingBox GetBounds(AxisAlignedBoundingBox first, AxisAlignedBoundingBox second)
		{
			if (first == null)
			{
				if (second == null)
				{
					return null;
				}
				return second;
			}
			if (second == null)
			{
				return first;
			}

			var x = Math.Min(first.X, second.X);
			var y = Math.Min(first.Y, second.Y);
			var width = Math.Max(first.X + first.Width, second.X + second.Width) - x;
			var height = Math.Max(first.Y + first.Height, second.Y + second.Height) - y;
			return new AxisAlignedBoundingBox(x, y, width, height);
		}

		public static AxisAlignedBoundingBox GetBounds(IEnumerable<AxisAlignedBoundingBox> boxes)
		{
			var x = boxes.Min(b => b.X);
			var y = boxes.Min(b => b.Y);
			var width = boxes.Max(b => b.X + b.Width) - x;
			var height = boxes.Max(b => b.Y + b.Height) - y;
			return new AxisAlignedBoundingBox(x, y, width, height);
		}

		public Collision CheckCollision(AxisAlignedBoundingBox other)
		{
			if (other == null)
			{
				return new Collision();
			}

			int minX = 0;
			int minY = 0;

			var shift = X - other.X;
			if (shift >= other.Width || -shift >= Width)
			{
				return new Collision();
			}
			else
			{
				int a = Width + X - other.X;
				int b = X - (other.X + other.Width);
				minX = a * a < b * b ? a : b;
			}

			shift = Y - other.Y;
			if (shift >= other.Height || -shift >= Height)
			{
				return new Collision();
			}
			else
			{
				int a = Height + Y - other.Y;
				int b = Y - (other.Y + other.Height);
				minY = a * a < b * b ? a : b;
			}

			return new Collision(minX, minY);
		}

		public Collision CheckCollision(int x, int y)
		{
			var localX = x - X;
			var localY = y - Y;
			if (localX > Width || localX < 0 || localY > Height || localY < 0)
			{
				return new Collision();
			}
			return new Collision(localX > Width / 2 ? Width - localX : -localX, localY > Height / 2 ? Height - localY : -localY);
		}
	}
}
