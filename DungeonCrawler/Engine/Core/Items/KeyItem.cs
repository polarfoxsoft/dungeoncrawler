﻿using System;
using OpenTK.Graphics.OpenGL;
using Engine.Core.Collisions;
using Engine.Graphics;
using System.Drawing;

namespace Engine.Core.Items
{
	public class KeyItem : Item, IEquatable<KeyItem> 
	{
		private static readonly Random _random;
		private static Sprite _defaultSprite;

		private readonly int _key;
		private static CoordinateManager _manager;
		private Sprite _sprite;
		private AxisAlignedBoundingBox _bounds;

		public bool Used { get; private set; }

		static KeyItem()
		{
			_random = new Random();
		}

		public KeyItem(CoordinateManager manager)
			: this(_defaultSprite, manager)
		{
		}

		public KeyItem(Sprite sprite, CoordinateManager manager)
		{
			_manager = manager;
			_sprite = sprite;
			_bounds = new AxisAlignedBoundingBox(0, 0, _sprite.Width, _sprite.Height);
			XChanged += () => _bounds.X = X;
			YChanged += () => _bounds.Y = Y;
			_key = _random.Next();
		}

		public static void Init(Sprite defaultSprite)
		{
			_defaultSprite = defaultSprite;
		}

		public int Use()
		{
			if (OnGround)
			{
				return 0;
			}
			if (Used)
			{
				return 0;
			}
			else
			{
				Used = true;
				return _key;
			}
		}

		public override void Render()
		{
			GL.PushMatrix();
			if (OnGround)
			{
				_manager.Translate(X, Y);
			}
			_sprite.Render();
			GL.PopMatrix();
		}

		public override AxisAlignedBoundingBox GetAabb()
		{
			if (OnGround)
			{
				return _bounds;
			}
			return null;
		}

		public bool Equals(KeyItem other)
		{
			if (other == null || _key != other._key)
			{
				return false;
			}
			return true;
		}
	}
}
