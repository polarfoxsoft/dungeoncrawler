﻿using Engine.Graphics;
using Engine.Core.Collisions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK.Graphics.OpenGL;

namespace Engine.Core.Items
{
	public class HealingPack : Item
	{
		private CoordinateManager _manager;
		private int _healingPoints;
		private Sprite _sprite;
		private AxisAlignedBoundingBox _bounds;

		public bool Used { get; private set; }

		public HealingPack(int healingPoints, Sprite sprite, CoordinateManager manager)
		{
			_manager = manager;
			_healingPoints = healingPoints;
			_sprite = sprite;
			_bounds = new AxisAlignedBoundingBox(0, 0, _sprite.Width, _sprite.Height);
			XChanged += () => _bounds.X = X;
			YChanged += () => _bounds.Y = Y;
		}

		public int Use()
		{
			if (OnGround)
			{
				return 0;
			}
			if (Used)
			{
				return 0;
			}
			else
			{
				Used = true;
				return _healingPoints;
			}
		}

		public override void Render()
		{
			GL.PushMatrix();
			if (OnGround)
			{
				_manager.Translate(X, Y);
			}
			_sprite.Render();
			GL.PopMatrix();
		}

		public override AxisAlignedBoundingBox GetAabb()
		{
			if (OnGround)
			{
				return _bounds;
			}
			return null;
		}
	}
}
