﻿using Engine.MapRender;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Engine.Core.Items
{
	public abstract class Item : GameObject
	{
		//public int ID { get; protected set; }
		public bool OnGround { get; protected set; }

		public bool CheckCollision(Player player)
		{
			if (OnGround)
			{
				return CheckAabbCollision(this, player).Detected;
			}
			return false;
		}

		public void Drop(Map map, Point location)
		{
			if (!OnGround)
			{
				OnGround = true;

				X = location.X;
				Y = location.Y;

				var collision = map.CheckCollision(this);
				if (collision.Detected)
				{
					X += collision.X;
					Y += collision.Y;
				}
			}
		}
		
		public void PickUp()
		{
			OnGround = false;
		}
	}
}
