﻿using Engine.MapRender;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Engine.Core.Items
{
	public abstract class InventoryItem
	{
		public bool InInventory { get; protected set; }

		//public Item Drop(Map map, Point location)
		//{
		//	if (InInventory)
		//	{
		//		var item = ToItem();
		//		item.X = location.X;
		//		item.Y = location.Y;

		//		var collision = map.CheckCollision(item);
		//		if (collision.Detected)
		//		{
		//			item.X += collision.X;
		//		}
		//		collision = map.CheckCollision(item);
		//		if (collision.Detected)
		//		{
		//			item.Y += collision.Y;
		//		}

		//		InInventory = false;
		//	}
		//}

		protected abstract Item ToItem();
	}
}
