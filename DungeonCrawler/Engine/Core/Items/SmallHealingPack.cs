﻿using Engine.Graphics;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Engine.Core.Items
{
	public class SmallHealingPack : HealingPack
	{
		private static CoordinateManager _manager;

		public static Sprite Sprite { get; private set; }
		public static int Points { get; private set; }

		public static void Init(CoordinateManager manager)
		{
			_manager = manager;
			Sprite = new Sprite(Texture.FromImage("textures/h1.png"), new Rectangle(0, 0, 64, 64), manager);
			Points = 50;
		}

		public SmallHealingPack() : base(Points, Sprite, _manager)
		{
		}
	}
}
