﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Drawing;
using OpenTK.Graphics;
using Engine.Graphics;
using Engine.Text;
using Engine.Core.Collisions;

namespace Engine.Core
{
	public class Character : MoveableObject
	{
		#region Fields

		protected static readonly Dictionary<Point, string> DirectionNames;
		protected static readonly string StandPrefix = "stand_";
		protected static readonly string DeadPrefix = "dead_";

		private int _maxHP;
		private int _currentHP;
		private int _defense;
		private int _invulnerability;
		private int _damage;
		private int _shootDamage;
		private bool _alive;
		private bool _paused;
		protected TextLine _hpView;
		protected TextStyle _hpStyle;
		protected CoordinateManager _coordManager;
		protected AnimationManager _legs;
		protected AnimationManager _walk;
		protected AnimationManager _attack;
		protected AxisAlignedBoundingBox _bounds;
		protected Point _lastDirection;
		protected Point _attackDirection;
		protected Sprite _attackAreaSprite;

		private Stopwatch _invulnerabilityStopwatch;
		private Stopwatch _shootStopwatch;
		private int _shootCoolDown;
		private AxisAlignedBoundingBox _attackArea;
		private int _attackDistance;
		private Point _lastAttackDirection;
		private HashSet<Character> _damaged;

		#endregion // Fields

		public event Action Killed;

		#region Properties

		public int Width { get; protected set; }
		public int Height { get; protected set; }

		public bool Alive
		{
			get
			{
				return _alive;
			}
			protected set
			{
				var killed = _alive && !value;
				_alive = value;
				Moveable = _alive;
				if (killed)
				{
					Killed?.Invoke();
				}
			}
		}

		public int MaxHitPoints
		{
			get
			{
				return _maxHP;
			}
			protected set
			{
				_maxHP = value > 0 ? value : 0;
			}
		}

		public int CurrentHitPoints
		{
			get
			{
				return _currentHP;
			}
			protected set
			{
				if (value > MaxHitPoints)
				{
					_currentHP = MaxHitPoints;
				}
				else if (value < 0)
				{
					_currentHP = 0;
				}
				else
				{
					_currentHP = value;
				}

				if (_currentHP == 0)
				{
					Alive = false;
				}
				_hpView = new TextLine(string.Format("{0}/{1}", _currentHP, _maxHP), _hpStyle, _coordManager);
			}
		}

		public int Defense
		{
			get
			{
				return _defense;
			}
			protected set
			{
				if (value > 100)
				{
					_defense = 100;
				}
				else if (value < 0)
				{
					_defense = 0;
				}
				else
				{
					_defense = value;
				}
			}
		}

		#endregion // Properties

		static Character()
		{
			DirectionNames = new Dictionary<Point, string>();
			DirectionNames.Add(new Point(-1, 0), "left");
			DirectionNames.Add(new Point(1, 0), "right");
			DirectionNames.Add(new Point(0, -1), "down");
			DirectionNames.Add(new Point(0, 1), "up");
			DirectionNames.Add(new Point(1, -1), "down");
			DirectionNames.Add(new Point(1, 1), "up");
			DirectionNames.Add(new Point(-1, -1), "down");
			DirectionNames.Add(new Point(-1, 1), "up");
			DirectionNames.Add(new Point(0, 0), "down");
		}

		public Character(int speed, int maxHP, int defense, int invulnerability, TextStyle hpStyle, AnimationManager legs, AnimationManager walk, AnimationManager attack, AxisAlignedBoundingBox bounds, CoordinateManager manager, AxisAlignedBoundingBox attackArea, int attackDistance, int damage, int shootDamage, int shootCoolDown)
			: base(speed)
		{
			if (hpStyle == null)
			{
				throw new ArgumentNullException("hpStyle");
			}
			if (manager == null)
			{
				throw new ArgumentNullException("manager");
			}

			_invulnerability = invulnerability;
			_invulnerabilityStopwatch = new Stopwatch();
			_hpStyle = hpStyle;
			_coordManager = manager;
			X = 0;
			Y = 0;
			Spawn(maxHP, defense);
			_bounds = bounds;
			if (_bounds != null)
			{
				XChanged += () => _bounds.X = X;
				YChanged += () => _bounds.Y = Y;
			}

			_lastDirection = new Point(0, -1);
			_attackDirection = new Point(0, -1);

			_legs = legs;
			_walk = walk;
			_attack = attack;
			Killed += SetDeadAnimation;
			Width = _walk.CurrentAnimation.Width;
			Height = _walk.CurrentAnimation.Height;

			_attackDistance = attackDistance;
			_attackArea = attackArea;
			_damage = damage;
			XChanged += () => _attackArea.X = AttackAreaX();
			YChanged += () => _attackArea.Y = AttackAreaY();
			_shootStopwatch = new Stopwatch();
			_shootCoolDown = shootCoolDown;
			_shootDamage = shootDamage;

			_attackAreaSprite = new Sprite(Texture.FromColor(Color.FromArgb(80, 200, 200, 0)), _attackArea.Height, _attackArea.Width, _coordManager);
		}

		#region Methods

		protected void Spawn(int maxHitPoints, int defense)
		{
			Alive = true;
			MaxHitPoints = maxHitPoints;
			CurrentHitPoints = MaxHitPoints;
			Defense = defense;
		}

		public void Pause()
		{
			_paused = true;
			_legs.CurrentAnimation.Pause();
			_walk.CurrentAnimation.Pause();
			_attack.CurrentAnimation.Pause();
		}

		public void Unpause()
		{
			_paused = false;
			_legs.CurrentAnimation.Unpause();
			_walk.CurrentAnimation.Unpause();
			_attack.CurrentAnimation.Unpause();
		}

		public void TakeDamage(int damage)
		{
			if (Alive && !_invulnerabilityStopwatch.IsRunning || _invulnerabilityStopwatch.ElapsedMilliseconds >= _invulnerability)
			{
				CurrentHitPoints -= (int)(damage * (1 - Defense / 100.0));
				_invulnerabilityStopwatch.Restart();
			}
		}

		public void Heal(int points)
		{
			if (Alive)
			{
				CurrentHitPoints += points;
			}
		}

		public override AxisAlignedBoundingBox GetAabb()
		{
			return _bounds;
		}
		
		public override void Render()
		{
			if (!_paused)
			{
				SetAnimation();
			}
			_legs.CurrentAnimation.Render();
			if (!_attack.CurrentAnimation.IsPlaying)
			{
				_walk.CurrentAnimation.Render();
			}
			else
			{
				_attack.CurrentAnimation.Render();
			}

			if (Alive)
			{
				RenderHpView();
			}

			//RenderAttackArea();
		}

		protected void RenderAttackArea()
		{
			if (_attack.CurrentAnimation.IsPlaying)
			{
				GL.PushMatrix();
				_coordManager.Translate(_attackArea.X - X, _attackArea.Y - Y);
				_attackAreaSprite.Render();
				GL.PopMatrix();
			}
		}

		protected void RenderHpView()
		{
			GL.PushMatrix();
			_coordManager.Translate((Width - _hpView.Width) / 2, Height + 10);
			_hpView.Render();
			GL.PopMatrix();
		}

		protected void SetAnimation()
		{
			if (Alive)
			{
				var direction = Direction.ToVector();

				if (!direction.Equals(_lastDirection))
				{
					var temp = direction;
					if (direction.X == 0 && direction.Y == 0)
					{
						_legs.CurrentAnimation.Stop();
						_walk.CurrentAnimation.Stop();
					}
					else if (direction.X != 0 && _lastDirection.X == 0)
					{
						direction.Y = 0;
						_legs.CurrentAnimation.Stop();
						_walk.CurrentAnimation.Stop();
					}
					else if (direction.Y != 0 && _lastDirection.Y == 0)
					{
						direction.X = 0;
						_legs.CurrentAnimation.Stop();
						_walk.CurrentAnimation.Stop();
					}

					string name;
					if (direction.X != 0 || direction.Y != 0)
					{
						DirectionNames.TryGetValue(direction, out name);
						_attackDirection = direction;
					}
					else
					{
						DirectionNames.TryGetValue(_lastDirection, out name);
						name = string.Concat(StandPrefix, name);
					}
					_legs.SetAnimation(name);
					_walk.SetAnimation(name);

					_lastDirection = temp;
					_legs.CurrentAnimation.Play();
					_walk.CurrentAnimation.Play();
				}
			}
		}

		protected void SetAttackAnimation()
		{
			if (Alive && !_attack.CurrentAnimation.IsPlaying)
			{
				string name;
				DirectionNames.TryGetValue(_attackDirection, out name);
				_attack.SetAnimation(name);
				_attack.CurrentAnimation.PlayCycles(1);
				_lastAttackDirection = _attackDirection;
				_attackArea.X = AttackAreaX();
				_attackArea.Y = AttackAreaY();
			}
		}

		private void SetDeadAnimation()
		{
			string name;
			DirectionNames.TryGetValue(_attackDirection, out name);
			name = string.Concat(DeadPrefix, name);
			_legs.CurrentAnimation.Stop();
			_walk.CurrentAnimation.Stop();
			_attack.CurrentAnimation.Stop();
			_legs.SetAnimation(name);
			_walk.SetAnimation(name);
			_legs.CurrentAnimation.PlayOnce();
			_walk.CurrentAnimation.PlayOnce();
		}

		protected void SetAnimationRigidly()
		{
			if (Alive)
			{
				var direction = Direction.ToVector();
				if (direction.Y != 0)
				{
					direction.X = 0;
				}

				if (!direction.Equals(_lastDirection))
				{
					_legs.CurrentAnimation.Stop();
					_walk.CurrentAnimation.Stop();
					string name;

					if (direction.X != 0 || direction.Y != 0)
					{
						DirectionNames.TryGetValue(direction, out name);
						_attackDirection = direction;
					}
					else
					{
						DirectionNames.TryGetValue(_lastDirection, out name);
						name = string.Concat(StandPrefix, name);
					}

					_legs.SetAnimation(name);
					_walk.SetAnimation(name);
					_lastDirection = direction;
					_legs.CurrentAnimation.Play();
					_walk.CurrentAnimation.Play();
				}
			}
		}

		public void Attack()
		{
			if (Alive && !_attack.CurrentAnimation.IsPlaying)
			{
				SetAttackAnimation();
				_damaged = new HashSet<Character>();
			}
		}

		public void Attack(Point direction)
		{
			if (Alive && !_attack.CurrentAnimation.IsPlaying)
			{
				var absX = Math.Abs(direction.X);
				var absY = Math.Abs(direction.Y);
				if (absX > absY)
				{
					direction.X = absX != 0 ? direction.X / absX : 0;
					direction.Y = 0;
				}
				else
				{
					direction.Y = absY != 0 ? direction.Y / absY : 0;
					direction.X = 0;
				}
				_attackDirection = direction;
				SetAttackAnimation();
				_damaged = new HashSet<Character>();
			}
		}

		public void ApplyAttack(Character target)
		{
			if (Alive && _attack.CurrentAnimation.IsPlaying && _damaged != null && !_damaged.Contains(target) && AxisAlignedBoundingBox.CheckCollision(_attackArea, target._bounds).Detected)
			{
				target.TakeDamage(_damage);
				_damaged.Add(target);
			}
		}

		public void Shoot(Point direction, AnimationManager animations, List<Projectile> projectiles)
		{
			if (Alive && (!_shootStopwatch.IsRunning || _shootCoolDown <= _shootStopwatch.ElapsedMilliseconds))
			{
				var size = new Size(animations.CurrentAnimation.Width / 2, animations.CurrentAnimation.Height / 2);
				projectiles.Add(new Projectile(_shootDamage, new Point(X + (Width - size.Width) / 2, Y + (Height - size.Height) / 2), direction, 10, size, animations, new Point(-size.Width / 2, -size.Height / 2), _coordManager));
				_shootStopwatch.Restart();
			}
		}

		private int AttackAreaX()
		{
			return X + (Width - _attackArea.Width) / 2 + _lastAttackDirection.X * _attackDistance;
		}

		private int AttackAreaY()
		{
			return Y + (Height - _attackArea.Height) / 2 + _lastAttackDirection.Y * _attackDistance;
		}

		#endregion // Methods
	}
}
