﻿using System;
using System.Drawing;

namespace Engine.Core
{
	public class Direction
	{
		public bool Left { get; set; }
		public bool Right { get; set; }
		public bool Up { get; set; }
		public bool Down { get; set; }

		public Direction()
		{
			Reset();
		}

		public Point ToVector()
		{
			var x = 0;
			var y = 0;
			if (Left)
			{
				x -= 1;
			}
			if (Right)
			{
				x += 1;
			}
			if (Down)
			{
				y -= 1;
			}
			if (Up)
			{
				y += 1;
			}
			return new Point(x, y);
		}

		public void FromVector(Point vector)
		{
			Left = vector.X < 0;
			Right = vector.X > 0;
			Up = vector.Y > 0;
			Down = vector.Y < 0;
		}

		public void Set(Direction other)
		{
			if (other == null)
			{
				throw new ArgumentNullException("other");
			}
			Left = other.Left;
			Right = other.Right;
			Up = other.Up;
			Down = other.Down;
		}

		public void Reset()
		{
			Left = Right = Up = Down = false;
		}
	}
}
