﻿using System;
using System.Collections.Generic;
using OpenTK.Graphics;
using Engine.Graphics;
using Engine.Core.Collisions;
using Engine.Core.Items;


namespace Engine.Core.MapObjects
{
	public class Chest : MapTile
	{
		private int _key;
		private Sprite _openedSprite;
		private Sprite _closedSprite;
		private AxisAlignedBoundingBox _aabb;
		private List<Item> _items;

		public bool Closed { get; private set; }

		public Chest(Sprite closed, Sprite opened, int x, int y, CoordinateManager manager, int key)
		{
			if (closed == null)
			{
				throw new ArgumentNullException("closed");
			}
			if (opened == null)
			{
				throw new ArgumentNullException("opened");
			}
			if (manager == null)
			{
				throw new ArgumentNullException("manager");
			}

			_closedSprite = closed;
			_openedSprite = opened;
			_manager = manager;
			_key = key;
			X = x;
			Y = y;
			Width = _closedSprite.Width;
			Height = _closedSprite.Height;
			_aabb = new AxisAlignedBoundingBox(X, Y, Width, Height);
			Closed = true;
			SetTranslation();
			_items = new List<Item>();
			for (int i = 0; i < new Random().Next(1, 5); i++)
			{
				_items.Add(new SmallHealingPack());
			}
		}

		public void Open(Player player)
		{
			if (Closed)
			{
				Closed = false;
				foreach (var item in _items)
				{
					player.PickUp(item);
				}
				_items = new List<Item>();
			}
		}

		public void Open(int key)
		{
			if (_key == key)
			{
				Closed = false;
			}
		}

		public override void Render()
		{
			GL.PushMatrix();
			Translate();
			if (Closed)
			{
				_closedSprite.Render();
			}
			else
			{
				_openedSprite.Render();
			}
			GL.PopMatrix();
		}

		public override AxisAlignedBoundingBox GetAabb()
		{
			int halfW = _aabb.Width / 2;
			int halfH = _aabb.Height / 2;
			return new AxisAlignedBoundingBox(_aabb.X + halfW/2, _aabb.Y + halfH/2, halfW, halfH);
		}
	}
}
