﻿using System;
using OpenTK.Graphics;
using Engine.Graphics;
using Engine.MapRender;

namespace Engine.Core.MapObjects
{
	public class Surface : MapTile
	{
		private static Random _random = new Random();

		private Sprite _sprite;

		public Surface(SpriteSelector selector, int x, int y, CoordinateManager manager)
		{
			if (selector == null)
			{
				throw new ArgumentNullException("selector");
			}
			if (manager == null)
			{
				throw new ArgumentNullException("manager");
			}

			_sprite = selector.GetNext();
			_manager = manager;
			X = x;
			Y = y;
			Width = _sprite.Width;
			Height = _sprite.Height;
			SetTranslation();
		}

		public override void Render()
		{
			GL.PushMatrix();
			Translate();
			_sprite.Render();
			GL.PopMatrix();
		}
	}
}
