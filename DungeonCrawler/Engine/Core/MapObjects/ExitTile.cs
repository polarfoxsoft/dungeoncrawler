﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK.Graphics.OpenGL;
using Engine.Core.Collisions;
using Engine.Core.Items;
using Engine.Graphics;

namespace Engine.Core.MapObjects
{
	public class ExitTile : MapTile
	{
		private KeyItem _key;
		private Sprite _openedSprite;
		private Sprite _closedSprite;
		private AxisAlignedBoundingBox _bounds;

		public bool Closed { get; private set; }

		public ExitTile(Sprite closed, Sprite opened, int x, int y, CoordinateManager manager, KeyItem key)
		{
			if (closed == null)
			{
				throw new ArgumentNullException("closed");
			}
			if (opened == null)
			{
				throw new ArgumentNullException("opened");
			}
			if (manager == null)
			{
				throw new ArgumentNullException("manager");
			}

			_closedSprite = closed;
			_openedSprite = opened;
			_manager = manager;
			X = x;
			Y = y;
			Width = _closedSprite.Width;
			Height = _closedSprite.Height;
			_bounds = new AxisAlignedBoundingBox(X, Y, Width, Height);
			Closed = true;
			_key = key;
			SetTranslation();
			XChanged += () => _bounds.X = X;
			YChanged += () => _bounds.Y = Y;
		}

		public void Open(KeyItem key)
		{
			if (_key.Equals(key))
			{
				Closed = false;
				key.Use();
			}
		}

		public override void Render()
		{
			GL.PushMatrix();
			Translate();
			if (Closed)
			{
				_closedSprite.Render();
			}
			else
			{
				_openedSprite.Render();
			}
			GL.PopMatrix();
		}

		public override AxisAlignedBoundingBox GetAabb()
		{
			int halfW = _bounds.Width / 2;
			int halfH = _bounds.Height / 2;
			return new AxisAlignedBoundingBox(_bounds.X + halfW / 2, _bounds.Y + halfH / 2, halfW, halfH);
		}
	}
}
