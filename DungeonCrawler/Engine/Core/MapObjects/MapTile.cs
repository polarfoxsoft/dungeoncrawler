﻿using OpenTK;
using OpenTK.Graphics;
using Engine.Graphics;

namespace Engine.Core.MapObjects
{
	public abstract class MapTile : GameObject
	{
		private Vector3 _translation;
		protected CoordinateManager _manager;
		private int _lastVersion;

		public int Width { get; protected set; }
		public int Height { get; protected set; }

		protected void SetTranslation()
		{
			_lastVersion = _manager.Version;
			_translation = _manager.GetTranslation(X, Y);
		}

		protected void Translate()
		{
			if (_manager.Version != _lastVersion)
			{
				SetTranslation();
			}
			GL.Translate(_translation);
		}
	}
}
