﻿using System;
using OpenTK.Graphics;
using Engine.Graphics;
using Engine.Core.Collisions;

namespace Engine.Core.MapObjects
{
	public class Wall : MapTile
	{
		private Sprite _sprite;
		private Sprite _sideSprite;
		private AxisAlignedBoundingBox _aabb;

		public Wall(Sprite sprite, int x, int y, CoordinateManager manager)
		{
			if (sprite == null)
			{
				throw new ArgumentNullException("sprite");
			}
			if (manager == null)
			{
				throw new ArgumentNullException("manager");
			}

			_sprite = sprite;
			_manager = manager;
			X = x;
			Y = y;
			Width = _sprite.Width;
			Height = _sprite.Height;
			_aabb = new AxisAlignedBoundingBox(X, Y, Width, Height);
			SetTranslation();
			//_sideSprite = new Sprite(Texture.FromColor(System.Drawing.Color.FromArgb(255, 16, 32, 0)), Height * 2 / 3, Width, _manager);
		}

		public override void Render()
		{
			GL.PushMatrix();
			Translate();
			//_sideSprite.Render();
			//_manager.Translate(0, _sideSprite.Height);
			//GL.Translate(0, 0, -0.1);
			_sprite.Render();
			GL.PopMatrix();
		}

		public override AxisAlignedBoundingBox GetAabb()
		{
			return _aabb;
		}
	}
}
