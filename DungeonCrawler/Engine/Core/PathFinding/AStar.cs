﻿using System;
using System.Collections.Generic;
using Engine.Core.Collections;

namespace Engine.Core.PathFinding
{
	public class AStar
	{
		private NodeNetwork _nodes;
		private IHeuristic _heuristic;

		public AStar(NodeNetwork nodes, IHeuristic heuristic)
		{
			if (nodes == null)
			{
				throw new ArgumentNullException("nodes");
			}
			if (heuristic == null)
			{
				throw new ArgumentNullException("heuristic");
			}

			_nodes = nodes;
			_heuristic = heuristic;
		}

		public List<Node> FindPath(Node sourse, Node destination)
		{
			if (sourse == null) throw new ArgumentNullException("sourse");
			if (destination == null) throw new ArgumentNullException("destination");

			var exploredNodes = new HashSet<Node>();
			var distanceFromSource = new Dictionary<Node, int>();
			var heuristicScore = new Dictionary<Node, int>();
			var parents = new Dictionary<Node, Node>();
			var heap = new Heap<int, Node>(null);

			foreach (Node node in _nodes.ValidNodes)
			{
				heuristicScore.Add(node, int.MaxValue);
				distanceFromSource.Add(node, int.MaxValue);
			}

			heuristicScore[sourse] = _heuristic.CalcScore(sourse, destination);
			distanceFromSource[sourse] = 0;

			heap.Enqueue(heuristicScore[sourse], sourse);

			while (heap.Count > 0)
			{
				Node current = heap.Dequeue().Value;
				
				if (current.Equals(destination))
				{
					return RetracePath(parents, destination);
				}
				
				exploredNodes.Add(current);

				IList<Node> neighbors = _nodes.GetNeighbours(current);

				foreach (Node node in neighbors)
				{
					if (exploredNodes.Contains(node))
					{
						continue;
					}

					int currentScore = distanceFromSource[current] + _nodes.GetDistance(current, node);

					if (currentScore >= distanceFromSource[node])
					{
						continue;
					}

					parents[node] = current;
					distanceFromSource[node] = currentScore;
					heuristicScore[node] = currentScore + _heuristic.CalcScore(node, destination);
					heap.Enqueue(heuristicScore[node], node);

					var cursor = heap.FindValue(node);
					if (cursor == null)
					{
						
					}

					//if (heuristicScore[node] == int.MaxValue)
					//{
					//	heuristicScore[node] = _heuristic.CalcScore(node, destination);
					//}

					//if (currentScore < distanceFromSource[node])
					//{
					//	distanceFromSource[node] = currentScore;
					//	parents[node] = current;

					//	heap.Enqueue(currentScore + heuristicScore[node], node);

					//	//var cursor = heap.FindValue(node);

					//	//if (cursor == null)
					//	//{
					//	//	heuristicScore[node] = _heuristic.CalcScore(node, destination);
					//	//	heap.Enqueue(distanceFromSource[node] + heuristicScore[node], node);
					//	//}
					//	//else
					//	//{
					//	//	heap.UpdateKey(cursor, distanceFromSource[node] + heuristicScore[node]);
					//	//}
					//}
				}
			}

			return new List<Node>();
		}

		private List<Node> RetracePath(Dictionary<Node, Node> parents, Node endNode)
		{
			var path = new List<Node>();
			var current = endNode;

			while (current != null)
			{
				path.Add(current);
				parents.TryGetValue(current, out current);
			}
			path.Reverse();
			return path;
		}
	}
}
