﻿using System;

namespace Engine.Core.PathFinding
{
	public class Node : IEquatable<Node>
	{
		public int X { get; private set; }
		public int Y { get; private set; }
		public bool Walkable { get; private set; }
		public int Weight { get; private set; }

		public Node(Node other)
			: this(other.X, other.Y, other.Walkable, other.Weight)
		{
		}

		public Node(int x, int y, bool walkable, int weight)
		{
			X = x;
			Y = y;
			Walkable = walkable;
			Weight = weight;
		}

		public override bool Equals(object obj)
		{
			if (obj == null) return false;
			if (ReferenceEquals(this, obj)) return true;
			return Equals(obj as Node);
		}

		public override int GetHashCode()
		{
			return X.GetHashCode() + 19 * Y.GetHashCode();
		}

		public bool Equals(Node other)
		{
			if (other == null) return false;
			if (ReferenceEquals(this, other)) return true;
			return X == other.X && Y == other.Y;
		}
	}
}
