﻿using System;
using System.Collections.Generic;
using System.Linq;
using Engine.StoredMap;

namespace Engine.Core.PathFinding
{
	public class NodeNetwork
	{
		private HashSet<Node> _validNodes;

		public IEnumerable<Node> ValidNodes
		{
			get
			{
				return _validNodes;
			}
		}

		public NodeNetwork(IDsMap map)
		{
			_validNodes = new HashSet<Node>();

			for (int i = 0; i < map.RowsCount; i++)
			{
				var k = map.RowsCount - i - 1;
				for (int j = 0; j < map.ColumnsCount; j++)
				{
					switch (map.Cell(i, j))
					{
						case 0:
						case 2:
						case 3:
						case 7:
						case 8:
						case 9:
							_validNodes.Add(new Node(j, k, true, 1));
							break;
					}
				}
			}
		}

		public List<Node> GetNeighbours(Node node)
		{
			var allNeighbours = new Node[]
			{
				new Node(node.X - 1, node.Y + 1, true, 1),
				new Node(node.X, node.Y + 1, true, 1),
				new Node(node.X + 1, node.Y + 1, true, 1),
				new Node(node.X + 1, node.Y, true, 1),
				new Node(node.X + 1, node.Y - 1, true, 1),
				new Node(node.X, node.Y - 1, true, 1),
				new Node(node.X - 1, node.Y - 1, true, 1),
				new Node(node.X - 1, node.Y, true, 1)
			};

			int count = allNeighbours.Count();
			var isValid = new bool[count];

			for (int i = 0; i < allNeighbours.Count(); i++)
			{
				isValid[i] = _validNodes.Contains(allNeighbours[i]);
			}

			for (int i = 1; i < count; i += 2)
			{
				if (!isValid[i])
				{
					isValid[i - 1] = false;
					int j = i + 1;
					j = j == count ? 0 : j;
					isValid[j] = false;
				}
			}

			var neighbours = new List<Node>();

			for (int i = 0; i < count; i++)
			{
				if (isValid[i])
				{
					neighbours.Add(allNeighbours[i]);
				}
			}

			return neighbours;
		}

		public int GetDistance(Node source, Node destination)
		{
			var x = Math.Abs(destination.X - source.X);
			var y = Math.Abs(destination.Y - source.Y);
			switch (x + y)
			{
				case 0:
					return 0;
				case 1:
					return 10;
				case 2:
					return 14;
				default:
					return int.MaxValue;
			}
		}
	}
}
