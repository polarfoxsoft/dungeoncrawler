﻿using System;

namespace Engine.Core.PathFinding
{
	public class EuclideanHeuristic : IHeuristic
	{
		private static EuclideanHeuristic _instance;

		static EuclideanHeuristic()
		{
			_instance = new EuclideanHeuristic();
		}

		private EuclideanHeuristic()
		{

		}

		public static EuclideanHeuristic GetInstance()
		{
			return _instance;
		}

		public int CalcScore(Node sourse, Node destination)
		{
			var x = sourse.X - destination.X;
			var y = sourse.Y - destination.Y;
			return (int)Math.Sqrt(x * x + y * y);
		}
	}
}
