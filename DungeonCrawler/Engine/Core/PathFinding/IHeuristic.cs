﻿
namespace Engine.Core.PathFinding
{
	public interface IHeuristic
	{
		int CalcScore(Node sourse, Node destination);
	}
}
