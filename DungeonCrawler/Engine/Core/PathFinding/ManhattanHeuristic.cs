﻿using System;

namespace Engine.Core.PathFinding
{
	public class ManhattanHeuristic : IHeuristic
	{
		private static ManhattanHeuristic _instance;

		static ManhattanHeuristic()
		{
			_instance = new ManhattanHeuristic();
		}

		private ManhattanHeuristic()
		{

		}

		public static ManhattanHeuristic GetInstance()
		{
			return _instance;
		}

		public int CalcScore(Node sourse, Node destination)
		{
			return Math.Abs(sourse.X - destination.X) + Math.Abs(sourse.Y - destination.Y);
		}
	}
}
