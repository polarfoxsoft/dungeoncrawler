﻿using System;
using System.Collections.Generic;
using System.Drawing;
using OpenTK.Graphics;
using Engine.Graphics;

namespace Engine.Text
{
	public class TextLine : IRenderable
	{
		#region Fields

		private static readonly Color DefaultColor = Color.White;

		private List<Sprite> _line;
		private CoordinateManager _coordManager;
		private Font _font;
		private float _size;

		#endregion // Fields

		#region Properties

		public int Width { get; private set; }
		public int Height { get; private set; }
		public Color Color { get; set; }

		#endregion // Properties

		#region Constructors

		public TextLine(string line, Font font, CoordinateManager manager)
			: this(line, font, manager, DefaultColor)
		{
		}

		public TextLine(string line, Font font, CoordinateManager manager, float size)
			: this(line, font, manager, DefaultColor, size)
		{
		}

		public TextLine(string line, TextStyle style, CoordinateManager manager)
			: this(line, style.Font, manager, style.Color, style.Size)
		{
		}

		public TextLine(string line, Font font, CoordinateManager manager, Color color)
			: this(line, font, manager, color, 1)
		{
		}

		public TextLine(string line, Font font, CoordinateManager manager, Color color, float size)
		{
			if (manager == null)
			{
				throw new ArgumentNullException("manager");
			}
			if (font == null)
			{
				throw new ArgumentNullException("font");
			}

			_coordManager = manager;
			_font = font;
			_line = Convert(line, _font);
			_size = size > 0 ? size : 1;
			Width = GetWidth(_line, _size);
			Height = GetHeight(_line, _size);
			Color = color;
		}

		public TextLine(TextLine other)
			: this(other, other != null ? other.Color : DefaultColor)
		{
		}

		public TextLine(TextLine other, Color color)
		{
			if (other == null)
			{
				throw new ArgumentNullException("other");
			}

			_size = other._size;
			_coordManager = other._coordManager;
			_font = other._font;
			_line = new List<Sprite>(other._line);
			Width = other.Width;
			Height = other.Height;
			Color = color;
		}

		#endregion // Constructors

		#region Methods

		public void Render()
		{
			Render(Color);
		}

		public void Render(Color color)
		{
			GL.PushMatrix();
			GL.Scale(_size, _size, 1);
			int dx = 0;
			foreach (var symbol in _line)
			{
				_coordManager.Translate(dx, 0);
				symbol.Render(color);
				dx = symbol.Width;
			}
			GL.PopMatrix();
		}

		public void Append(TextLine other)
		{
			if (other != null)
			{
				Append(other._line);
			}
		}

		public void Append(string line)
		{
			Append(Convert(line, _font));
		}

		public void Append(char symbol)
		{
			var sprite = _font.Convert(symbol);
			_line.Add(sprite);
			Width += sprite.Width;
			if (sprite.Height > Height)
			{
				Height = sprite.Height;
			}
		}

		private void Append(List<Sprite> line)
		{
			if (line != null)
			{
				foreach (var symbol in line)
				{
					_line.Add(symbol);
				}
				Width += GetWidth(line, _size);
				var otherHeight = GetHeight(line, _size);
				if (otherHeight > Height)
				{
					Height = otherHeight;
				}
			}
		}

		private static List<Sprite> Convert(string line, Font font)
		{
			var result = new List<Sprite>();
			if (!string.IsNullOrEmpty(line))
			{
				foreach (var symbol in line)
				{
					result.Add(font.Convert(symbol));
				}
			}
			return result;
		}

		private static int GetWidth(List<Sprite> line, float size)
		{
			int width = 0;
			foreach (var symbol in line)
			{
				width += symbol.Width;
			}
			return (int)(size * width);
		}

		private static int GetHeight(List<Sprite> line, float size)
		{
			int height = 0;
			foreach (var symbol in line)
			{
				if (symbol.Height > height)
				{
					height = symbol.Height;
				}
			}
			return (int)(size * height);
		}

		#endregion // Methods
	}
}
