﻿using System;
using System.Drawing;

namespace Engine.Text
{
	public class TextStyle
	{
		public static readonly Color DefaultColor = Color.White;

		private Font _font;

		public float Size { get; set; }
		public Color Color { get; set; }

		public Font Font
		{
			get
			{
				return _font;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException();
				}
				_font = value;
			}
		}

		public TextStyle(Font font)
			: this(font, DefaultColor)
		{
		}

		public TextStyle(Font font, Color color)
			: this(font, color, 1)
		{
		}

		public TextStyle(Font font, Color color, float size)
		{
			Font = font;
			Color = color;
			Size = size;
		}
	}
}
