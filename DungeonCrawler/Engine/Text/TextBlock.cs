﻿using System;
using System.Collections.Generic;
using OpenTK.Graphics;
using Engine.Graphics;
using System.Drawing;

namespace Engine.Text
{
	public class TextBlock : IRenderable
	{
		#region Fields

		private static readonly Color DefaultColor = Color.White;

		private List<TextLine> _lines;
		private CoordinateManager _coordManager;
		private int _widthLimit;
		private int _gap;
		private int _height;
		private Color _color;
		private float _size;

		#endregion // Fields

		#region Properties

		public int Width { get; private set; }

		public int Height
		{
			get
			{
				return _height + LineGap * (_lines.Count - 1);
			}
		}

		public int LineGap
		{
			get
			{
				return _gap;
			}
			set
			{
				_gap = value > 0 ? value : 0;
			}
		}

		public Color Color
		{
			get
			{
				return _color;
			}
			set
			{
				if (_color != value)
				{
					_color = value;
					foreach (var line in _lines)
					{
						line.Color = value;
					}
				}
			}
		}

		#endregion // Properties

		#region Constructors

		public TextBlock(string text, int widthLimit, Font font, CoordinateManager manager)
			: this(text, widthLimit, font, manager, DefaultColor)
		{
		}

		public TextBlock(string text, int widthLimit, Font font, CoordinateManager manager, float size)
			: this(text, widthLimit, font, manager, DefaultColor, size)
		{
		}

		public TextBlock(string text, int widthLimit, Font font, CoordinateManager manager, Color color)
			: this(text, widthLimit, font, manager, color, 1)
		{
		}

		public TextBlock(string text, int widthLimit, Font font, CoordinateManager manager, Color color, float size)
		{
			if (font == null)
			{
				throw new ArgumentNullException("font");
			}
			if (text == null)
			{
				throw new ArgumentNullException("text");
			}
			if (manager == null)
			{
				throw new ArgumentNullException("manager");
			}

			_coordManager = manager;
			_lines = new List<TextLine>();
			Color = color;
			_size = size > 0 ? size : 0;

			var words = text.Split(' ');
			var line = new TextLine(string.Empty, font, manager, color, _size);
			var space = new TextLine(" ", font, manager, color, _size);
			foreach (var word in words)
			{
				var temp = new TextLine(word, font, manager, color, _size);
				if (line.Width + space.Width + temp.Width < widthLimit)
				{
					if (line.Width != 0)
					{
						line.Append(space);
					}
					line.Append(temp);
				}
				else
				{
					_lines.Add(line);
					line = temp;
				}
			}
			if (line.Width != 0)
			{
				_lines.Add(line);
			}
			_height = GetHeight();
			Width = GetWidth();
			_widthLimit = widthLimit;
		}

		#endregion // Constructors

		#region Methods

		public void Render()
		{
			Render(Color);
		}

		public void Render(Color color)
		{
			GL.PushMatrix();
			int dy = 0;
			for (int i = _lines.Count - 1; i >= 0; i--)
			{
				_coordManager.Translate(0, dy);
				_lines[i].Render(color);
				dy = _lines[i].Height + LineGap;
			}
			GL.PopMatrix();
		}

		private int GetWidth()
		{
			int width = 0;
			foreach (var line in _lines)
			{
				if (line.Width > width)
				{
					width = line.Width;
				}
			}
			return width;
		}

		private int GetHeight()
		{
			int height = 0;
			foreach (var line in _lines)
			{
				height += line.Height;
			}
			return height;
		}

		#endregion // Methods
	}
}
