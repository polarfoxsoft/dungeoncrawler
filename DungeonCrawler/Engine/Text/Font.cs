﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using Engine.Graphics;
using Engine.Xml;

namespace Engine.Text
{
	public class Font
	{
		private Dictionary<char, Sprite> _symbols;
		private Sprite _default;

		public string Name { get; private set; }

		public Font(string xmlFileName, CoordinateManager manager)
		{
			_symbols = new Dictionary<char, Sprite>();

			var doc = new XmlDocument();
			doc.Load(FileStorage.StorageManager.Instance.GetFile(xmlFileName));
			Name = doc.DocumentElement.GetAttribute("name");
			var textureName = doc.DocumentElement.GetAttribute("texture");
			var texture = Texture.FromImage(textureName);
			var defaultElement = doc.DocumentElement.GetElementsByTagName("default").Cast<XmlElement>().First().GetElementsByTagName(XmlLoader.RectangleTagName).Cast<XmlElement>().First();
			_default = new Sprite(texture, XmlLoader.LoadRectangle(defaultElement), manager);

			foreach (var item in doc.DocumentElement.GetElementsByTagName("symbol").Cast<XmlElement>())
			{
				char ch;
				char.TryParse(item.GetAttribute("name"), out ch);
				var sprite = new Sprite(texture, XmlLoader.LoadRectangle(item.GetElementsByTagName(XmlLoader.RectangleTagName).Cast<XmlElement>().First()), manager);
				_symbols.Add(ch, sprite);
			}
		}

		public Sprite Convert(char symbol)
		{
			Sprite result;
			if (!_symbols.TryGetValue(symbol, out result))
			{
				result = _default;
			}
			return result;
		}
	}
}
