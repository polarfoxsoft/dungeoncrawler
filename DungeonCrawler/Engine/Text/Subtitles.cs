﻿using Engine.FileStorage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace Engine.Text
{
	public class Subtitles
	{
		private class Line : IComparable<Line>
		{
			public static readonly string XmlTag = "line";
			public static readonly string XmlTimeAttrName = "time";
			public static readonly string XmlTextAttrName = "text";

			public string Text { get; set; }
			public TimeSpan Time { get; set; }

			public Line()
			{
			}

			public Line(string text, TimeSpan time)
			{
				Text = text;
				Time = time;
			}

			public void AddToXml(XmlDocument xml, XmlElement parent)
			{
				var element = xml.CreateElement(XmlTag);
				parent.AppendChild(element);
				var attribute = xml.CreateAttribute(XmlTimeAttrName);
				attribute.Value = Time.ToString();
				element.Attributes.Append(attribute);
				attribute = xml.CreateAttribute(XmlTextAttrName);
				attribute.Value = Text;
				element.Attributes.Append(attribute);
			}

			public int CompareTo(Line other)
			{
				return Time.CompareTo(other.Time);
			}
		}

		private List<Line> _lines;

		public Subtitles(string fileName)
		{
			_lines = new List<Line>();
			var xml = new XmlDocument();
			xml.Load(StorageManager.Instance.GetFile(fileName));
			foreach (var obj in xml.GetElementsByTagName(Line.XmlTag))
			{
				var element = (XmlElement)(obj);
				TimeSpan time;
				if (TimeSpan.TryParse(element.GetAttribute(Line.XmlTimeAttrName), out time))
				{
					_lines.Add(new Line(element.GetAttribute(Line.XmlTextAttrName), time));
				}
			}
			_lines.Sort();
		}

		public string GetLine(TimeSpan time)
		{
			return _lines.Last(l => l.Time.CompareTo(time) <= 0).Text;
		}

		public static void CreateDraft(string textFile, string outputFile)
		{
			var ms = new MemoryStream();
			using (var writer = new XmlTextWriter(outputFile, null))
			{
				writer.WriteStartDocument();
				writer.WriteStartElement("subtitles");
				writer.WriteEndElement();
			}
			var xml = new XmlDocument();
			xml.Load(outputFile);
			using (var reader = new StreamReader(textFile))
			{
				while (!reader.EndOfStream)
				{
					var text = reader.ReadLine();
					var line = new Line(text, new TimeSpan());
					line.AddToXml(xml, xml.DocumentElement);
				}
			}
			xml.Save(outputFile);
		}
	}
}
