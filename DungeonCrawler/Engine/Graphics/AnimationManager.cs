﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Engine.Graphics
{
	public class AnimationManager
	{
		#region Fields

		private Dictionary<string, Animation> _animations;

		#endregion // Fields

		#region Properties

		public Animation CurrentAnimation { get; private set; }

		public IEnumerable<string> Names
		{
			get
			{
				return _animations.Keys;
			}
		}

		#endregion // Properties

		#region Constructors

		public AnimationManager()
		{
			_animations = new Dictionary<string, Animation>();
		}

		public AnimationManager(AnimationManager other)
		{
			if (other == null)
			{
				throw new ArgumentNullException("other");
			}
			_animations = new Dictionary<string, Animation>();
			foreach (var name in other._animations.Keys)
			{
				Animation temp;
				other._animations.TryGetValue(name, out temp);
				Add(name, new Animation(temp));
			}
		}

		#endregion // Constructors

		#region Methods

		public bool Add(string name, Animation animation)
		{
			if (!_animations.ContainsKey(name))
			{
				_animations.Add(name, animation);
				CurrentAnimation = animation;
				return true;
			}
			return false;
		}

		public bool SetAnimation(string name)
		{
			Animation animation;
			if (_animations.TryGetValue(name, out animation))
			{
				CurrentAnimation = animation;
				return true;
			}
			return false;
		}

		#endregion // Methods
	}
}
