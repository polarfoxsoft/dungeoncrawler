﻿using System;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Engine.Graphics
{
	public class GridLines
	{
		private CoordinateManager _manager;
		private int _rowsCount;
		private int _columnsCount;

		#region Properties

		public Size CellSize { get; set; }

		public int RowsCount
		{
			get
			{
				return _rowsCount;
			}
			set
			{
				_rowsCount = value > 0 ? value : 0;
			}
		}

		public int ColumnsCount
		{
			get
			{
				return _columnsCount;
			}
			set
			{
				_columnsCount = value > 0 ? value : 0;
			}
		}

		#endregion // Properties

		public GridLines(Size cellSize, int rowsCount, int columnCount, CoordinateManager manager)
		{
			if (manager == null)
			{
				throw new ArgumentNullException("manager");
			}
			_manager = manager;
			CellSize = cellSize;
			RowsCount = rowsCount;
			ColumnsCount = columnCount;
		}

		public void Render()
		{
			Render(Color.White);
		}

		public void Render(Color color)
		{
			var translation = _manager.GetTranslation(CellSize.Width, CellSize.Height);
			var vertical = new Vector2(0, translation.Y * RowsCount);
			var horizontal = new Vector2(translation.X * ColumnsCount, 0);

			GL.PushMatrix();

			GL.Begin(PrimitiveType.Lines);

			GL.Color3(color);

			for (int i = 0; i <= RowsCount; i++)
			{
				var dy = translation.Y * i;
				GL.Vertex2(0, dy);
				GL.Vertex2(horizontal.X, horizontal.Y + dy);
			}

			for (int i = 0; i <= ColumnsCount; i++)
			{
				var dx = translation.X * i;
				GL.Vertex2(dx, 0);
				GL.Vertex2(vertical.X + dx, vertical.Y);
			}

			GL.End();

			GL.PopMatrix();
		}
	}
}
