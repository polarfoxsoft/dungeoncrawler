﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using OpenTK.Graphics;

namespace Engine.Graphics
{
	public class Texture
	{
		private static readonly Dictionary<string, Texture> TexturesFromFiles = new Dictionary<string, Texture>();
		private int _id;
		
		public int Width { get; protected set; }
		
		public int Height { get; protected set; }

		private Texture(string fileName)
			: this(new Bitmap(fileName))
		{
		}
		
		public Texture(Bitmap bitmap)
		{
			if (bitmap == null)
			{
				throw new ArgumentNullException("bitmap");
			}

			_id = GL.GenTexture();
			Bind();

			Width = bitmap.Width;
			Height = bitmap.Height;

			var bitmapData = bitmap.LockBits(
				new Rectangle(0, 0, Width, Height),
				ImageLockMode.ReadOnly,
				System.Drawing.Imaging.PixelFormat.Format32bppArgb);

			GL.TexImage2D(
				TextureTarget.Texture2D,
				0,
				PixelInternalFormat.Rgba,
				bitmapData.Width, bitmapData.Height,
				0,
				OpenTK.Graphics.PixelFormat.Bgra,
				PixelType.UnsignedByte,
				bitmapData.Scan0);

			bitmap.UnlockBits(bitmapData);

			GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);
			//GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.LinearMipmapLinear);
			//GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
		}

		public void SetFilters(TextureMinFilter min, TextureMagFilter mag)
		{
			Bind();
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)min);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)mag);
			BindDefault();
		}

		public void Bind()
		{
			GL.BindTexture(TextureTarget.Texture2D, _id);
		}

		public static void BindDefault()
		{
			GL.BindTexture(TextureTarget.Texture2D, 0);
		}

		public static Texture FromColor(Color color)
		{
			return FromColor(color, 1, 1);
		}

		public static Texture FromColor(Color color, int width, int height)
		{
			var bitmap = new Bitmap(width, height);
			for (int i = 0; i < width; i++)
			{
				for (int j = 0; j < height; j++)
				{
					bitmap.SetPixel(i, j, color);
				}
			}
			return new Texture(bitmap);
		}

		public static Texture FromImage(string fileName)
		{
			Texture result;
			if (!TexturesFromFiles.TryGetValue(fileName, out result))
			{
				//result = new Texture(fileName);
				result = new Texture(new Bitmap(FileStorage.StorageManager.Instance.GetFile(fileName)));
				TexturesFromFiles.Add(fileName, result);
			}
			return result;
		}

		public static List<Texture> ListFromGif(string fileName)
		{
			var file = FileStorage.StorageManager.Instance.GetFile(fileName);
			var bitmap = new Bitmap(file);
			var frames = new List<Texture>();
			if (bitmap.FrameDimensionsList.Length > 0)
			{
				var dimension = new FrameDimension(bitmap.FrameDimensionsList[0]);
				var frameCount = bitmap.GetFrameCount(dimension);
				for (int i = 0; i < frameCount; i++)
				{
					bitmap.SelectActiveFrame(dimension, i);
					frames.Add(new Texture(bitmap));
				}
			}
			else
			{
				frames.Add(new Texture(bitmap));
			}
			return frames;
		}
	}
}
