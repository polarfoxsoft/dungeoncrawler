﻿using System;
using System.Diagnostics;
using System.Collections.Generic;

namespace Engine.Graphics
{
	public class Animation : IRenderable
	{
		#region Fields

		private static readonly int DefaultDelay = 100;

		private List<Sprite> _frames;
		private int _currentFrame;
		private int _delay;
		private Stopwatch _stopwatch;
		private bool _cyclesMode;
		private int _cyclesDuration;
		private bool _paused;

		#endregion // Fields

		#region Properties
		
		public bool IsPlaying
		{
			get
			{
				return _stopwatch.IsRunning;
			}
		}

		public int Width
		{
			get
			{
				return _frames[_currentFrame].Width;
			}
		}

		public int Height
		{
			get
			{
				return _frames[_currentFrame].Height;
			}
		}

		public int CurrentFrameIndex
		{
			get { return _currentFrame; }
		}

		public int FramesCount
		{
			get { return _frames.Count; }
		}

		#endregion // Properties

		#region Constructors

		public Animation(List<Sprite> frames, int delay)
		{
			if (frames == null)
			{
				throw new ArgumentNullException("frames");
			}
			if (frames.Count == 0)
			{
				throw new ArgumentException("At least 1 frame in list required", "frames");
			}

			_delay = delay > 0 ? delay : DefaultDelay;
			_frames = frames;
			_stopwatch = new Stopwatch();
		}

		public Animation(Animation other)
		{
			if (other == null)
			{
				throw new ArgumentNullException("other");
			}

			_delay = other._delay;
			_frames = new List<Sprite>(other._frames);
			_stopwatch = new Stopwatch();
		}

		#endregion // Constructors

		#region Methods

		public static Animation FromGif(string fileName, int delay, CoordinateManager manager)
		{
			var textures = Texture.ListFromGif(fileName);
			var frames = new List<Sprite>(textures.Count);
			foreach (var tex in textures)
			{
				frames.Add(new Sprite(tex, manager));
			}
			return new Animation(frames, delay);
		}

		public void Render()
		{
			GetCurrentFrame().Render();
		}

		public Sprite GetCurrentFrame()
		{
			if (_stopwatch.IsRunning)
			{
				var step = _stopwatch.ElapsedMilliseconds / _delay;
				_currentFrame = (int)(step % _frames.Count);
				if (_cyclesMode && _stopwatch.ElapsedMilliseconds >= _cyclesDuration)
				{
					_stopwatch.Reset();
				}
			}
			return _frames[_currentFrame];
		}

		public void Play()
		{
			_paused = false;
			_cyclesMode = false;
			_stopwatch.Start();
		}

		public void PlayCycles(int count)
		{
			_paused = false;
			_currentFrame = 0;
			_cyclesMode = true;
			_cyclesDuration = count * _frames.Count * _delay;
			_stopwatch.Start();
		}

		public void PlayOnce()
		{
			_paused = false;
			_currentFrame = 0;
			_cyclesMode = true;
			_cyclesDuration = (_frames.Count - 1) * _delay;
			_stopwatch.Start();
		}

		public void Pause()
		{
			if (_stopwatch.IsRunning)
			{
				_paused = true;
			}
			_stopwatch.Stop();
		}

		public void Unpause()
		{
			if (_paused)
			{
				_paused = false;
				_stopwatch.Start();
			}
		}

		public void Stop()
		{
			_currentFrame = 0;
			_stopwatch.Reset();
		}

		#endregion // Methods
	}
}
