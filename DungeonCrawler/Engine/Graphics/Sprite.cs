﻿using System;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics;

namespace Engine.Graphics
{
	public class Sprite : IRenderable
	{
		#region Fields

		private Texture _texture;
		private int _height;
		private int _width;
		private float _x;
		private float _y;
		private RectangleF _textureFrame;
		private int _lastVersion;
		private CoordinateManager _manager;

		#endregion //Fields

		#region Properties

		/// <summary>
		/// Получает высоту объекта Engine.Graphics.Sprite в пикселях.
		/// </summary>
		public int Height
		{
			get
			{
				return _height;
			}
			private set
			{
				if (value < 0)
				{
					throw new ArgumentOutOfRangeException();
				}
				_height = value;
			}
		}

		/// <summary>
		/// Получает ширину объекта Engine.Graphics.Sprite в пикселях.
		/// </summary>
		public int Width
		{
			get
			{
				return _width;
			}
			private set
			{
				if (value < 0)
				{
					throw new ArgumentOutOfRangeException();
				}
				_width = value;
			}
		}

		#endregion //Properties

		#region Constructors
		
		public Sprite(Texture texture, CoordinateManager manager)
			: this(texture, texture != null ? texture.Height : 0, texture != null ? texture.Width : 0, manager)
		{
		}
		
		public Sprite(int height, int width, CoordinateManager manager)
			: this(null, height, width, manager)
		{
		}
		
		public Sprite(Texture texture, int height, int width, CoordinateManager manager)
			: this(texture, texture != null ? new Rectangle(0, 0, texture.Width, texture.Height) : new Rectangle(), height, width, manager)
		{
		}
		
		public Sprite(Texture texture, Rectangle frame, CoordinateManager manager)
			: this(texture, frame, frame != null ? frame.Height : 0, frame != null ? frame.Width : 0, manager)
		{
		}
		
		public Sprite(Texture texture, Rectangle frame, int height, int width, CoordinateManager manager)
		{
			Init(texture, frame, height, width, manager);
		}

		#endregion //Constructors

		#region Methods
		
		public void Render()
		{
			Render(false, false);
		}

		public void Render(Color color)
		{
			Render(false, false, color);
		}
		
		public void Render(bool horizontalFlip, bool verticalFlip)
		{
			Render(horizontalFlip, verticalFlip, Color.White);
		}

		public void Render(bool horizontalFlip, bool verticalFlip, Color color)
		{
			GL.PushMatrix();

			if (_texture != null)
			{
				_texture.Bind();
				GL.Enable(EnableCap.Texture2D);
			}
			GL.Color3(color);

			if (_manager.Version != _lastVersion)
			{
				CalculateSize(_manager);
				_lastVersion = _manager.Version;
			}

			if (horizontalFlip)
			{
				GL.Scale(-1, 1, 0);
				GL.Translate(-_x, 0, 0);
			}
			if (verticalFlip)
			{
				GL.Scale(1, -1, 0);
				GL.Translate(0, -_y, 0);
			}

			var x = _textureFrame.X;
			var y = _textureFrame.Y;
			GL.Begin(BeginMode.Quads);
			GL.TexCoord2(x, y + _textureFrame.Height);
			GL.Vertex2(0, 0);
			GL.TexCoord2(x + _textureFrame.Width, y + _textureFrame.Height);
			GL.Vertex2(_x, 0);
			GL.TexCoord2(x + _textureFrame.Width, y);
			GL.Vertex2(_x, _y);
			GL.TexCoord2(x, y);
			GL.Vertex2(0, _y);
			GL.End();

			GL.Disable(EnableCap.Texture2D);

			GL.PopMatrix();
		}
		
		private void CalculateSize(CoordinateManager manager)
		{
			var temp = manager.GetTranslation(Width, Height);
			_x = temp.X;
			_y = temp.Y;
		}

		private void Init(Texture texture, Rectangle frame, int height, int width, CoordinateManager manager)
		{
			_texture = texture;
			Height = height;
			Width = width;
			CalculateSize(manager);
			_textureFrame = CalcTextureFrame(texture, frame);
			_manager = manager;
			_lastVersion = manager.Version;
		}

		private static RectangleF CalcTextureFrame(Texture texture, Rectangle frame)
		{
			if (texture == null || frame == null)
			{
				return new RectangleF();
			}
			float width = texture.Width;
			float height = texture.Height;
			return new RectangleF(frame.X / width, frame.Y / height, frame.Width / width, frame.Height / height);
		}

		#endregion //Methods
	}
}
