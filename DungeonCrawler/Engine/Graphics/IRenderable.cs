﻿namespace Engine.Graphics
{
	public interface IRenderable
	{
		int Width { get; }
		int Height { get; }
		void Render();
	}
}
