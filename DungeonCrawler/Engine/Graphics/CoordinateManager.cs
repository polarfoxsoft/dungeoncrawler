﻿using System;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics;

namespace Engine.Graphics
{
	public class CoordinateManager
	{
		private GameWindow _screen;

		public int Width
		{
			get { return _screen.Width; }
		}

		public int Height
		{
			get { return _screen.Height; }
		}

		public int Version { get; private set; }

		public CoordinateManager(GameWindow targetScreen)
		{
			if (targetScreen == null)
			{
				throw new ArgumentNullException("targetScreen");
			}
			if (targetScreen.Width <= 0 || targetScreen.Height <= 0)
			{
				throw new ArgumentOutOfRangeException("targetScreen", targetScreen, "Width and height must be greater than 0");
			}
			_screen = targetScreen;
		}

		public Vector3 GetTranslation(int x, int y)
		{
			return new Vector3((float)(2 * x) / _screen.Width, (float)(2 * y) / _screen.Height, 0);
		}

		public void Translate(int x, int y)
		{
			GL.Translate(GetTranslation(x, y));
		}

		public void Update()
		{
			Version++;
		}
	}
}
