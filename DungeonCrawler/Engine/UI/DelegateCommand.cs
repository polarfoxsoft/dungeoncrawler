﻿using System;

namespace Engine.UI.Controls
{
	public class DelegateCommand : ICommand
	{
		private Action _action;

		public DelegateCommand(Action action)
		{
			if (action == null)
			{
				throw new ArgumentNullException("action");
			}
			_action = action;
		}

		public void Execute()
		{
			_action?.Invoke();
		}
	}
}
