﻿using System;
using System.Drawing;
using OpenTK.Input;
using OpenTK.Graphics.OpenGL;
using Engine.Core.Collisions;
using Engine.UI.Controls;
using Engine.Graphics;

namespace Engine.UI.Windows
{
	public abstract class Window : IControl
	{
		private AxisAlignedBoundingBox _bounds;
		private Sprite _background;
		protected Texture _backgroundTexture;
		protected CoordinateManager _manager;

		#region Properties

		public bool Closed { get; private set; }

		public AxisAlignedBoundingBox Bounds
		{
			get
			{
				return _bounds;
			}
		}

		public int Height
		{
			get
			{
				return _bounds.Height;
			}
		}

		public int Width
		{
			get
			{
				return _bounds.Width;
			}
		}

		public int X
		{
			get
			{
				return _bounds.X;
			}

			set
			{
				var temp = _bounds.X;
				_bounds.X = value;
				OnXChanged(temp);
			}
		}

		public int Y
		{
			get
			{
				return _bounds.Y;
			}

			set
			{
				var temp = _bounds.Y;
				_bounds.Y = value;
				OnYChanged(temp);
			}
		}

		#endregion // Properties

		#region Methods

		public void Open()
		{
			Closed = false;
			OnOpened();
		}

		public void Close()
		{
			Closed = true;
			OnClosed();
		}

		public void SetBounds(AxisAlignedBoundingBox bounds)
		{
			if (bounds == null)
			{
				throw new ArgumentNullException("bounds");
			}
			SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
		}

		public void SetBounds(Rectangle bounds)
		{
			if (bounds == null)
			{
				throw new ArgumentNullException("bounds");
			}
			SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
		}

		public void SetBounds(int x, int y, int width, int height)
		{
			var oldX = _bounds == null ? 0 : _bounds.X;
			var oldY = _bounds == null ? 0 : _bounds.Y;
			var rectangle = new Rectangle(x, y, width, height);
			_bounds = new AxisAlignedBoundingBox(rectangle);
			_background = new Sprite(_backgroundTexture, rectangle, _manager);
			OnXChanged(oldX);
			OnYChanged(oldY);
		}

		protected void RenderBackground()
		{
			GL.PushMatrix();
			GL.LoadIdentity();
			GL.Translate(-1, 1, 0);
			_manager.Translate(X, -Y - Height);
			_background.Render();
			GL.PopMatrix();
		}

		#endregion Methods

		#region Overridable methods

		public virtual void OnClick(object sender, MouseButtonEventArgs e)
		{
		}

		public virtual void OnHover(object sender, MouseMoveEventArgs e)
		{
		}

		public virtual void Render()
		{
			RenderBackground();
		}

		protected virtual void OnOpened()
		{
		}

		protected virtual void OnClosed()
		{
		}

		protected virtual void OnXChanged(int oldX)
		{
		}

		protected virtual void OnYChanged(int oldY)
		{
		}

		#endregion // Overridable methods
	}
}
