﻿using System;
using System.Drawing;
using OpenTK.Input;
using OpenTK.Graphics.OpenGL;
using Engine.Core.Collisions;
using Engine.Graphics;
using Engine.Text;
using Engine.UI;
using Engine.UI.Controls;

namespace Engine.UI.Windows
{
	public class MessageBox : Window
	{
		private TextBlock _message;
		private Button _ok;
		private Point _margin;

		#region Properties

		#endregion // Properties

		public MessageBox(CoordinateManager manager, TextBlock message, IRenderable buttonView, IRenderable buttonSelectedView, Texture background)
		{
			if (manager == null)
			{
				throw new ArgumentNullException("manager");
			}

			_manager = manager;
			_backgroundTexture = background;
			_margin = new Point(20, 20);
			_message = message;
			var width = 2 * _margin.X + Math.Max(_message.Width, Math.Max(buttonView.Width, buttonSelectedView.Width));
			var height = 3 * _margin.Y + _message.Height + Math.Max(buttonView.Height, buttonSelectedView.Height);
			_ok = new Button(new DelegateCommand(() => Close()), (width - buttonView.Width) / 2, height - _margin.Y - buttonView.Height, buttonView, buttonSelectedView, _manager);
			SetBounds(0, 0, width, height);
		}

		public override void Render()
		{
			base.Render();
			GL.PushMatrix();
			GL.LoadIdentity();
			GL.Translate(-1, 1, 0);
			_manager.Translate(X + _margin.X, -Y - _margin.Y - _message.Height);
			_message.Render();
			GL.PopMatrix();
			_ok.Render();
		}

		public override void OnHover(object sender, MouseMoveEventArgs e)
		{
			if (!Closed)
			{
				_ok.OnHover(sender, e);
			}
		}

		public override void OnClick(object sender, MouseButtonEventArgs e)
		{
			if (!Closed)
			{
				_ok.OnClick(sender, e);
			}
		}

		protected override void OnXChanged(int oldX)
		{
			_ok.X = _ok.X + X - oldX;
		}

		protected override void OnYChanged(int oldY)
		{
			_ok.Y = _ok.Y + Y - oldY;
		}
	}
}
