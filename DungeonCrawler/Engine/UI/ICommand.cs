﻿namespace Engine.UI
{
	public interface ICommand
	{
		void Execute();
	}
}
