﻿using Engine.Core.Collisions;
using OpenTK.Input;
using Engine.Graphics;

namespace Engine.UI.Controls
{
	public class CheckBox : IControl
	{
		#region Fields
		
		private Button _checked;
		private Button _unchecked;
		private AxisAlignedBoundingBox _bounds;

		#endregion // Fields

		#region Properties

		public bool Checked { get; set; }
		
		public int X
		{
			get
			{
				return _bounds.X;
			}
			set
			{
				_bounds.X = value;
			}
		}

		public int Y
		{
			get
			{
				return _bounds.Y;
			}
			set
			{
				_bounds.Y = value;
			}
		}

		public int Width
		{
			get
			{
				return _bounds.Width;
			}
		}

		public int Height
		{
			get
			{
				return _bounds.Height;
			}
		}

		public AxisAlignedBoundingBox Bounds
		{
			get
			{
				return _bounds;
			}
		}

		#endregion // Properties

		public CheckBox(IRenderable uncheckedView, IRenderable uncheckedHoverView, IRenderable checkedView, IRenderable checkedHoverView, CoordinateManager manager)
			: this(uncheckedView, uncheckedHoverView, checkedView, checkedHoverView, manager, 0, 0)
		{
		}

		public CheckBox(IRenderable uncheckedView, IRenderable uncheckedHoverView, IRenderable checkedView, IRenderable checkedHoverView, CoordinateManager manager, int x, int y)
			: this(uncheckedView, uncheckedHoverView, checkedView, checkedHoverView, manager, new AxisAlignedBoundingBox(x, y, uncheckedView.Width, uncheckedView.Height))
		{
		}

		public CheckBox(IRenderable uncheckedView, IRenderable uncheckedHoverView, IRenderable checkedView, IRenderable checkedHoverView, CoordinateManager manager, AxisAlignedBoundingBox bounds)
		{
			var changeState = new DelegateCommand(() => Checked = !Checked);
			_unchecked = new Button(changeState, bounds, uncheckedView, uncheckedHoverView, manager);
			_checked = new Button(changeState, bounds, checkedView, checkedHoverView, manager);
			_bounds = bounds;
		}

		public void OnClick(object sender, MouseButtonEventArgs e)
		{
			if (Checked)
			{
				_checked.OnClick(sender, e);
			}
			else
			{
				_unchecked.OnClick(sender, e);
			}
		}

		public void OnHover(object sender, MouseMoveEventArgs e)
		{
			if (Checked)
			{
				_checked.OnHover(sender, e);
			}
			else
			{
				_unchecked.OnHover(sender, e);
			}
		}

		public void Render()
		{
			if (Checked)
			{
				_checked.Render();
			}
			else
			{
				_unchecked.Render();
			}
		}
	}
}
