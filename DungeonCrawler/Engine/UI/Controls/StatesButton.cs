﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Engine.Core.Collisions;
using Engine.Graphics;
using OpenTK.Input;

namespace Engine.UI.Controls
{
	public class StatesButton<T> : IControl
	{
		private AxisAlignedBoundingBox _bounds;
		private List<Button> _buttons;
		private Dictionary<T, Button> _backLinks;
		private Button _button;
		private T _state;

		#region Properties

		public T State
		{
			get
			{
				return _state;
			}
			set
			{
				if (_backLinks.ContainsKey(value))
				{
					_state = value;
					_backLinks.TryGetValue(_state, out _button);
				}
			}
		}

		public AxisAlignedBoundingBox Bounds
		{
			get
			{
				return _bounds;
			}
		}

		public int Height
		{
			get
			{
				return _bounds.Height;
			}
		}

		public int Width
		{
			get
			{
				return _bounds.Width;
			}
		}

		public int X
		{
			get
			{
				return _bounds.X;
			}

			set
			{
				if (_bounds.X != value)
				{
					_bounds.X = value;
					foreach (var button in _buttons)
					{
						button.X = value;
					}
				}
			}
		}

		public int Y
		{
			get
			{
				return _bounds.Y;
			}

			set
			{
				if (_bounds.Y != value)
				{
					_bounds.Y = value;
					foreach (var button in _buttons)
					{
						button.Y = value;
					}
				}
			}
		}

		#endregion // Properties

		#region Constructors

		public StatesButton(List<IRenderable> basicViews, List<IRenderable> selectedViews, List<T> states, CoordinateManager manager)
			: this(0, 0, basicViews, selectedViews, states, manager)
		{
		}

		public StatesButton(int x, int y, List<IRenderable> basicViews, List<IRenderable> selectedViews, List<T> states, CoordinateManager manager)
			: this(new AxisAlignedBoundingBox(x, y, basicViews[0].Width, basicViews[0].Height), basicViews, selectedViews, states, manager)
		{
		}

		public StatesButton(AxisAlignedBoundingBox bounds, List<IRenderable> basicViews, List<IRenderable> selectedViews, List<T> states, CoordinateManager manager)
		{
			if (bounds == null)
			{
				throw new ArgumentNullException("bounds");
			}
			if (basicViews == null)
			{
				throw new ArgumentNullException("basicViews");
			}
			if (selectedViews == null)
			{
				throw new ArgumentNullException("selectedViews");
			}
			if (states == null)
			{
				throw new ArgumentNullException("states");
			}
			if (manager == null)
			{
				throw new ArgumentNullException("manager");
			}

			var count = Math.Min(basicViews.Count, Math.Min(selectedViews.Count, states.Count));
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("Some of three lists are empty");
			}

			_bounds = bounds;
			_buttons = new List<Button>();
			_backLinks = new Dictionary<T, Button>();
			for (int i = 0; i < count; i++)
			{
				var nextIndex = (i + 1) % count;
				var temp = states[nextIndex];
				var button = new Button(new DelegateCommand(() => State = temp), _bounds, basicViews[i], selectedViews[i], manager);
				_backLinks.Add(states[i], button);
				_buttons.Add(button);
			}
			State = states[0];
		}

		#endregion // Constructors

		public void OnClick(object sender, MouseButtonEventArgs e)
		{
			_button.OnClick(sender, e);
		}

		public void OnHover(object sender, MouseMoveEventArgs e)
		{
			_button.OnHover(sender, e);
		}

		public void Render()
		{
			_button.Render();
		}
	}
}
