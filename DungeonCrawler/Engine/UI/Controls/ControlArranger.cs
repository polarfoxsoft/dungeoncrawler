﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Engine.UI.Controls
{
	public static class ControlArranger
	{
		public enum Alignment
		{
			Left,
			Right,
			Middle
		}

		public static List<IControl> ArrangeVertical(List<IControl> controls, int x, int y, Alignment type, int gap)
		{
			var width = controls.Max(c => c.Width);
			var yPos = y;
			Action<IControl> resetY = c => { c.Y = yPos; yPos = yPos + c.Height + gap; };
			Action<IControl> resetX;
			switch (type)
			{
				case Alignment.Left:
					resetX = c => c.X = x;
					break;
				case Alignment.Right:
					resetX = c => c.X = x + width - c.Width;
					break;
				case Alignment.Middle:
					resetX = c => c.X = x + (width - c.Width) / 2;
					break;
				default:
					resetX = c => c.X = x;
					break;
			}

			foreach (var control in controls)
			{
				resetX(control);
				resetY(control);
			}
			return controls;
		}
	}
}
