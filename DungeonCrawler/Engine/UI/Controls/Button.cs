﻿using System;
using OpenTK.Graphics;
using OpenTK.Input;
using Engine.Graphics;
using Engine.Core.Collisions;

namespace Engine.UI.Controls
{
	public class Button : IControl
	{
		#region Fields

		private ICommand _command;
		private IRenderable _view;
		private IRenderable _basicView;
		private IRenderable _selectedView;
		private AxisAlignedBoundingBox _bounds;
		private CoordinateManager _manager;

		#endregion // Fields

		#region Properties

		public int X
		{
			get
			{
				return _bounds.X;
			}
			set
			{
				_bounds.X = value;
			}
		}

		public int Y
		{
			get
			{
				return _bounds.Y;
			}
			set
			{
				_bounds.Y = value;
			}
		}

		public int Width
		{
			get
			{
				return _bounds.Width;
			}
		}

		public int Height
		{
			get
			{
				return _bounds.Height;
			}
		}

		public AxisAlignedBoundingBox Bounds
		{
			get
			{
				return _bounds;
			}
		}

		#endregion // Properties

		#region Constructors

		public Button(ICommand command, IRenderable basicView, IRenderable selectedView, CoordinateManager manager)
			: this(command, 0, 0, basicView, selectedView, manager)
		{
		}

		public Button(ICommand command, int x, int y, IRenderable basicView, IRenderable selectedView, CoordinateManager manager)
			: this(command, new AxisAlignedBoundingBox(x, y, basicView.Width, basicView.Height), basicView, selectedView, manager)
		{
		}

		public Button(ICommand command, AxisAlignedBoundingBox bounds, IRenderable basicView, IRenderable selectedView, CoordinateManager manager)
		{
			if (command == null)
			{
				throw new ArgumentNullException("command");
			}
			if (bounds == null)
			{
				throw new ArgumentNullException("bounds");
			}
			if (basicView == null)
			{
				throw new ArgumentNullException("basicView");
			}
			if (selectedView == null)
			{
				throw new ArgumentNullException("selectedView");
			}
			if (manager == null)
			{
				throw new ArgumentNullException("manager");
			}
			_command = command;
			_bounds = bounds;
			_basicView = basicView;
			_selectedView = selectedView;
			_view = basicView;
			_manager = manager;
		}

		#endregion // Constructors

		#region Methods

		public void Render()
		{
			GL.PushMatrix();
			GL.LoadIdentity();
			GL.Translate(-1, 1, 0);
			_manager.Translate(_bounds.X, -_bounds.Y - _view.Height);
			_view.Render();
			GL.PopMatrix();
		}

		public void OnClick(object sender, MouseButtonEventArgs e)
		{
			if (MouseOnElement(e))
			{
				_command.Execute();
			}
		}

		public void OnHover(object sender, MouseMoveEventArgs e)
		{
			if (MouseOnElement(e))
			{
				_view = _selectedView;
			}
			else
			{
				_view = _basicView;
			}
		}

		protected bool MouseOnElement(MouseEventArgs e)
		{
			return _bounds.CheckCollision(e.X, e.Y).Detected;
		}

		#endregion // Methods
	}
}
