﻿using System;
using System.Collections.Generic;
using System.Linq;
using Engine.Core.Collisions;
using OpenTK.Input;

namespace Engine.UI.Controls
{
	public class ControlContainer : IControl
	{
		protected List<IControl> _controls;
		private AxisAlignedBoundingBox _bounds;

		#region Properties

		public bool Active { get; set; }

		public virtual int Width
		{
			get
			{
				return _bounds.Width;
			}
		}

		public virtual int Height
		{
			get
			{
				return _bounds.Height;
			}
		}

		public virtual int X
		{
			get
			{
				return _bounds.X;
			}
			set
			{
				if (X != value)
				{
					var diff = value - _bounds.X;
					_bounds.X = value;
					foreach (var control in _controls)
					{
						control.X += diff;
					}
				}
			}
		}

		public virtual int Y
		{
			get
			{
				return _bounds.Y;
			}
			set
			{
				if (_bounds.Y != value)
				{
					var diff = value - _bounds.Y;
					_bounds.Y = value;
					foreach (var control in _controls)
					{
						control.Y += diff;
					}
				}
			}
		}

		public virtual AxisAlignedBoundingBox Bounds
		{
			get
			{
				return _bounds;
			}
		}

		#endregion //Properties

		#region Constructors

		public ControlContainer()
			: this(null)
		{
		}

		public ControlContainer(List<IControl> controls)
		{
			_controls = controls == null ? new List<IControl>() : controls;
			UpdateBounds();
			Active = false;
		}

		#endregion // Constructors

		#region Methods

		public virtual void Add(IControl control)
		{
			if (control == null)
			{
				throw new ArgumentNullException("control");
			}
			_controls.Add(control);
			_bounds = AxisAlignedBoundingBox.GetBounds(_bounds, control.Bounds);
		}

		public void UpdateBounds()
		{
			if (_controls.Count > 0)
			{
				_bounds = AxisAlignedBoundingBox.GetBounds(_controls.Select(c => c.Bounds));
			}
		}

		public virtual void Render()
		{
			foreach (var control in _controls)
			{
				control.Render();
			}
		}

		public virtual void OnClick(object sender, MouseButtonEventArgs e)
		{
			if (Active)
			{
				foreach (var control in _controls)
				{
					control.OnClick(sender, e);
				}
			}
		}

		public virtual void OnHover(object sender, MouseMoveEventArgs e)
		{
			if (Active)
			{
				foreach (var control in _controls)
				{
					control.OnHover(sender, e);
				}
			}
		}

		#endregion // Methods
	}
}
