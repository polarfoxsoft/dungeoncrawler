﻿using OpenTK.Input;
using Engine.Graphics;
using Engine.Core.Collisions;

namespace Engine.UI.Controls
{
	public interface IControl : IRenderable
	{
		int X { get; set; }
		int Y { get; set; }
		AxisAlignedBoundingBox Bounds { get; }
		void OnClick(object sender, MouseButtonEventArgs e);
		void OnHover(object sender, MouseMoveEventArgs e);
	}
}
