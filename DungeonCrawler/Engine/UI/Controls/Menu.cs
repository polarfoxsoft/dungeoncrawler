﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK.Input;

namespace Engine.UI.Controls
{
	public class Menu : ControlContainer
	{
		private int _x;
		private int _y;

		public override int X
		{
			get
			{
				return _x;
			}
			set
			{
				if (_x != value)
				{
					var diff = value - _x;
					_x = value;
					foreach (var control in _controls)
					{
						control.X += diff;
					}
				}
			}
		}

		public override int Y
		{
			get
			{
				return _y;
			}
			set
			{
				if (_y != value)
				{
					var diff = value - _y;
					_y = value;
					foreach (var control in _controls)
					{
						control.Y += diff;
					}
				}
			}
		}

		public override int Width
		{
			get
			{
				return _width;
			}
		}

		public override int Height
		{
			get
			{
				return _height;
			}
		}

		public Menu(List<Button> buttons) 
			: base()
		{
			if (buttons != null)
			{
				foreach (var button in buttons)
				{
					Add(button);
				}
			}
		}
	}
}
