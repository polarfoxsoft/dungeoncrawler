﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Engine.FileStorage
{
	public class StorageManager : IStorage
	{
		private static readonly FileSystemStorage FSStorage;

		public static readonly StorageManager Instance;

		private List<IStorage> _storages;
		private Dictionary<string, IStorage> _fileIndex;

		static StorageManager()
		{
			FSStorage = FileSystemStorage.GetInstance();
			Instance = new StorageManager();
		}

		public StorageManager()
		{
			_storages = new List<IStorage>();
			_fileIndex = new Dictionary<string, IStorage>();
		}

		public bool Contains(string filename)
		{
			return GetStorageContaining(filename).Contains(filename);
		}

		public Stream GetFile(string filename)
		{
			return GetStorageContaining(filename).GetFile(filename);
		}

		public void AddStorage(IStorage storage)
		{
			if (storage != null)
			{
				_storages.Add(storage);
			}
		}

		private IStorage GetStorageContaining(string filename)
		{
			IStorage storage;
			if (!_fileIndex.TryGetValue(filename, out storage))
			{
				foreach (var s in _storages)
				{
					if (s.Contains(filename))
					{
						storage = s;
						break;
					}
				}
				if (storage != null)
				{
					_fileIndex.Add(filename, storage);
				}
				else
				{
					storage = FSStorage;
				}
			}
			return storage;
		}
	}
}
