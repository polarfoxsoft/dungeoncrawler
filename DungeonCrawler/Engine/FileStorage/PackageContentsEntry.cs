﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Engine.FileStorage
{
	public class PackageContentsEntry
	{
		public static readonly int ConstPartSize;

		private string _filename;
		private int _size;

		public long Position { get; set; }
		public long Size { get; set; }

		public string Filename
		{
			get
			{
				return _filename;
			}
			set
			{
				_filename = value;
				_size = 0;
			}
		}

		public int EntrySize
		{
			get
			{
				if (_size == 0)
				{
					_size = ConstPartSize + Package.Encoding.GetByteCount(_filename);
				}
				return _size;
			}
		}

		static PackageContentsEntry()
		{
			ConstPartSize = sizeof(int) + 2 * sizeof(long);
		}

		public static PackageContentsEntry Read(BinaryReader reader)
		{
			var entry = new PackageContentsEntry();
			var size = reader.ReadInt32();
			entry.Position = reader.ReadInt64();
			entry.Size = reader.ReadInt64();
			var bytes = reader.ReadBytes(size - ConstPartSize);
			entry.Filename = Package.Encoding.GetString(bytes);
			return entry;
		}

		public void Write(BinaryWriter writer)
		{
			writer.Write(EntrySize);
			writer.Write(Position);
			writer.Write(Size);
			writer.Write(Package.Encoding.GetBytes(Filename));
		}
	}
}
