﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Engine.FileStorage
{
	public interface IStorage
	{
		bool Contains(string filename);
		Stream GetFile(string filename);
	}
}
