﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Engine.FileStorage
{
	public class FileSystemStorage : IStorage
	{
		private static readonly FileSystemStorage Instance;

		static FileSystemStorage()
		{
			Instance = new FileSystemStorage();
		}

		private FileSystemStorage()
		{

		}

		public static FileSystemStorage GetInstance()
		{
			return Instance;
		}

		public bool Contains(string filename)
		{
			return File.Exists(filename);
		}

		public Stream GetFile(string filename)
		{
			return File.OpenRead(filename);
		}
	}
}
