﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Engine.FileStorage
{
	public class Package : IStorage
	{
		public static readonly Encoding Encoding = Encoding.UTF8;
		public static readonly int CurrentVersion = 1;

		private string _packagePath;
		private List<PackageContentsEntry> _contents;
		private Dictionary<string, PackageContentsEntry> _index;

		public int FileCount
		{
			get
			{
				return _contents.Count;
			}
		}

		public ICollection<string> Files
		{
			get
			{
				return _index.Keys;
			}
		}

		public Package(string path)
		{
			_packagePath = path;
			_contents = new List<PackageContentsEntry>();
			_index = new Dictionary<string, PackageContentsEntry>();
			using (var file = new FileStream(_packagePath, FileMode.Open))
			using (var reader = new BinaryReader(file))
			{
				var version = reader.ReadInt32();
				var count = version;
				if (version == 1)
				{
					var contentsPosition = reader.ReadInt64();
					count = reader.ReadInt32();
					file.Seek(contentsPosition, SeekOrigin.Begin);
				}
				for (int i = 0; i < count; i++)
				{
					var entry = PackageContentsEntry.Read(reader);
					entry.Filename = FixFileName(entry.Filename);
					_contents.Add(entry);
					_index.Add(entry.Filename, entry);
				}
			}
		}

		public void Unpack(string path)
		{
			foreach (var entry in _contents)
			{
				var name = entry.Filename;
				var p = Path.GetDirectoryName(name);
				if (!string.IsNullOrWhiteSpace(p))
				{
					var dir = string.Concat(path, "\\", p);
					if (!Directory.Exists(dir))
					{
						Directory.CreateDirectory(dir);
					}
				}
				var filename = string.Concat(path, "\\", name);
				using (var file = File.Create(filename))
				{
					GetFile(name).CopyTo(file);
				}
			}
		}

		public Stream GetFile(string name)
		{
			var stream = new MemoryStream();
			name = FixFileName(name);
			var entry = GetEntry(name);
			using (var file = new FileStream(_packagePath, FileMode.Open))
			using (var reader = new BinaryReader(file))
			{
				file.Seek(entry.Position, SeekOrigin.Begin);
				var bytes = reader.ReadBytes((int)entry.Size);
				stream = new MemoryStream(bytes);
			}
			return stream;
		}

		public bool Contains(string filename)
		{
			filename = FixFileName(filename);
			return _index.ContainsKey(filename);
		}

		private PackageContentsEntry GetEntry(string filename)
		{
			PackageContentsEntry entry;
			_index.TryGetValue(filename, out entry);
			return entry;
		}

		public static void Pack(string outputFilename, string filesDirectory)
		{
			Pack(outputFilename, GetFileNames(filesDirectory));
		}

		public static void Pack(string outputFilename, string filesDirectory, Func<string, bool> filter)
		{
			var files = GetFileNames(filesDirectory);
			if (filter != null)
			{
				Pack(outputFilename, files.Where(filter));
			}
			else
			{
				Pack(outputFilename, files);
			}
		}

		public static void Pack(string outputFilename, IEnumerable<string> fileNames)
		{
			var log = new StringBuilder();
			var added = new HashSet<string>();
			using (var file = new FileStream(outputFilename, FileMode.Create))
			using (var writer = new BinaryWriter(file))
			{
				writer.Write(CurrentVersion);
				writer.Write((long)0);
				writer.Write((int)0);

				var contents = new List<PackageContentsEntry>(fileNames.Count());
				foreach (var fn in fileNames)
				{
					var name = FixFileName(fn);
					if (!added.Contains(name))
					{
						if (File.Exists(name))
						{
							var entry = new PackageContentsEntry();
							entry.Filename = name;
							entry.Position = file.Position;
							entry.Size = new FileInfo(name).Length;
							writer.Write(File.ReadAllBytes(name));
							contents.Add(entry);
							added.Add(name);
						}
						else
						{
							log.AppendLine(string.Format("Error:\tFile not found:\t", name));
						}
					}
				}
				var contentsPosition = file.Position;
				foreach (var entry in contents)
				{
					entry.Write(writer);
				}
				file.Seek(0, SeekOrigin.Begin);
				writer.Write(CurrentVersion);
				writer.Write(contentsPosition);
				writer.Write(contents.Count);
			}
		}

		private static List<string> GetFileNames(string dirName)
		{
			var files = new List<string>(Directory.EnumerateFiles(dirName));
			foreach (var dir in Directory.GetDirectories(dirName))
			{
				files.AddRange(GetFileNames(dir));
			}
			return files;
		}

		private static string FixFileName(string fileName)
		{
			return fileName.Replace('/', '\\');
		}
	}
}
