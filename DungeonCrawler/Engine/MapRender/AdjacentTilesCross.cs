﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Engine.Graphics;
using Engine.StoredMap;

namespace Engine.MapRender
{
	public class AdjacentTilesCross
	{
		private static readonly string SameType = "1";
		private static readonly string OtherType = "0";

		private readonly Dictionary<string, SpriteSelector> _variants;

		public AdjacentTilesCross(Dictionary<string, SpriteSelector> variants)
		{
			if (variants == null)
			{
				throw new ArgumentNullException("variants");
			}
			_variants = variants;
		}

		public Sprite GetSprite(IDsMap map, int rowIndex, int columnIndex)
		{
			if (rowIndex < 0 || columnIndex < 0 || rowIndex >= map.RowsCount || columnIndex >= map.ColumnsCount)
			{
				throw new ArgumentOutOfRangeException();
			}

			var id = map.Cell(rowIndex, columnIndex);
			var prevRow = rowIndex - 1;
			var nextRow = rowIndex + 1;
			var prevColumn = columnIndex - 1;
			var nextColumn = columnIndex + 1;
			var key = new StringBuilder();

			if (prevRow < 0 || map.Cell(prevRow, columnIndex) != id)
			{
				key.Append(OtherType);
			}
			else
			{
				key.Append(SameType);
			}

			if (prevColumn < 0 || map.Cell(rowIndex, prevColumn) != id)
			{
				key.Append(OtherType);
			}
			else
			{
				key.Append(SameType);
			}

			if (nextColumn >= map.ColumnsCount || map.Cell(rowIndex, nextColumn) != id)
			{
				key.Append(OtherType);
			}
			else
			{
				key.Append(SameType);
			}

			if (nextRow >= map.RowsCount || map.Cell(nextRow, columnIndex) != id)
			{
				key.Append(OtherType);
			}
			else
			{
				key.Append(SameType);
			}

			SpriteSelector selector;
			_variants.TryGetValue(key.ToString(), out selector);
			return selector.GetNext();
		}
	}
}
