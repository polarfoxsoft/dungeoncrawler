﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Engine.StoredMap;
using Engine.Graphics;
using Engine.Core;
using Engine.Core.MapObjects;
using Engine.Core.Collisions;
using Engine.Core.Items;

namespace Engine.MapRender
{
	public class Map : IRenderable
	{
		#region Variables

		private readonly CoordinateManager _manager;
		private readonly IDsMap _map;
		private readonly MapTileFactory _factory;
		private MapTile[][] _matrix;
		private int _tileSize;
		private TileSet _tileset;
		private GridLines _grid;
		private Point _enterance;
		private ExitTile _exit;
		private List<Chest> _chests;
		private KeyItem _exitKey;

		#endregion

		#region Properties

		public bool GridVisible { get; set; }
		public Color GridColor { get; set; }

		public Point Enterance
		{
			get
			{
				return _enterance;
			}
		}

		public ExitTile Exit
		{
			get
			{
				return _exit;
			}
		}

		public KeyItem ExitKey
		{
			get
			{
				return _exitKey;
			}
		}

		public int TileSize
		{
			get
			{
				return _tileSize;
			}
			private set
			{
				if (value <= 0)
				{
					throw new ArgumentOutOfRangeException();
				}
				_tileSize = value;
			}
		}

		public int ColumnsCount
		{
			get { return _map.ColumnsCount; }
		}

		public int RowsCount
		{
			get { return _map.RowsCount; }
		}

		public int Width
		{
			get { return ColumnsCount * TileSize; }
		}

		public int Height
		{
			get { return RowsCount * TileSize; }
		}

		#endregion

		#region Methods

		public Map(CoordinateManager manager, IDsMap map, TileSet tileset)
		{
			_manager = manager;
			_tileset = tileset;
			TileSize = tileset.TileSize;
			_map = map;
			_exitKey = new KeyItem(manager);
			_chests = new List<Chest>();
			_factory = new MapTileFactory(manager, tileset);
			_matrix = Convert();

			if (_exit == null)
			{
				//ChangeCell(Enterance.X, Enterance.Y, 3);
				//_exit = new ExitTile(_tileset.ClosedExit, _tileset.OpenedExit, Enterance.X, Enterance.Y, _manager, _exitKey);
				ChangeCell(_tileSize + 1, _tileSize + 1, 3);
				//_exit = new ExitTile(_tileset.ClosedExit, _tileset.OpenedExit, _tileSize, _tileSize, _manager, _exitKey);
			}

			_grid = new GridLines(new Size(_tileSize, _tileSize), RowsCount, ColumnsCount, manager);
			GridVisible = false;
			GridColor = Color.White;
		}

		private MapTile[][] Convert()
		{
			MapTile[][] result = new MapTile[RowsCount][];
			int x;
			int y = 0;
			for (int i = 0; i < RowsCount; i++)
			{
				var k = RowsCount - i - 1;
				result[i] = new MapTile[ColumnsCount];
				x = 0;
				for (int j = 0; j < ColumnsCount; j++)
				{
					result[i][j] = _factory.Create(_map, k, j, x, y);
					switch (_map.Cell(k, j))
					{
						case 2:
							_enterance = new Point(x, y);
							break;
						case 3:
							_exit = new ExitTile(_tileset.ClosedExit, _tileset.OpenedExit, x, y, _manager, _exitKey);
							break;
						case 4:
							_chests.Add(new Chest(_tileset.ClosedChest, _tileset.OpenedChest, x, y, _manager, 0));
							break;
					}
					x += TileSize;
				}
				y += TileSize;
			}

			return result;
		}

		public List<Point> GetEnemies()
		{
			var enemies = new List<Point>();
						int x;
			int y = 0;
			for (int i = 0; i < RowsCount; i++)
			{
				var k = RowsCount - i - 1;
				x = 0;
				for (int j = 0; j < ColumnsCount; j++)
				{

					switch (_map.Cell(k, j))
					{
						case 8:
							enemies.Add(new Point(x, y));
							break;
					}
					x += TileSize;
				}
				y += TileSize;
			}
			return enemies;
		}

		public List<Point> GetBoss()
		{
			var enemies = new List<Point>();
			int x;
			int y = 0;
			for (int i = 0; i < RowsCount; i++)
			{
				var k = RowsCount - i - 1;
				x = 0;
				for (int j = 0; j < ColumnsCount; j++)
				{

					switch (_map.Cell(k, j))
					{
						case 9:
							enemies.Add(new Point(x, y));
							break;
					}
					x += TileSize;
				}
				y += TileSize;
			}
			return enemies;
		}

		public void Render()
		{
			for (int i = 0; i < _map.RowsCount; i++)
			{
				for (int j = 0; j < _map.ColumnsCount; j++)
				{
					_matrix[i][j].Render();
				}
			}
			foreach (var chest in _chests)
			{
				chest.Render();
			}
			_exit.Render();

			if (GridVisible)
			{
				_grid.Render(GridColor);
			}
		}

		public void ChangeCell(int x, int y, int id)
		{
			var ri = GetRowIndex(y);
			var ci = GetColumnIndex(x);
			var rri = _map.RowsCount - 1 - ri;
			if (id != _map.Cell(rri, ci))
			{
				_map.ChangeCell(rri, ci, id);
				_matrix[ri][ci] = _factory.Create(_map, rri, ci, _matrix[ri][ci].X, _matrix[ri][ci].Y);
				UpdateWallsAround(ri, ci);

				switch (id)
				{
					case 2:
						_enterance = new Point(ci * _tileSize, ri * _tileSize);
						break;
					case 3:
						_exit = new ExitTile(_tileset.ClosedExit, _tileset.OpenedExit, ci * _tileSize, ri * _tileSize, _manager, _exitKey);
						break;
					case 4:
						_chests.Add(new Chest(_tileset.ClosedChest, _tileset.OpenedChest, ci * _tileSize, ri * _tileSize, _manager, 0));
						break;
				}
			}
		}

		public int GetCellId(int x, int y)
		{
			var ri = GetRowIndex(y);
			var ci = GetColumnIndex(x);
			var rri = _map.RowsCount - 1 - ri;
			return _map.Cell(rri, ci);
		}

		private void UpdateWallsAround(int rowIndex, int columnIndex)
		{
			for (int i = Math.Max(rowIndex - 1, 0); i <= Math.Min(rowIndex + 1, RowsCount - 1); i++)
			{
				var k = RowsCount - 1 - i;
				for (int j = Math.Max(columnIndex - 1, 0); j <= Math.Min(columnIndex + 1, ColumnsCount - 1); j++)
				{
					if (_map.Cell(k, j) == 1)
					{
						_matrix[i][j] = _factory.Create(_map, k, j, _matrix[i][j].X, _matrix[i][j].Y);
					}
				}
			}
		}

		public MapTile GetTile(int x, int y)
		{
			return _matrix[GetRowIndex(y)][GetColumnIndex(x)];
		}

		public void Export(string filename)
		{
			MapWriter.Write(_map, filename);
		}

		public Collision CheckCollision(GameObject entity)
		{
			var aabb = entity?.GetAabb();
			if (aabb == null)
			{
				return new Collision();
			}

			int maxDx = Math.Max(aabb.Width, TileSize);
			int maxDy = Math.Max(aabb.Height, TileSize);
			int minDx = maxDx + 2;
			int minDy = maxDy + 2;

			for (int i = GetRowIndex(aabb.Y); i <= GetRowIndex(aabb.Y + aabb.Height); i++)
			{
				for (int j = GetColumnIndex(aabb.X); j <= GetColumnIndex(aabb.X + aabb.Width); j++)
				{
					var collision = GameObject.CheckAabbCollision(_matrix[i][j], entity);
					if (collision.X != 0 && collision.X * collision.X < minDx * minDx)
					{
						minDx = collision.X;
					}
					if (collision.Y != 0 && collision.Y * collision.Y < minDy * minDy)
					{
						minDy = collision.Y;
					}
				}
			}
			foreach (var chest in _chests)
			{
				var collision = GameObject.CheckAabbCollision(chest, entity);
				if (collision.X != 0 && collision.X * collision.X < minDx * minDx)
				{
					minDx = collision.X;
				}
				if (collision.Y != 0 && collision.Y * collision.Y < minDy * minDy)
				{
					minDy = collision.Y;
				}
			}

			return new Collision(minDx < maxDx ? minDx : 0, minDy < maxDx ? minDy : 0);
		}

		public void LootChests(Player player)
		{
			foreach (var chest in _chests)
			{
				if (GameObject.CheckAabbCollision(player, chest).Detected)
				{
					chest.Open(player);
				}
			}
		}

		public int GetColumnIndex(int x)
		{
			var index = x / TileSize;
			index = index >= ColumnsCount ? ColumnsCount - 1 : index;
			return index > 0 ? index : 0;
		}

		public int GetRowIndex(int y)
		{
			var index = y / TileSize;
			index = index >= RowsCount ? RowsCount - 1 : index;
			return index > 0 ? index : 0;
		}

		#endregion
	}
}
