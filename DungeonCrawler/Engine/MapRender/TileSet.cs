﻿using System;
using System.Linq;
using System.Xml;
using Engine.Graphics;
using Engine.Xml;

namespace Engine.MapRender
{
	public class TileSet
	{
		public SpriteSelector Surface { get; private set; }
		public AdjacentTilesCross Wall { get; private set; }
		public Sprite ClosedChest { get; private set; }
		public Sprite OpenedChest { get; private set; }
		public Sprite ClosedExit { get; private set; }
		public Sprite OpenedExit { get; private set; }
		public Sprite Enterance { get; private set; }
		public int TileSize { get; private set; }

		public TileSet(int tileSize, SpriteSelector surface, AdjacentTilesCross wall, Sprite closedExit, Sprite openedExit, Sprite closedChest, Sprite openedChest, Sprite enterance)
		{
			if (surface == null)
			{
				throw new ArgumentNullException("surface");
			}
			if (wall == null)
			{
				throw new ArgumentNullException("wall");
			}
			if (closedExit == null)
			{
				throw new ArgumentNullException("closedExit");
			}
			if (openedExit == null)
			{
				throw new ArgumentNullException("openedExit");
			}
			if (closedChest == null)
			{
				throw new ArgumentNullException("closedChest");
			}
			if (openedChest == null)
			{
				throw new ArgumentNullException("openedChest");
			}

			TileSize = tileSize;
			Surface = surface;
			ClosedExit = closedExit;
			OpenedExit = openedExit;
			ClosedChest = closedChest;
			OpenedChest = openedChest;
			Enterance = enterance;
			Wall = wall;
		}

		public static TileSet FromXml(string path, CoordinateManager manager)
		{
			var doc = new XmlDocument();
			doc.Load(FileStorage.StorageManager.Instance.GetFile(path));

			var texture = Texture.FromImage(doc.DocumentElement.GetAttribute("texture"));
			var tileSize = int.Parse(doc.DocumentElement.GetAttribute("tilesize"));

			var tiles = doc.DocumentElement.GetElementsByTagName("tile").Cast<XmlElement>();
			var surfaceSelector = XmlLoader.LoadSpriteSelector(tiles.First(e => e.GetAttribute("name") == "surface"), texture, manager);
			var wallSelector = XmlLoader.LoadAdjacentTilesCross(tiles.First(e => e.GetAttribute("name") == "wall"), texture, manager);
			var closedExit = new Sprite(texture, XmlLoader.LoadRectangle(tiles.First(e => e.GetAttribute("name") == "closed_exit").GetElementsByTagName("rectangle").Cast<XmlElement>().First()), manager);
			var openedExit = new Sprite(texture, XmlLoader.LoadRectangle(tiles.First(e => e.GetAttribute("name") == "opened_exit").GetElementsByTagName("rectangle").Cast<XmlElement>().First()), manager);
			var closedChest = new Sprite(texture, XmlLoader.LoadRectangle(tiles.First(e => e.GetAttribute("name") == "closed_chest").GetElementsByTagName("rectangle").Cast<XmlElement>().First()), manager);
			var openedChest = new Sprite(texture, XmlLoader.LoadRectangle(tiles.First(e => e.GetAttribute("name") == "opened_chest").GetElementsByTagName("rectangle").Cast<XmlElement>().First()), manager);
			var enterance = new Sprite(texture, XmlLoader.LoadRectangle(tiles.First(e => e.GetAttribute("name") == "enterance").GetElementsByTagName("rectangle").Cast<XmlElement>().First()), manager);

			return new TileSet(tileSize, surfaceSelector, wallSelector, closedExit, openedExit, closedChest, openedChest, enterance);
		}
	}
}
