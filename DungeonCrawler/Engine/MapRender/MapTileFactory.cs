﻿using System;
using Engine.Graphics;
using Engine.Core.MapObjects;
using Engine.StoredMap;

namespace Engine.MapRender
{
	public class MapTileFactory
	{
		#region Variables
		
		private readonly CoordinateManager _manager;
		private readonly TileSet _tileSet;

		#endregion

		#region Properties
		#endregion

		#region Methods

		public MapTileFactory(CoordinateManager manager, TileSet tileset)
		{
			if (manager == null)
			{
				throw new ArgumentNullException("manager");
			}
			if (tileset == null)
			{
				throw new ArgumentNullException("tileset");
			}

			_manager = manager;
			_tileSet = tileset;
		}

		public MapTile Create(IDsMap map, int rowIndex, int columnIndex, int x, int y)
		{
			MapTile result;

			switch (map.Cell(rowIndex, columnIndex))
			{
				case 1:
					result = new Wall(_tileSet.Wall.GetSprite(map, rowIndex, columnIndex), x, y, _manager);
					return result;
				default:
					result = new Surface(_tileSet.Surface, x, y, _manager);
					return result;
			}	
		}

		#endregion
	}
}
