﻿using System;
using System.Collections.Generic;
using Engine.Graphics;

namespace Engine.MapRender
{
	public class SpriteSelector
	{
		private readonly List<Sprite> _sprites;
		private readonly List<int> _selectors;
		private readonly Random _random;

		public SpriteSelector(List<Sprite> sprites)
			: this(sprites, null)
		{
		}

		public SpriteSelector(List<Sprite> sprites, List<int> frequencies)
		{
			if (sprites == null)
			{
				throw new ArgumentNullException("sprites");
			}
			if (sprites.Count == 0)
			{
				throw new ArgumentOutOfRangeException("sprites", "List is empty.");
			}

			if (frequencies == null)
			{
				frequencies = new List<int>(sprites.Count);
			}
			if (frequencies.Count < sprites.Count)
			{
				for (int i = 0; i < sprites.Count - frequencies.Count; i++)
				{
					frequencies.Add(1);
				}
			}

			_random = new Random();
			_sprites = new List<Sprite>(sprites);
			_selectors = new List<int>();
			for (int i = 0; i < sprites.Count; i++)
			{
				for (int j = 0; j < frequencies[i]; j++)
				{
					_selectors.Add(i);
				}
			}
		}

		public Sprite GetNext()
		{
			return _sprites[_selectors[_random.Next(_selectors.Count)]];
		}
	}
}
